import dash
from dash.dependencies import Input, Output, State, ALL
from dash.exceptions import PreventUpdate
from dash import html, dcc
import dash_bootstrap_components as dbc

from app import app
from utils.helper import style, T

# Modules
#from apps._template import callbacks, layout as app0
from apps.cantilever_beam import callbacks, layout as app1
from apps.simply_supported_beam import callbacks, callbacks_superposition, layout as app2
from apps.cable import callbacks, layout as app4
from apps.trusses import callbacks, layout as app5
from apps.stress_fields import callbacks, layout as app6
from apps.complex_cross_sections import callbacks, layout as app7
from apps.deformation import callbacks, layout as app8

## LAYOUT
T_id_store = dcc.Store(
    id='T_id',
    storage_type='session',
    data=0
)

# Navigation bar layout
def navbar_layout(T_id):
    item_color = style['color']['success']
    tab = 8
    navbar = dbc.NavbarSimple(
        children=[
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem(
                        module['metadata']['short_title'][T_id], 
                        href=f"/app{module['metadata']['id']}",
                        id={'type':'nav_btn', 'index':module['metadata']['id']}
                    ) for module in T['apps'].values()
                ], 
                direction="down",
                nav=True,
                in_navbar=True,
                label=T['general']['navbar']['modules'][T_id],
                right=True, 
                menu_variant='dark',
            ),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem(
                        [
                            html.I(
                                className="fas fa-globe me-2",
                                style={'color': item_color}
                            ), 
                            html.B(
                                T['general']['navbar']['settings']['language']['label'][T_id],
                                style={'color': item_color}
                            )
                        ], 
                        header=True
                    ),
                    dbc.DropdownMenuItem(
                        html.Div(
                            T['general']['navbar']['settings']['language']['english'][T_id], 
                            style={'margin-left': tab}), 
                            id='EN', 
                            active=(T_id==0), 
                            href='/'
                        ),
                    dbc.DropdownMenuItem(
                        html.Div( 
                            T['general']['navbar']['settings']['language']['german'][T_id], 
                            style={'margin-left': tab}
                        ), 
                        id='DE', 
                        active=(T_id==1),
                        href='/'
                        ),
                    dbc.DropdownMenuItem(
                        [
                            html.I(
                                className="fas fa-external-link-alt me-2",  
                                style={'color': item_color}
                            ), 
                            html.B(
                                T['general']['navbar']['settings']['external_links']['label'][T_id], 
                                style={'color': item_color}
                            )
                        ],
                        header=True, 
                        style={'margin-top': 30}
                    ),
                    dbc.DropdownMenuItem(
                        html.Div(
                            T['general']['navbar']['settings']['external_links']['course_page'][T_id],
                            style={'margin-left': tab}
                        ), 
                        href='https://sudret.ibk.ethz.ch/education/baustatik.html', 
                        target='_blank'
                    ),
                    dbc.DropdownMenuItem(
                        html.Div(
                            'Moodle', 
                            style={'margin-left': tab}
                        ),
                        href='https://moodle-app2.let.ethz.ch/course/view.php?id=17991', 
                        target='_blank'
                    ),
                    dbc.DropdownMenuItem(
                        html.Div(
                            T['general']['navbar']['settings']['external_links']['chair'][T_id], 
                            style={'margin-left': tab}
                        ),
                        href='https://sudret.ibk.ethz.ch/', 
                        target='_blank'
                    )

                ], direction="down",
                nav=True,
                in_navbar=True,
                label=T['general']['navbar']['settings']['label'][T_id],
                right=True, 
                menu_variant='dark',
            ),
            dbc.Button(
                html.Img(
                    src='/assets/images/eth_logo.png',
                    style={'margin-left': 10, 'width': 120}
                ), 
                color="link",
                href='https://ethz.ch/',
                target='_blank'
            ) 
        ],
        brand=T['general']['navbar']['home'][T_id],
        brand_href="/",
        color="primary",
        dark=True, 
        fluid=False,
    )

    return navbar 


# Modules cards
def module_card(module, T_id):
    card = dbc.Card(
        [
            dbc.CardImg(src=module['metadata']['img'], top=True, style={'object-fit':'cover', 'height':'220px'}),
            dbc.CardBody(
                html.Div(
                [   
                    
                        html.H4(module['metadata']['full_title'][T_id], className="card-title"),
                        html.P(module['metadata']['description'][T_id], className="card-text"),
                        dbc.Button(
                            T['home']['open_module'][T_id], 
                            href=f"/app{module['metadata']['id']}", 
                            color='success',
                            outline=True,
                            style={'margin-bottom': 0, 'margin-top': 'auto'}
                        ),
                ],
                style={'display': 'flex', 'flex-flow':'column', 'height': '100%'}
                )
            ),
        ],
        style={"width": '22%', "margin": 15, "padding":0},
    )

    return card 

# Homepage layout
def homepage_layout(T_id):
     # Header
    jumbotron = html.Div(
        dbc.Container(
            [
                html.H1(
                    [
                        html.B(T['home']['title'][T_id]),
                        html.B('Baustatik App',style={'margin-left': 10, 'color': style['color']['secondary']})
                    ], 
                    style={'display':'flex', 'justify-content':'center'},
                    className="display-3"
                ),
                html.P(T['home']['subtitle'][T_id], className="lead"),
                html.Hr(className="my-2"),
                html.P(T['home']['description'][T_id])
            ],
            fluid=True,
            className="py-3 text-center",
        ),
        className="jumbotron text-center",
        style={'margin-top': 30, 'margin-bottom':-15}
    )

    homepage = html.Div(    
        [
            #html.Br(),
            jumbotron,
            dbc.Row(
                [module_card(module, T_id) for module in T['apps'].values()], 
                justify='center'
            )
        ]
    )  

    return homepage     

# Page content
# Listing all the apps available 
apps_layout = {
    '/' : homepage_layout,
    #'/app0' : app0.layout,
    '/app1' : app1.layout,
    '/app2' : app2.layout,
    '/app3' : app2.layout,
    '/app4' : app4.layout,  
    '/app5' : app5.layout,
    '/app6' : app6.layout,
    '/app7' : app7.layout,
    '/app8' : app8.layout
} 

app.layout = html.Div(
    [
        T_id_store,
        html.Div(navbar_layout(0), id='navbar'),
        dcc.Location(id='url', refresh=True),
        dbc.Container(id='page-content'),
    ]
)
app.title = 'Baustatik I Web App'  
server = app.server                     

## CALLBACKS
# Language selection
@app.callback(
    Output('T_id', 'data'), 
    [
        Input('EN', 'n_clicks'),
        Input('DE', 'n_clicks')
    ]
)
def language_selection(en, de):
    languages = dash.callback_context.triggered[0]['prop_id'].split('.')[0]

    # Prevent callback if no language button is cliked 
    if (en is None) and (de is None):
        raise PreventUpdate
    if languages=='EN':
        T_id = 0
    else:
        T_id = 1

    return T_id

# Multi-page navigation
@app.callback(
    [
        Output('page-content', 'children'),
        Output('navbar', 'children'),
        Output('EN', 'href'),
        Output('DE', 'href'),
        Output({'type': 'nav_btn', 'index': ALL}, 'active'),
    ],
    Input('url', 'pathname'),
    Input('T_id', 'data')
)
def display_page(pathname, T_id):
    """Change content acording to the url

    Args:
        pathname (string): URL path name

    Returns:
        Div: Dash html.Div object containing the page layout
    """

    # Active module button
    active = [False]*8#(len(apps_layout) - 1)
    if pathname != '/':
        index = int(pathname[-1]) - 1
        active[index] = True

    # Layout
    if pathname in apps_layout.keys():
        
        ## Page content
        # Special case for simply_supported_beam
        if pathname=='/app2':
            page_content = apps_layout[pathname](T_id, '')
        elif pathname=='/app3':
            page_content = apps_layout[pathname](T_id, '-superp')
        # Page with translation
        elif callable(apps_layout[pathname]):
            page_content = apps_layout[pathname](T_id)
        # Page without translation
        else:
            page_content = apps_layout[pathname]
        
        ## Navigation bar
        navbar = navbar_layout(T_id)
        
        return page_content, navbar, pathname, pathname, active
    else:
        page_content = '404'
        navbar = navbar_layout(T_id)
        return page_content, navbar, pathname, pathname, active


# Theory boxes collapse
for app_id in T['apps'].keys():
    @app.callback(
        Output(f"theory-collapse-{app_id}", "is_open"),
        [Input(f"theory-toggle-{app_id}", "n_clicks")],
        [State(f"theory-collapse-{app_id}", "is_open")],
    )
    def toggle_accordion(n_clicks, is_open):
        if n_clicks == 0:
            raise PreventUpdate
        return not is_open


if __name__ == "__main__":
    app.run_server(debug=False, port=8010)