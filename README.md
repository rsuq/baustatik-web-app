# Baustatik Web App
[https://baustatik.herokuapp.com](https://baustatik.herokuapp.com/)

![Alt Text](https://media.giphy.com/media/rgg2NktLZP31u0mgfO/giphy.gif)
## Running the app locally
1. Create python environment
2. Install dependencies in [requirements.txt](requirements.txt)
3. Run [main.py](main.py)
4. Open [http://127.0.0.1:8010/](http://127.0.0.1:8010/) in your browser

**Note:** turn on/off **debug mode** changing the following boolean in [main.py](main.py)
```python
if __name__ == '__main__':
    app.run_server(debug=True, port=8010)
``` 


## Implementing new modules
Note: taking [_template](apps/_template) module as example

**Metadata**

In the [translate.json](assets/translate.json) file:
1. Add a new module inside the 'apps' tree
2. Fill in the module metadata
3. Add new strings to be translated

**Develop**

In [theory_de.md](apps/_template/theory_de.md) and [theory_en.md](apps/_template/theory_en.md) files
1. Write theory box

In [layout.py](apps/_template/layout.py) file:
1. Add parameters
2. Add graphs  

In the [callbacks.py](apps/_template/callbacks.py) file:
1.  Add callbacks

**Deploy**

In [main.py](main.py) file:
1. Add to `# Modules` section
```python
from apps._template import callbacks, layout as app0
``` 

2.  Add to `apps_layout` dictionary
```python
'/app0' : app0.layout
```


## Deploy to production
1. Install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
2. Have the latest `main` version of the project source code in your local machine
3. Follow the common git steps:
```git
$ heroku login
$ heroku git:remote -a baustatik
$ git add .
$ git commit -m "make it better"
$ git push heroku main
```
### Checking app logs
```git
$ heroku logs --tail
```


## Git convention 
- When developing a new module, create a new branch with the name of the module 
- Only merge the branch when the module is concluded
- For minor fixes, commit directly to main branch
### Semantic Commit Messages:
**Format:** 

`<type>(<scope>): <subject>`

`<scope>` is optional

**Example:**
```git
feat: make it better
```

- `build`: Changes that affect the build system or external dependencies (formerly `chore`)
- `ci`: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- `docs`: Documentation only changes
- `feat`: A new feature
- `fix`: A bug fix
- `perf`: A code change that improves performance
- `refactor`: A code change that neither fixes a bug nor adds a feature
- `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- `test`: Adding missing tests or correcting existing tests


## Useful links
- [Dash Documentation & User Guide | Plotly](https://dash.plotly.com/)
- [Plotly Python Graphing Library](https://plotly.com/python/)
- [Dash Bootstrap Components](https://dash-bootstrap-components.opensource.faculty.ai/)
- [SVG path editor](https://yqnn.github.io/svg-path-editor/)
- [Concrete benchmark app](https://concrete.ethz.ch/applikationen/)
- [Overleaf project specifications](https://www.overleaf.com/project/62a36d271a6a7179beb617b0) (internal)
- [Gitlab code repository](https://gitlab.ethz.ch/rdiasmoreira/baustatik-web-app) (internal)
- [Heroku app deployment dashboard](https://dashboard.heroku.com/apps/baustatik) (internal)



