from plotly.basedatatypes import BaseTraceType
from sympy import *
import numpy as np
import math
from utils.helper import T 

# Equations toolkit for Structural Mechanics applications

class SimplySupportedBeam:
    """Calculator for internal force diagram of a simply supported beam under point load
    """

    def __init__(self, load_mode, F=0, q=0, a=0, l=10):
        """

        Args:
            F (int): Load [kN]
            a (int): Load distance from the A support [m]
            l (int, optional): Beam's lenght. Defaults to 10. [m]
        """
        self.load_mode = load_mode
        self.F = F
        self.q = q
        self.a = a
        self.l = l

    # Shear force

    def shear(self, x):
        """Shear cut (V) calculation

        Args:
            positive (bool, optional): Positive/negative reaction sign. Defaults to True.

        Returns:
            float: Shear cut [kN]
        """
        if self.load_mode == 'point-load':
            if x < self.a:
                shear = self.F*(self.l-self.a)/self.l
            else:
                shear = -self.F*self.a/self.l
        
        elif self.load_mode == 'distributed-load':
            shear = self.q * ((self.l / 2) - x)

        return shear

    # Beding moment
    def moment(self, x):
        """Bending moment (M) calculation

        Returns:
            float: Bending moment [kN.m]
        """

        if self.load_mode == 'point-load':
            if x < self.a:
                moment = self.F * (self.l - self.a) * x / self.l 
            else:
                moment = self.F * (self.a / self.l) * (self.l - x)

        elif self.load_mode == 'distributed-load':
            moment = self.q * x * (self.l - x) / 2

        return moment

def equilibrium(
    T_id, app_id,
    f1_neg=False, f1_none=False, f1_pos=True, 
    f2_neg=False, f2_none=False, f2_pos=True, 
    m_neg=False, m_none=False, m_pos=True, 
    ):

    # variables
    ah, av, gamma, f1, f2, m, m2 = symbols('A_{H}, A_{V}, \Gamma_{A}, F_{1}, F_{2}, \Gamma, F_{2}\cdotℓ')

    # Updating variables
    if f1_neg:
        f1 = -f1
    elif f1_none:
        f1 = 0

    if f2_neg:
        f2 = -f2
        m2 = -m2
    elif f2_none:
        f2 = 0
        m2 = 0

    if m_neg:
        m = -m
    elif m_none:
        m = 0

    # functions
    fx = Eq(ah + f1 , 0)
    fy = Eq(av + f2 , 0)
    ma = Eq(gamma - m2 + m , 0)
    functions = [fx, fy, ma]

    # solutions
    ah_solver=Eq(ah, solve(fx, ah)[0])
    av_solver=Eq(av, solve(fy, av)[0])
    gamma_solver=Eq(gamma, solve(ma, gamma)[0])
    solutions = [ah_solver, av_solver , gamma_solver]

    # Transform to LaTeX
    functions = [latex(eq) for eq in functions]
    solutions = [latex(eq) for eq in solutions]

    # Create text
    text = f'''

        {T['apps'][app_id]['global_equilibrium'][T_id]}

        $$ 
        \\begin{{aligned}}
        \sum F_{{x}} = 0 : \qquad &{functions[0]} &&\\text{{({T['apps'][app_id]['horizontal'][T_id]})}} \\\\

        \sum F_{{y}} = 0 : \qquad &{functions[1]} &&\\text{{({T['apps'][app_id]['vertical'][T_id]})}} \\\\

        \sum M_{{/A}} = 0 : \qquad &{functions[2]} &&\\text{{({T['apps'][app_id]['rotation'][T_id]})}} \\\\

        &\\phantom{{\qquad\qquad\qquad\qquad\qquad\qquad}}&&

        \\end{{aligned}}
        $$

        {T['apps'][app_id]['with_solution'][T_id]}
            
        $$ 
        \\begin{{aligned}}
        
        {solutions[0].replace('=','&=')} \\\\

        {solutions[1].replace('=','&=')} \\\\

        {solutions[2].replace('=','&=')} \\\\

        &\\phantom{{\qquad\qquad\qquad\qquad\qquad}}
        \\end{{aligned}}
        $$
    '''

    return text


def cable_location(x, M, H, z0, zl, l):
    # Values are mirrored
    z0 = -z0
    zl = -zl
    h=zl-z0
    z = z0 + M/H + h*x/l
    z = -z
    z_rel = -M/H

    return z, z_rel


class Truss:
    def __init__(self, l, h, bays, type):
        self.l = l
        self.h = h
        self.bays = bays
        self.type = type

        self.geometry()

    def geometry(self):
        step = self.l/(self.bays*2)
        n_nodes = self.bays*2+1

        # Nodes
        nodes = [[x*step, 0] for x in range(0,n_nodes)]
        nodes = np.array(nodes)
        nodes[1::2][:,1] += self.h
        if self.type!='warren':
            nodes[1::2][:,0] += step
            nodes = np.delete(nodes,-2,0) if self.bays>1 else nodes

        # Labels
        labels = np.arange(len(nodes))

        # Chords
        chords = [[labels[i], labels[i+2]] for i in range(len(labels)-2)]
        if self.type != 'warren' and self.bays>1:
            chords[-1][0] += 1
        btm_chords = chords[:-1:2]+[chords[-1]]
        top_chords = chords[1:-1:2]
        btm_chords = np.array(btm_chords)
        top_chords = np.array(top_chords)
        
        # Bars
        diagonals = [[labels[i], labels[i+1]] for i in range(0,len(labels)-1)]
        diagonals = np.array(diagonals)
        if self.type!='warren' and self.bays>1:
            diagonals[-1][0] -= 1
            if self.type=='pratt':
                diagonals[2:n_nodes//2:2] += [-1,1]
            elif self.type=='howe':
                diagonals[((self.bays+1)//2)*2:-1:2] += [-1,1]
        bars = np.concatenate((diagonals,btm_chords))
        try:
            bars = np.concatenate((bars,top_chords))
        except:
            pass
                
        # Diagonals
        diagonals = np.array([nodes[i] for i in diagonals.flatten()])
        top_chords = np.array([nodes[i] for i in top_chords.flatten()])
        btm_chords = np.array([nodes[i] for i in btm_chords.flatten()])

        # Update bars + labels
        labels += 1

        self.nodes = nodes # Used by the solver
        self.bars = bars # Used by the solver
        self.labels = labels
        self.diagonals = diagonals
        self.top_chords = top_chords
        self.btm_chords = btm_chords


    def internal_forces(self, loads):
        
        nnodes = self.nodes.shape[0]
        nbars = self.bars.shape[0]

        # %% - Externe Lasten
        forces = np.zeros([nnodes,2])
        for k, v in loads.items():
            forces[int(k)-1,:] = v
            forces[int(k)-1,:][1] = - forces[int(k)-1,:][1] # Hot-fix to mirror vertical axis


        # %% - Lager
        # für Lagerung in x-Richtung [1,0]
        # für Lagerung in z-Richtung [0,1]
        # für Lagerung in x-Richtung und z-Richtung [1,1]
        bearing = np.zeros([nnodes,2])
        bearing[0,:] = [1,1]
        bearing[-1,:] = [0,1]
        nlager = int(bearing.sum(0).sum(0))

        # %% Ist Fachwerk statisch bestimmt?
        h = nlager + nbars -2*nnodes
        if h!=0:
            # Unterbrechung des Programms
            raise Exception('Das system ist nicht statisch bestimmt!')

        # %% Aufstellen des Systems
        #- Anordnen des Kraftvektors
        F = forces.reshape(-1)


        # zunächst die Stabkräfte M_N
        M_N = np.zeros([2*nnodes, nbars]);
        for ii in range(nnodes):
            # iteriere über jeden Knoten und stelle Gleichungen auf
            # (horizontales und vertikales Gleichgewicht)
            
            # - Stabkräfte
            # Stäbe die am Knoten ii angreifen
            for jj in range(nbars):
                # iteriere über jeden Stab und überprüfe ob er am
                # Knoten ii angreift
                barcurr = self.bars[jj,:]
                if any(barcurr == ii):
                    # barcurr greift am Knoten ii an
                    if ii==barcurr[0]:
                        # Stab startet vom Knoten ii
                        nodestart = self.nodes[barcurr[0],:]
                        nodeend = self.nodes[barcurr[1],:]
                    else:
                        # Stab endet bei Knoten ii
                        nodestart = self.nodes[barcurr[1],:]
                        nodeend = self.nodes[barcurr[0],:]
                    
                    barvec = nodeend-nodestart
                    barlength = np.linalg.norm(barvec)
                    
                    # - Projektion der Stabkraft in Matrix einsetzen
                    # x-Richtung
                    M_N[2*ii,jj] = barvec[0]/barlength;
                    # y-Richtung
                    M_N[2*ii+1,jj] = barvec[1]/barlength;

        # Lagerreaktionen Berücksichtigung
        M_R = bearing.reshape(-1)
        M_R = np.diag(M_R)
        # entferne 0 Spalten
        M_R = np.delete(M_R, ~M_R.any(axis=1),1)

        # - Zusammensetzen der Matrix
        M = np.block([M_N,M_R])

        # %% Berechnung
        # - Lösen des Gleichungssystems
        # M*n=F
        # wobei n...die Stabkräfte und Lagerreaktionen beinhaltet

        n = np.linalg.solve(M,-F)

        # Aufteilen der berechneten Kräfte
        barforces = n[0:nbars]
        reactionforces = n[nbars:]
        
        return barforces


class Displacement:
    def __init__(self, mz=0, young=200, poisson=0.3, magnifier=1, cross_section={'type':'rectangle', 'b':0, 'h':0}):
        self.mz = mz*1e3 # KNm to Nm
        self.young = young*1e9 # GPa to Pa (or Nm^2)
        self.poisson = poisson
        self.magnifier = magnifier
        self.cross_section = cross_section

        self.iz = self.moment_of_inertia()
    
    def moment_of_inertia(self):
        if self.cross_section['type']=='circle':  
            iz = (np.pi*self.cross_section['r']**2)/4
        elif self.cross_section['type']=='rectangle':
            iz = (self.cross_section['b']*self.cross_section['h']**3)/12
        
        return iz


    def x_disp(self, x, y):
        x_shift = -(self.mz/(self.young*self.iz))*y*x 
        x_shift = x_shift*self.magnifier

        return x_shift

    
    def y_disp(self, x, y, z):
        y_shift = (self.mz/(self.young*self.iz))*((x**2)/2 + self.poisson*(y**2-z**2)/2)
        y_shift = y_shift*self.magnifier
        
        return y_shift
        
    
    def z_disp(self, y, z):
        z_shift = self.poisson*(self.mz/(self.young*self.iz))*y*z
        z_shift = z_shift*self.magnifier
        
        return z_shift

    def sum_disp(self, x, y, z):
        disp = np.sqrt(self.x_disp(x,y)**2 + self.y_disp(x,y,z)**2 + self.z_disp(y,z)**2)
        return disp
    
    def stress(self, y):
        stress=-(self.mz/self.iz)*y

        return stress


class ComplexCrossSection:
    def __init__(self, b, h, t, s):
        self.b = b
        self.h = h
        self.t = t
        self.s = s
        self.elements = []
        self.eta = np.array([])
        self.zeta = np.array([])
        self.center_of_mass = [0,0]
        self.area = self.Ieta = self.Izeta = self.Ietazeta = self.theta = 0
    

    def add_element(self, eta0, eta1, zeta0, zeta1, thickness, point_gap, label, margin=0):
        # Add scatter points
        eta, zeta = self.__rectangle_scatter(eta0, eta1, zeta0, zeta1, point_gap, margin)
        self.eta = np.concatenate((self.eta, eta),axis=None)
        self.zeta = np.concatenate((self.zeta, zeta),axis=None)
        
        # Define element with area and center of mass
        element = {
            'label': label,
            'area': (eta1-eta0)*(zeta1-zeta0),
            'center_of_mass': [(eta1+eta0)/2, (zeta1+zeta0)/2],
            'moment_inertia': {
                'Ieta': ((eta1-eta0)*(zeta1-zeta0)**3)/12, 
                'Izeta': ((zeta1-zeta0)*(eta1-eta0)**3)/12, 
                'Ietazeta':0
            },
            'eta': eta,
            'zeta': zeta,
            'thickness': thickness
        }
        self.elements.append(element)

    
    # Generate a rectangular scatter plot
    def __rectangle_scatter(self, eta0, eta1, zeta0, zeta1 ,point_gap , margin=0):
        hor = np.arange(eta0+margin, eta1-margin+1e-6, point_gap)
        ver = np.arange(zeta0+margin, zeta1-margin+1e-6,point_gap)
        eta, zeta = np.meshgrid(hor, ver)    
        eta = eta.flatten()
        zeta = zeta.flatten()

        return eta, zeta
    

    # Generate a elliptical scatter plot
    def add_ellipse(self, n_theta=230, n_gap=10):
        b1 = self.b-self.t*2
        h1 = self.h-self.t*2
        # Add scatter points
        theta = np.linspace(0, 2*np.pi, n_theta)
        eta = np.array([r*np.cos(theta) for r in np.arange(self.b/2-self.t,self.b/2+1e-6,n_gap)])
        zeta = np.array([r*np.sin(theta) for r in np.arange(self.h/2-self.t,self.h/2+1e-6,n_gap)])
        self.eta = eta.flatten()
        self.zeta = zeta.flatten()

        # Define element with area and center of mass
        element = {
            'area': (np.pi/4)*(self.b*self.h-b1*h1),
            'center_of_mass': [0, 0],
            'moment_inertia': {
                'Ieta': np.pi*(self.b*self.h**3 - b1*h1**3)/64, 
                'Izeta': np.pi*(self.b**3*self.h - b1**3*h1)/64, 
                'Ietazeta':0
            }
        }
        self.elements.append(element)
    

    def get_center_of_mass(self):
        self.area = sum([element['area'] for element in self.elements])
        hor = sum([element['area']*element['center_of_mass'][0] for element in self.elements])/self.area
        ver = sum([element['area']*element['center_of_mass'][1] for element in self.elements])/self.area
        self.center_of_mass = [hor, ver]
    

    def moment_inertia(self):
        # Move moment of inertia for cross section center of mass (Parallel axes theorem)
        for element in self.elements:
            element['moment_inertia']['Ieta'] += element['area']*(self.center_of_mass[1] - element['center_of_mass'][1])**2
            element['moment_inertia']['Izeta'] += element['area']*(self.center_of_mass[0] - element['center_of_mass'][0])**2
            element['moment_inertia']['Ietazeta'] += element['area']*(self.center_of_mass[0] - element['center_of_mass'][0])*(self.center_of_mass[1] - element['center_of_mass'][1])
    
        self.Ieta = sum([element['moment_inertia']['Ieta'] for element in self.elements])
        self.Izeta = sum([element['moment_inertia']['Izeta'] for element in self.elements])
        self.Ietazeta = sum([element['moment_inertia']['Ietazeta'] for element in self.elements])
        self.theta = (math.atan(2*self.Ietazeta /( self.Izeta-self.Ieta)))/2


    def __area_moment_inertia(self, eta, zeta, label):
        Szeta = eta*0
        if (label=='I-top') | (label=='I-bottom'):
            x = self.b/2 - abs(eta)
            A = x*self.t
            y = (self.h - self.t)/2

            Seta = A*y 

            # Szeta
            x = self.b/2 - abs(eta) 
            A = x*self.t
            y = (self.b - x)/2

            Szeta = A*y

        elif label=='I-vertical':
            x = self.h/2 - self.t - abs(zeta)
            A1 = self.b*self.t
            y1 = (self.h-self.t)/2

            A2 = x*self.s
            y2 = (self.h-x)/2 - self.t

            S1 = A1*y1
            S2 = A2*y2

            Seta = S1 + S2  

        elif label=='U-top':
            x = self.b/2 - eta
            A = x*self.t
            y = (self.h-self.t)/2

            Seta = A*y
            
            # Szeta
            x = self.b/2 - eta
            A = x*self.t
            y = (self.b/2-self.center_of_mass[0]) - x/2

            Szeta = A*y


        elif label=='U-vertical':
            x = self.h/2 - self.t - abs(zeta)
            A1 = self.b*self.t
            y1 = (self.h-self.t)/2

            A2 = x*self.s 
            y2 = (self.h-x)/2 - self.t

            S1 = A1*y1
            S2 = A2*y2

            Seta = S1 + S2
            
            # Szeta
            A1 = self.b*self.t
            y1 = abs(self.center_of_mass[0])
            S1 = A1*y1

            h_ = self.h/2 - self.t
            slope = S1 / h_

            Szeta = S1 - slope * (h_-abs(zeta))


        elif label=='U-bottom':
            x = self.b/2 - eta
            A = x*self.t
            y = (self.h-self.t)/2

            Seta = A*y

            # Szeta
            x = self.b/2 - eta
            A = x*self.t
            y = (self.b/2-self.center_of_mass[0]) - x/2

            Szeta = A*y

        
        elif label=='L-top':
            x = self.b/2 - eta
            A = x*self.t
            y = (self.h-self.t)/2 - self.center_of_mass[1]
            Seta = A*y

            # Szeta
            x = self.b/2 - eta
            A = x*self.t
            y = (self.b-x)/2 - self.center_of_mass[0]
            Szeta =A*y
        
        elif label=='L-vertical':
            x = self.h/2 - self.t - zeta
            A1 = self.b*self.t
            y1 = (self.h-self.t)/2 - self.center_of_mass[1]

            A2 = x*self.s
            y2 = (self.h-x)/2 - self.t - self.center_of_mass[1]

            S1 = A1*y1
            S2 = A2*y2

            Seta = S1 + S2

            # Szeta
            x = self.h/2 - self.t - zeta
            A1 = self.b*self.t
            y1 = -self.center_of_mass[0]

            A2 = self.s*x
            y2 = (self.b - self.s)/2  + self.center_of_mass[0]

            S1 = A1*y1
            S2 = A2*y2

            Szeta = S1 - S2


        return Seta, Szeta
    
    def __flow(self, eta, zeta, label):
        flow_zeta = np.ones_like(zeta)
        flow_eta = np.ones_like(eta)

        if (label == 'I-top') | (label == 'I-bottom'):
            flow_zeta[np.logical_and(eta > 0, zeta <= 0)] *= -1
            flow_zeta[np.logical_and(eta <= 0, zeta > 0)] *= -1

        elif label == 'I-vertical':
            flow_eta[zeta <= 0] *= -1

        elif label == 'U-top':
            # Flow is always positive so nothing to do here
            pass

        elif label == 'U-vertical':
            # Styfen:
            # This should be flow_eta[zeta > 0] *= -1 because its negative where zeta' is positive but then the plot is wrong!?
            # With flow_eta[zeta <= 0] *= -1 the plot is correct but the logic seems wrong to me.
            # How's your coodinate system in the backend defined?
            flow_eta[zeta <= 0] *= -1

        elif label == 'U-bottom':
            flow_zeta *= -1

        elif label == 'L-top':
            # Flow is always positive so nothing to do here
            pass

        elif label == 'L-vertical':
            # Flow is always positive so nothing to do here
            pass

        return flow_zeta, flow_eta


    def neutral_axis(self, start, end, N, Meta, Mzeta):
        eta, zeta = symbols('𝜂, 𝜁')

        equation = Eq(N/self.area + Meta*(self.Izeta*zeta - self.Ietazeta*eta)/(self.Ieta*self.Izeta - self.Ietazeta**2) - Mzeta*(self.Ieta*eta - self.Ietazeta*zeta)/(self.Ieta*self.Izeta - self.Ietazeta**2),0)
   
        zeta_start = solve(
            [
                equation,
                Eq(eta, start)
            ]
        )[zeta]
        zeta_end = solve(
            [
                equation,
                Eq(eta, end)
            ]
        )[zeta]

        return float(zeta_start), float(zeta_end)


    
    def normal_stress(self, eta, zeta, N, Meta, Mzeta): # -x,-y in Plotly natural axes represents y and z correspondingly 
        sigma_n = N/self.area
        sigma_meta = Meta*(self.Izeta*zeta - self.Ietazeta*eta)/(self.Ieta*self.Izeta - self.Ietazeta**2)
        sigma_mzeta = Mzeta*(self.Ieta*eta - self.Ietazeta*zeta)/(self.Ieta*self.Izeta - self.Ietazeta**2)
        sigma = sigma_n + sigma_meta - sigma_mzeta
        
        return sigma

    
    def shear_stress(self, Veta, Vzeta):
        shear = np.array([]) 
        # Calculate for each bar/element
        for element in self.elements:
            Seta, Szeta =  self.__area_moment_inertia(element['eta'], element['zeta'], element['label'])
            
            tau_xeta =  (Veta/element['thickness']) * ((self.Ieta*Szeta - self.Ietazeta*Seta) / (self.Ieta*self.Izeta - self.Ietazeta**2))
            tau_xzeta = (Vzeta/element['thickness']) * ((self.Izeta*Seta - self.Ietazeta*Szeta) / (self.Ieta*self.Izeta - self.Ietazeta**2))

            flow_zeta, flow_eta = self.__flow(
                element['eta'], element['zeta'], element['label'])

            tau_xeta *= flow_eta
            tau_xzeta *= flow_zeta

            
            shear = np.concatenate((shear, tau_xeta+tau_xzeta), axis=None)
        
        return shear


# Euler Bernoulli/Timoshenko beam theory
def beam_theory(beam_supports,  l, h, E, I, G, Av, q, x):
    solutions = {
        'Euler-Bernoulli': {},
        'Timoshenko': {}
    }
    if beam_supports == ['fixed', 'fixed']:
        solutions['Euler-Bernoulli']['moment'] = solutions['Timoshenko']['moment'] = (-q*l**2/12)*(
            6*(x/l)**2-6*(x/l)+1)
        solutions['Euler-Bernoulli']['rotation'] = solutions['Timoshenko']['rotation'] = (
            q*l**3/(12*E*I))*(x/l)*(2*(x/l)**2-3*(x/l)+1)
        solutions['Euler-Bernoulli']['deflection'] = (
            q*l**4/(24*E*I))*(x/l)**2*((x/l)-1)**2
        solutions['Timoshenko']['deflection'] = solutions['Euler-Bernoulli']['deflection'] - \
            (q*l**2/(2*G*Av))*(x/l)*((x/l)-1)
    elif beam_supports == ['fixed', 'roller']:
        solutions['Euler-Bernoulli']['moment'] = solutions['Timoshenko']['moment'] = - \
            q*(l**2/8 - 5*l*x/8 + x**2/2)
        solutions['Euler-Bernoulli']['rotation'] = solutions['Timoshenko']['rotation'] = - \
            l**2*q*x/(8*E*I) + 5*l*q*x**2/(16*E*I) - q*x**3/(6*E*I)
        solutions['Euler-Bernoulli']['deflection'] = l**2*q * \
            x**2/(16*E*I) - 5*l*q*x**3/(48*E*I) + q*x**4/(24*E*I)
        solutions['Timoshenko']['deflection'] = solutions['Euler-Bernoulli']['deflection'] + \
            5*l*q*x/(8*(G*Av)) - q*x**2/(2*(G*Av))
    elif beam_supports == ['pinned', 'roller']:
        solutions['Euler-Bernoulli']['moment'] = solutions['Timoshenko']['moment'] = q * \
            x*(l - x)/2
        solutions['Euler-Bernoulli']['rotation'] = solutions['Timoshenko']['rotation'] = - \
            l**3*q/(24*E*I) + l*q*x**2/(4*E*I) - q*x**3/(6*E*I)
        solutions['Euler-Bernoulli']['deflection'] = l**3 * \
            q*x/(24*E*I) - l*q*x**3/(12*E*I) + q*x**4/(24*E*I)
        solutions['Timoshenko']['deflection'] = solutions['Euler-Bernoulli']['deflection'] + \
            l*q*x/(2*(G*Av)) - q*x**2/(2*(G*Av))
    elif beam_supports == ['fixed', 'free-end']:
        solutions['Euler-Bernoulli']['moment'] = solutions['Timoshenko']['moment'] = - \
            q*(l**2/2 - l*x + x**2/2)
        solutions['Euler-Bernoulli']['rotation'] = solutions['Timoshenko']['rotation'] = - \
            l**2*q*x/(2*E*I) + l*q*x**2/(2*E*I) - q*x**3/(6*E*I)
        solutions['Euler-Bernoulli']['deflection'] = l**2*q * \
            x**2/(4*E*I) - l*q*x**3/(6*E*I) + q*x**4/(24*E*I)
        solutions['Timoshenko']['deflection'] = solutions['Euler-Bernoulli']['deflection'] + \
            l*q*x/(G*Av) - q*x**2/(2*(G*Av))
    else:
        print('Unknown beam supports')


    return solutions



            
