import os
import json
import base64
from dash import html
import numpy as np
from xml.dom import minidom
import re
import pandas as pd
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
from dash import html, dcc
import dash
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, State, ALL ,MATCH

# Aesthetics
with open('assets/aesthetics.json') as json_file:
    style = json.load(json_file)
ROW_CLASS = 'g-3 mt-0 mb-0'
CARD_STYLE = {'height': '100%'}

# Translations
with open('assets/translate.json', encoding='utf-8') as json_file:
    T = json.load(json_file)

# Markdown reader
def md_reader(path):
    with open(path, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        text = """{}""".format("".join(lines))
        text = text.replace('\\', '\\\\')

    return text

# Theory box generator
def theory_box(app_id, T_id, module_path, img=False):
    language = 'en' if T_id==0 else 'de'
    if img:
        path = module_path + f"/theory_{language}.png"
        encoded_image = base64.b64encode(open(path, 'rb').read())
        theory_image = html.Img(
            src='data:image/png;base64,{}'.format(encoded_image.decode()), 
            style={'margin':'auto', 'width':'60%'}
        )
        theory_body = dbc.Collapse(
            dbc.CardBody(
                theory_image, style={'display':'flex'}
                ),
            id=f"theory-collapse-{app_id}",
            is_open=False,
        )
    else:
        path = module_path + f"/theory_{language}.md"
        theory_text = md_reader(path)
        theory_body = dbc.Collapse(
            dbc.CardBody(
                dcc.Markdown(theory_text, style={"text-align": "justify"})
                ),
            id=f"theory-collapse-{app_id}",
            is_open=False,
        )

    theory_container = dbc.Row([
        dbc.Col([
            dbc.Card([
                dbc.CardHeader([
                    dbc.Button(
                        [html.I(className="fas fa-graduation-cap me-1"), T['general']['theory_box'][T_id]],
                        color="link",
                        id=f"theory-toggle-{app_id}",
                        n_clicks=0
                    )
                ]),
                theory_body
            ], style=CARD_STYLE)
        ], md=12),
    ], class_name=ROW_CLASS)

    return theory_container

# Layout generator
def layout_template(app_id, T_id, theory_box, parameters, graphs):
    layout = dbc.Container([
        html.Br(),
        html.H4(T['apps'][app_id]['metadata']['full_title'][T_id]),
        theory_box,
        dbc.Row([
            dbc.Col([
                dbc.Card([
                    dbc.CardHeader([html.I(className="bi bi-sliders me-2"), T['general']['parameters'][T_id]]),
                    dbc.CardBody(
                        parameters
                    )
                ], style = CARD_STYLE),
            ], md=3),
            dbc.Col([
                dbc.Card(
                    graphs
                    ,style=CARD_STYLE, 
                    body=True
                    )
            ], md=9)
        ], class_name=ROW_CLASS),
        html.Br()
    ], id='layout',fluid=False)

    return layout

# Arrow dimensions
class ArrowDim:
    def __init__(self, line_width, F):
        self.line_width = line_width
        self.F = F

    def length(self, factor=0.0045):
        size = self.line_width*self.F*factor
        #size = abs(size)

        return size

    def head(self, factor=0.01):
        size = (self.line_width*self.F*factor)
        size = abs(size)
        # Limit the minimum size
        size = np.where(
            size > 0.3,
            size,
            0.3
        )
        size = float(size)

        return size


# SVG path handler
class SVGPath:

    def __init__(self, type='free-end', path=''):
        self.path = path
        self.supports = {
            'fixed' : 3,
            'pinned': 2,
            'roller': 1,
            'sliding':2,
            'free-end':0,
        }
        self.value = self.supports[type]
        
        self.str_to_df() if path!='' else ''
    
     
    
    def reader(self, file):
        doc = minidom.parse(file)  # parseString also exists
        path_strings = [path.getAttribute('d') for path
                        in doc.getElementsByTagName('path')]
        doc.unlink()
        path = path_strings[0]

        self.path = path
        self.str_to_df()
    
    def str_to_df(self):
        s = self.path.replace('Z', 'Z * *')
        s_list = re.split(' ',s)
        N = 3
        subList = [s_list[n:n+N] for n in range(0, len(s_list), N)]
        self.df = pd.DataFrame(subList, columns=['command','x','y'])
        for dim in ['x','y']:
            self.df[dim] = pd.to_numeric(self.df[dim], errors='coerce')

    def df_to_str(self):
        aux = self.df.fillna('')
        aux = aux.astype('str')
        aux = aux.apply(' '.join, axis=1)
        aux = aux.str.cat(sep=' ')

        self.path = aux
    
    def translate(self, x=0, y=0):
        self.df['x'] = self.df['x'] + x
        self.df['y'] = self.df['y'] + y
        self.df_to_str()
    
    def scale(self, x=1, y=1):
        self.df['x'] = self.df['x'] * x
        self.df['y'] = self.df['y'] * y
        self.df_to_str()
    
    def flip(self):
        max = self.df['x'].max()
        self.df['x'] = 2*max - self.df['x']
        self.df_to_str()

# Color scale with a single color
def single_colorscale(color):
        return [[0, color], [1, color]]    

# Diagram dimensions
def dimension(fig, text, start, end, position, line_width, color, font_size, yref='y3', scale=1, orientation='h', text_margin = -0.15, tab_size=0.2):
    if start!=end:
        if orientation=='h':
            l_start='x'
            l_end='ax'
            pos_start='y'
            pos_end='ay'
        elif orientation=='v':
            l_start='y'
            l_end='ay'
            pos_start='x'
            pos_end='ax'
        else:
            print('Unknown orientation')

        # Arrow
        if start < end:   
            start_margin = 0
            end_margin = 0.03
        else:
           start_margin = 0
           end_margin = 0
        fig.add_annotation(**{
            l_start:start+start_margin,  # arrows head
            l_end:end+end_margin,  # arrows' tail
            pos_start:position,  # arrows' head
            pos_end:position,  # arrows' tail
            'xref':'x',
            'yref':yref,
            'axref':"x",
            'ayref':yref,
            'showarrow':True,
            'text':'',
            'align':"left",
            'arrowhead':1,
            'arrowside':'start+end',
            'arrowsize':1.2*scale,
            'arrowwidth':line_width,
            'arrowcolor':color,
        })

        # Text 
        fig.add_annotation(**{
            'text':text, 
            'font':{'size':font_size, 'color':color},
            'align':'center',
            l_start:(end-start)/2, 
            pos_start:position+text_margin, 
            'showarrow':False, 
            'yref':yref
        })

        # Tabs
        for x_position in [0,end]:
            fig.add_shape({
                'type':'line',
                l_start+'0':x_position,
                l_start+'1':x_position,
                pos_start+'0':position+tab_size/2,
                pos_start+'1':position-tab_size/2,
                'line_color':color,
                'yref':yref
            })
    else:
        pass

    return fig

# Diagram axis convention
def axis(fig, x, y, line_width, color, font_size, yref='y',scale=1):
    fig.add_annotation(
            ax=x+0.8*scale,  # arrows' tail
            x=x,  # arrows' head
            ay=y,  # arrows' tail
            y=y,  # arrows' head
            arrowside='start',
            xref='x',
            axref='x',
            yref=yref,
            ayref=yref,
            text='𝑥',
            showarrow=True,
            arrowhead=1,
            arrowsize=1.2*scale,
            arrowwidth=line_width,
            arrowcolor=color,
            font={'size': font_size, 'color': color}
        )
    fig.add_annotation(
            ax=x,  # arrows' tail
            x=x,  # arrows' head
            ay=y-0.8*scale,  # arrows' tail
            y=y,  # arrows' head
            arrowside='start',
            xref='x',
            yref=yref,
            axref="x",
            ayref=yref,
            text='𝓏',
            showarrow=True,
            arrowhead=1,
            arrowsize=1.2*scale,
            arrowwidth=line_width,
            arrowcolor=color,
            font={'size': font_size, 'color': color}
        )

    return fig

# 3D diagram axis convention
def axis_3d(fig, origin=[0,1,2], size=5, u=[0,0,1], v=[0,1,0], w=[0,0,1], labels =['𝑥','𝑦','𝑧'], color='#FFFFFF', font_color='grey', font_size=20):

    # Prepare arrays
    heads = np.array([
        origin,
        origin,
        origin
    ])
    heads = heads + np.array([u,v,w])*size

    # Plot the arrow tail
    for i in range(3):
        line = go.Scatter3d( 
            x = [origin[0], heads[i,0]],
            y = [origin[1], heads[i,1]],
            z = [origin[2], heads[i,2]],
            mode='lines',
            line = dict(color = color,
                        width = 4),
            hoverinfo='skip',
            showlegend=False
        )
        fig.add_trace(line)

    # Plot the arrow head
    cone = go.Cone(
        x=heads[:,0], 
        y=heads[:,1], 
        z=heads[:,2],
        u=u,
        v=v,
        w=w,
        text=labels,
        name='Axes convention',
        hovertemplate='%{text}',
        showscale=False,
        colorscale=single_colorscale(color),
        sizemode='scaled',
        sizeref=0.2
    )
    fig.add_trace(cone)

    # Plot the text labels
    annotations=[
        dict(
            showarrow=False,
            x=heads[i,0],
            y=heads[i,1],
            z=heads[i,2],
            text=labels[i],
            xanchor="left",
            xshift=8,
            yshift=0,
            opacity=0.8,
            font={'size': font_size, 'color': font_color}
        )
        for i in range(3)
    ]
    fig.update_layout(scene = dict(annotations=annotations),)

    return fig

# 3D rectangular beam generation
def rectangular_beam(hor, ver, lon ,n_points=10, step=0.5):
    cross_lon = np.arange(-lon/2, lon/2+0.1, step)
    cross_ver1 = -np.ones(n_points)*ver/2
    cross_ver2 = np.linspace(-ver/2, ver/2, n_points)
    cross_ver = np.concatenate([cross_ver1, cross_ver2, -cross_ver1, -cross_ver2])
    cross_hor1 = np.linspace(-hor/2, hor/2, n_points)
    cross_hor2 = np.ones(n_points)*hor/2
    cross_hor = np.concatenate([cross_hor1, cross_hor2, -cross_hor1, -cross_hor2])

    cross_hor = np.meshgrid(cross_hor, cross_lon)[0]
    cross_ver, cross_lon = np.meshgrid(cross_ver, cross_lon)

    return cross_hor, cross_ver, cross_lon

# 3D circular beam generation
def cylinder_beam(r, l, a=0, n_points=100, n_sections =50):
    """
    parametrize the cylinder of radius r, height h, base point a
    """
    theta = np.linspace(0, 2*np.pi, n_points)
    v = np.linspace(a, a+l, n_sections)
    theta, v = np.meshgrid(theta, v)
    z = r*np.cos(theta)
    y = r*np.sin(theta)
    x = v
    
    return x, y, z


# Diagram dimensions (go.Scatter)
def dimension_scatter(x, y, text, orientation, color, font_size):
    settings = {
        'h':{
            'marker_symbol':['triangle-left','0','triangle-right'],
            'textposition':'top center',
        },
        'v':
        {
            'marker_symbol':['triangle-down','0','triangle-up'],
            'textposition':'middle right',
        },
    }
    arrow = go.Scatter(
        x=x,
        y=y,
        mode="lines+markers+text",
        marker_symbol=settings[orientation]['marker_symbol'],
        marker=dict(
            size=[8,0,8],
            color=color,
            opacity=1,
            line=dict(width=0)
        ),
        text=[' ',text,' '],
        textposition=settings[orientation]['textposition'],
        textfont=dict(
            size=font_size,
            color=color
        ),
        hoverinfo='skip',
    )

    return arrow

# Diagram loads (go.Scatter)
def load_scatter(x, y, text, orientation, color, font_size):
    settings = {
        'h':{
            'marker_symbol':['0','triangle-right'],
            'textposition':'middle left',
        },
        'v':
        {
            'marker_symbol':['0','triangle-down'],
            'textposition':'top center',
        },
    }
    arrow = go.Scatter(
        x=x,
        y=y,
        mode="lines+markers+text",
        marker_symbol=settings[orientation]['marker_symbol'],
        marker=dict(
            size=[0,8],
            color=color,
            opacity=1,
            line=dict(width=0)
        ),
        text=[text, ' ', ' '],
        textposition=settings[orientation]['textposition'],
        textfont=dict(
            size=font_size,
            color=color
        ),
        hoverinfo='skip',
    )

    return arrow

class LoadGenerator:
    def __init__(self, T_id, app_id):
        self.T_id = T_id
        self.app_id = app_id
        self.id_gen = 0

    # New load menu
    def new_load(self, load_id, point_disabled=False, dist_disabled=False, edit_disabled=False):
        if edit_disabled == True:
            alert = dbc.Alert(
                "Maximum loads reached!",
                id=f"alert-auto-{self.app_id}",
                is_open=True,
                dismissable=True,
                duration=4000,
                color='info',
                class_name='mt-2'
            )
        else:
            alert = html.Div()
        load = html.Div(
            [
                dbc.InputGroup(
                    [
                        dbc.InputGroupText(html.B('...', id={"type": f"load-label-{self.app_id}", "index": load_id})),
                        dbc.Select(
                            id = {"type": f"load-type-{self.app_id}", "index": load_id},
                            options=[
                                {"label": T['apps'][self.app_id]['point_load'][self.T_id], "value": 'point' ,"disabled": point_disabled},
                                {"label": T['apps'][self.app_id]['distributed_load'][self.T_id], "value": 'distributed', "disabled": dist_disabled},
                            ],
                            value='point' if not point_disabled else 'distributed',
                            disabled=edit_disabled,
                        ),
                        html.Div(
                            [
                                dbc.Button(
                                    html.I(className="fa fa-trash"),
                                    id={"type": f"load-delete-{self.app_id}", "index": load_id},
                                    color="danger", 
                                    outline=True, 
                                    active=False,
                                    size="md", 
                                    style={'display':'none'}, 
                                ),
                                dbc.Button(
                                    html.I(className="fa fa-plus", id={"type": f"load-edit-icon-{self.app_id}", "index": load_id}),
                                    id={"type": f"load-edit-{self.app_id}", "index": load_id},
                                    color="primary", 
                                    outline=True, 
                                    active=False,
                                    size="md",
                                    disabled=edit_disabled 
                                )
                            ]
                        )
                    ],
                    className="mt-3"
                ),
                dbc.Collapse(
                    [
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText(T['apps'][self.app_id]['magnitude'][self.T_id]),
                                dbc.Input(
                                    id={"type": f"load-magnitude-{self.app_id}", "index": load_id},
                                    type="number", 
                                    min=0, 
                                    max=20, 
                                    step=5, 
                                    placeholder="[1, 20]",
                                    style={'display': 'inline-block'},
                                ),
                                dbc.InputGroupText('kN', id={"type": f"load-magnitude-units-{self.app_id}", "index": load_id}),
                            ],
                            className="mt-1"
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText(T['apps'][self.app_id]['position'][self.T_id]),
                                dbc.Select(
                                    id = {"type": f"load-position-{self.app_id}", "index": load_id},
                                    options=[
                                        {'label':'0', 'value':0},
                                        {'label':'ℓ/8', 'value':0.125},
                                        {'label':'ℓ/4', 'value':0.25},
                                        {'label':'3ℓ/8', 'value':0.375},
                                        {'label':'ℓ/2', 'value':0.5},
                                        {'label':'5ℓ/8', 'value':0.625},
                                        {'label':'3ℓ/4', 'value':0.75},
                                        {'label':'7ℓ/8', 'value':0.875},
                                        {'label':'ℓ', 'value':1}
                                    ],
                                    #value='0'
                                ),  
                                dbc.InputGroupText('m'),
                            ],
                            className="mt-1",
                            id={"type": f"load-position-form-{self.app_id}", "index": load_id},
                        ),
                    ],
                    is_open=False,
                    navbar=True,
                    id={"type": f"load-form-{self.app_id}", "index": load_id},
                ),
                alert
            ],
            id={"type": f"load-{self.app_id}", "index": load_id}
        )

        return  load


    # Create load database and container
    def create_db(self):
        load_db = dcc.Store(
            id = f'load-db-{self.app_id}',
            data = {
                '𝐹₁' : 'free',
                '𝐹₂' : 'free',
                '𝐹₃' : 'free',
                '𝐹₄' : 'free',
                '𝐹₅' : 'free',
                '𝑞'  : 'free'  
            }
        )

        load_container = html.Div(
            [
                load_db,
                self.new_load(self.app_id, 0),
            ], 
            id=f'load-container-{self.app_id}'
        )

        return load_db, load_container
    
    # Edit existing load
    def load_edit_signals(self):
        return {
            'Output' : [
                Output( {'type': f'load-form-{self.app_id}', 'index': MATCH}, 'is_open'),
                Output({'type': f'load-delete-{self.app_id}', 'index': MATCH}, 'style'),
                Output({'type': f'load-edit-{self.app_id}', 'index': MATCH}, 'color'),
                Output({'type': f'load-edit-icon-{self.app_id}', 'index': MATCH}, 'className'),
                Output({'type': f'load-type-{self.app_id}', 'index': MATCH}, 'disabled'),
                Output({'type': f'load-position-form-{self.app_id}', 'index': MATCH}, 'style'),
                Output({'type': f'load-magnitude-units-{self.app_id}', 'index': MATCH}, 'children'),
                Output({'type': f'load-magnitude-{self.app_id}', 'index': MATCH}, 'max'),
                Output({'type': f'load-magnitude-{self.app_id}', 'index': MATCH}, 'placeholder'),
                Output({'type': f'load-magnitude-{self.app_id}', 'index': MATCH}, 'step'),
            ],
            'Input' : Input({'type': f'load-edit-{self.app_id}', 'index': MATCH}, 'n_clicks'),
            'State': [
                State({'type': f'load-type-{self.app_id}', 'index': MATCH}, 'value'),
                State({'type': f'load-form-{self.app_id}', 'index': MATCH}, 'is_open'),   
            ]
        }
    def load_edit(self, edit_clicks, load_type, is_open):
        context = dash.callback_context.triggered[0]['prop_id'].split('.')[0]
        if edit_clicks == None or context =='':
            raise PreventUpdate

        # First action
        if load_type == 'point': # Point load
            pos_display = 'flex'
            mag_units = 'kN'
            mag_max = 20
            mag_placeholder = '[1, 20]'
            mag_step = 5          

        else: # Distributed load
            pos_display = 'none'
            mag_units = 'kN/m'
            mag_max = 10
            mag_placeholder = '[1, 10]'
            mag_step = 2


        # Other actions   
        load_disabled = True

        if (edit_clicks % 2) == 0: # collapsed mode
            del_display = 'none'
            action_color = 'primary'
            action_icon = 'fa fa-pen'

        else: # expanded mode
            del_display = 'inline' if edit_clicks != 1 else 'none'
            action_color = 'success'
            action_icon = 'fa fa-check'

        return (
            not is_open,                #load-form
            {'display': del_display},   #load-delete
            action_color,               #load-edit
            action_icon,                #load-edit-icon
            load_disabled,              #load-type
            {'display': pos_display},   #load-position-form
            mag_units,                  #load-magnitude-units
            mag_max,                    #load-magnitude
            mag_placeholder,            #load-magnitude
            mag_step                    #load-magnitude
        )
    

    # Add new load + delete existing load
    def add_new_load_signals(self):
        return {
            'Output' : [
                Output(f'load-container-{self.app_id}','children'),
                Output({'type': f'load-label-{self.app_id}', 'index': ALL}, 'children'),
                Output(f'load-db-{self.app_id}','data')
            ],
            'Input' : [
                Input( {'type': f'load-form-{self.app_id}', 'index': ALL}, 'is_open'),
                Input( {'type': f'load-delete-{self.app_id}', 'index': ALL}, 'n_clicks'),
            ],
            'State': [
                State({'type': f'load-edit-{self.app_id}', 'index': ALL}, 'n_clicks'),
                State({'type': f'load-label-{self.app_id}', 'index': ALL}, 'children'),
                State(f'load-db-{self.app_id}','data'),
                State({'type': f'load-type-{self.app_id}', 'index': ALL}, 'value'),
                State(f'load-container-{self.app_id}', 'children'),
            ]
        }
    def add_new_load(self, is_open, del_clicks, edit_clicks, label_list, load_db, load_type, container):
        
        load_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0]

        # Avoid callback when app opens
        if load_id == '':
            raise PreventUpdate
        
        # General metadata about the trigger
        load_id = json.loads(load_id)
        trigger = load_id["type"] 
        load_id = load_id["index"]
        last_position = len(label_list) -1  

        # Update last load
        if trigger==f'load-form-{self.app_id}':

            # Matching Input/State with last load ID
            is_open = is_open[last_position]
            n_clicks = edit_clicks[last_position]
            load_type = load_type[last_position]

            # Prevent triggering unwated callbacks
            condition = ((is_open == False)&(n_clicks == 2))
            if not condition:
                raise PreventUpdate 

            # Labeling the select load
            if load_type == 'point':
                free_labels = {label:id for label,id in load_db.items() if 'free' in str(id)} # Filter only free labels
                label = min(free_labels.keys()) # Get minimum label available
                
            elif load_type == 'distributed': 
                label = '𝑞'

            label_list[last_position] = label # Update label
            load_db[label] = load_id  # Update label metadata with matching ID

        # Delete existing load
        elif trigger==f'load-delete-{self.app_id}':    
            container = [
                load
                for load in container
                if load['props']['id']!={"type": f"load-{self.app_id}", "index": load_id}
            ]
            label = dict(map(reversed, load_db.items()))[load_id] # Find label using ID
            load_db[label] = 'free' # Set label to be available again
            container = container[:-1] # Delete last element to "refresh" it


        # Add new load to the container
        edit_disabled = ('free' not in load_db.values()) # Disable editing if no more loads are available

        point_disabled = all([id!='free'  for load,id in load_db.items() if '𝐹' in load]) # Condition to disable point-loads
        dist_disabled = isinstance(load_db['𝑞'], int) # Condition to disable distributed-loads
        self.id_gen += 1
        load = self.new_load(self.id_gen , point_disabled=point_disabled, dist_disabled=dist_disabled, edit_disabled=edit_disabled)
        container.append(load)

        return container, label_list, load_db


    # Load input form validation
    def valid_input_signals(self):
        return {
            'Output' : Output({'type': f'load-edit-{self.app_id}', 'index': MATCH}, 'disabled'),
            'Input' : [
                Input({'type': f'load-edit-{self.app_id}', 'index': MATCH}, 'color'),
                Input({"type": f"load-magnitude-{self.app_id}", "index": MATCH}, 'value'),
                Input({"type": f"load-position-{self.app_id}", "index": MATCH}, 'value'),
            ],
            'State': [
                State({'type': f'load-type-{self.app_id}', 'index': MATCH}, 'value'),
                State({"type": f"load-magnitude-{self.app_id}", "index": MATCH}, 'min'),
                State({"type": f"load-magnitude-{self.app_id}", "index": MATCH}, 'max')
            ]
        }
    def valid_input(self, edit_color, mag_value, pos_value, load_type, mag_min, mag_max):

        if (edit_color=='primary'):
            raise PreventUpdate

        mag_value = 0 if mag_value == None else mag_value 
        
        edit_disabled = (mag_value<mag_min) or (mag_value>mag_max) or mag_value==0
        
        if load_type =='point':
            edit_disabled = pos_value==None or edit_disabled

        return edit_disabled






