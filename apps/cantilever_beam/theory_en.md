A static system is in equilibrium if its dyname vanishes with respect to each reference point $O$ (Chapter 3.2):

$$
\begin{aligned}
\underline{\mathcal{R}} &= \sum_i \underline{F}_i = \underline{0} \\
\underline{\mathcal{M}} &= \sum_i \underline{OA}_i \times \underline{F}_i + \sum_j \underline{\Gamma}_j= \underline{0}
\end{aligned}
$$

In three-dimensional space, support reactions can be determined with the help of the three equilibria for translation $\mathcal{R}_x=\mathcal{R}_y=\mathcal{R}_z=0$ and the three equilibria for rotation $\mathcal{M}_x=\mathcal{M}_y=\mathcal{M}_z=0$. In the two-dimensional case, only the two translational equilibria $\mathcal{R}_x=\mathcal{R}_z=0$, and the rotational equilibrium $\mathcal{M}_y=0$ are needed.