from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Active buttons color
toggle_list = ['horizontal', 'vertical', 'moment']
sign_list = ['negative', 'disabled', 'positive']

for toggle in toggle_list:
    @app.callback(
        [Output(f"btn-{toggle}-{sign}", "active") for sign in sign_list],
        [Input(f"btn-{toggle}-{sign}", "n_clicks") for sign in sign_list],
    )
    def set_active(n1, n2, n3, toggle=toggle):
        if not any([n1, n2, n3]):
            raise PreventUpdate

        ctx = dash.callback_context
        button_id = ctx.triggered[0]["prop_id"].split(".")[0]

        return [button_id == f"btn-{toggle}-{sign}" for sign in sign_list]


# Update formulas
@app.callback(
    Output('latex-formulas', 'children'),
    [Input(f"btn-{toggle}-{sign}", "active") for toggle in toggle_list for sign in sign_list],
    [State('latex-formulas','children')]
)
def update_formula(*args):
    combination = list(args[:-1])
    latex_components =  args[-1]

    for i in range(len(latex_components)):
        if latex_components[i]['props']['id']== f'eq-{str(combination)}':
            latex_components[i]['props']['style'] = {'display': 'block'}
        else:
            latex_components[i]['props']['style'] = {'display': 'none'}

    return latex_components


## Update figure
@app.callback(
    Output('graph-cantilever', 'figure'),
    [
        State('T_id', 'data'),
        Input('support-mode-2', 'value'),
    ] +
    [Input(f"btn-{toggle}-{sign}", "active") for toggle in toggle_list for sign in sign_list]
)
def update_figure(T_id, support_mode, *args):
    trace_beam = go.Scatter(
        x=[0, 10],
        y=[0, 0],
        mode='lines',
        line=dict(width=style['line_width']['xl']),
        marker=dict(size=10,
                    color=style['color']['primary'])
    )

    trace_labels = go.Scatter(
        x=[0, 10],
        y=[0, -0.1],
        mode='text',
        text=[' A', 'B'],
        textposition=['top right', 'bottom left'],
        textfont=dict(
            color=style['color']['primary'],
            size=style['font_size']['xl']
        )
    )

    data = [trace_beam, trace_labels]
    layout = go.Layout(
        xaxis=dict(
            domain=[0, 1],
            range=[-2.5, 12.5],
            tickmode='array',
            showgrid=False,
            zeroline=False,
            showticklabels=False
                      
        ),
        yaxis=dict(
            domain=[0, 1],
            range=[-0.5, 0.5],
            scaleanchor='x',
            scaleratio=1,
            showgrid=False,
            zeroline=False,
            showticklabels=False
        ),
    )

    fig = go.Figure(data=data, layout=layout)

    # Axis convention
    fig = helper.axis(
        fig, 
        line_width=style['line_width']['md'], 
        color=style['color']['primary'], 
        font_size=style['font_size']['lg'], 
        x=-2, 
        y=1
    )

    # SVG paths ETL
    geom_path='assets/geometries'
    FixedSupport = helper.SVGPath()
    FixedSupport.reader(f'{geom_path}/fixed_support.svg')
    FixedSupport.scale(x=0.01,y=0.01)
    CurveA = helper.SVGPath()
    CurveA.reader(f'{geom_path}/curved_arrow_a.svg')
    CurveB = helper.SVGPath()
    CurveB.reader(f'{geom_path}/curved_arrow_b.svg') 

    #F1 Arrow
    if args[1]==False:
        fig.add_annotation(
            x=10.1,  # arrows' head
            y=-0,  # arrows' head
            ax=11,  # arrows' tail
            ay=0,  # arrows' tail
            arrowside='start' if args[2]==True else 'end',
            xref='x',
            yref='y',
            axref="x",
            ayref="y",
            text='𝐹₁',
            showarrow=True,
            arrowhead=1,
            #arrowsize=1.5,
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['danger'],
            font={'size': style['font_size']['lg'], 'color': style['color']['danger']}
        )
    else:
        pass

    # F2 Arrow
    if args[4]==False:
        fig.add_annotation(
            x=10,  # arrows' head
            ax=10,  # arrows' tail
            y=0.1,  # arrows' head
            ay=1,  # arrows' tail
            xref='x',
            yref='y',
            axref="x",
            ayref="y",
            text='𝐹₂',
            showarrow=True,
            arrowside='end' if args[5]==True else 'start',
            arrowhead=1,
            #arrowsize=1.5,
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['danger'],
            font={'size': style['font_size']['lg'], 'color': style['color']['danger']}
        )
    else:
        pass

    # M arrow
    if args[7]==False:
        fig.add_annotation(
            x=10.9 if args[8]==False else 10.25,  # arrows' head
            ax=10.9 if args[8]==False else 10.35,  # arrows' tail
            y=0.2 if args[8]==False else 0.9,  # arrows' head
            ay=0.5 if args[8]==False else 0.9,  # arrows' tail
            xref='x',
            yref='y',
            axref="x",
            ayref="y",
            showarrow=True,
            arrowhead=1,
            #arrowsize=1.5,
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['danger'],
            font={'size': style['font_size']['lg'], 'color':style['color']['danger']}
        )
        fig.add_shape(
            type='path',
            path=CurveB.path,
            fillcolor=None,
            line_color=style['color']['danger'],
            line={'width': style['line_width']['md']},
        )
        fig.add_annotation(
            x=11, y=0.8,
            text="𝛤",
            showarrow=False,
            font={'size': style['font_size']['lg'], 'color': style['color']['danger']}
        )

    if support_mode == False:
        # Supports
        fig.add_shape(
            type='path',
            path=FixedSupport.path,
            fillcolor=None,
            line_color=style['color']['primary'],
            line={'width': style['line_width']['sm']},
            )

        fig.update_layout(title=T['apps'][app_id]['static_model'][T_id])

    else:
        # Ah
        if args[1]==False:
            fig.add_annotation(
                x=-0.1,  # arrows' head
                ax=-1,  # arrows' tail
                y=0,  # arrows' head
                ay=0,  # arrows' tail
                arrowside='end',
                xref='x',
                yref='y',
                axref="x",
                ayref="y",
                text='𝛢<sub>H',
                showarrow=True,
                arrowhead=1,
                #arrowsize=1.5,
                arrowwidth=style['line_width']['md'],
                arrowcolor=style['color']['success'],
                font={'size': style['font_size']['lg'], 'color': style['color']['success']}
            )
        else:
            pass

        # Av
        if args[4]==False:
            fig.add_annotation(
                x=0,  # arrows' head
                ax=0,  # arrows' tail
                y=-0.1,  # arrows' head
                ay=-1,  # arrows' tail
                arrowside='start',
                xref='x',
                yref='y',
                axref="x",
                ayref="y",
                text='𝛢<sub>V',
                showarrow=True,
                arrowhead=1,
                #arrowsize=1.5,
                arrowwidth=style['line_width']['md'],
                arrowcolor=style['color']['success'],
                font={'size': style['font_size']['lg'], 'color': style['color']['success']}
            )
        else:
            pass

        # Gamma
        if all([args[i] for i in [4,7]])==False:
            fig.add_annotation(
                x=-0.2,  # arrows' head
                ax=-0.4,  # arrows' tail
                y=-0.9,  # arrows' head
                ay=-0.9,  # arrows' tail
                xref='x',
                yref='y',
                axref="x",
                ayref="y",
                showarrow=True,
                arrowhead=1,
                #arrowsize=1.5,
                arrowwidth=style['line_width']['md'],
                arrowcolor=style['color']['success'],
                font={'size': style['font_size']['lg'], 'color': style['color']['success']}
            )
            fig.add_shape(
                type='path',
                path=CurveA.path,
                fillcolor=None,
                line_color=style['color']['success'],
                line={'width': style['line_width']['md']},
            )
            fig.add_annotation(
                x=-1, y=-0.8,
                text="𝛤<sub>A",
                showarrow=False,
                font={'size': style['font_size']['lg'], 'color': style['color']['success']}
            )
        else:
            pass


        fig.update_layout(title=T['apps'][app_id]['free_body'][T_id])
        pass

    fig.update_layout(
        height=200,
        showlegend=False,
        font=dict(size=style['font_size']['sm']),
        margin=dict(l=20, r=20, t=40, b=20),
        #paper_bgcolor="LightSteelBlue",
        #plot_bgcolor='rgba(236,240,241,0.2)',
    )

    fig.update_traces(hoverinfo='skip')
    fig.update_yaxes(fixedrange=True)

    return fig
