Ein statisches System ist im Gleichgewicht, wenn seine Dyname bezüglich jedes Referenzpunktes $O$ verschwindet (Kapitel 3.2):

$$
\begin{aligned}
\underline{\mathcal{R}} &= \sum_i \underline{F}_i = \underline{0} \\
\underline{\mathcal{M}} &= \sum_i \underline{OA}_i \times \underline{F}_i + \sum_j \underline{\Gamma}_j= \underline{0}
\end{aligned}
$$

Im dreidimensionalen Raum können dadurch Lagerreaktionen mithilfe der drei Gleichgewichte für Translation $\mathcal{R}_x=\mathcal{R}_y=\mathcal{R}_z=0$ und der drei Gleichgewichte für Rotation $\mathcal{M}_x=\mathcal{M}_y=\mathcal{M}_z=0$ bestimmt werden. Im zweidimensionalen Fall werden nur die zwei Translationsgleichgewichte  $\mathcal{R}_x=\mathcal{R}_z=0$, sowie das Rotationsgleichgewicht $\mathcal{M}_y=0$ benötigt.
