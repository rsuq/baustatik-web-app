from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Layout
def layout(T_id):
    neg = [True, False, False]
    none = [False, True, False]
    pos = [False, False, True]
    f1 = [neg, none,pos]
    f2 = [neg, none,pos]
    m = [neg, none,pos]
    combinations = list(itertools.product(f1, f2, m))
    combinations = [list(itertools.chain(*c)) for c in combinations]

    latex_components=[]
    i=1
    for combination in combinations:
        component = html.Div(solvers.equilibrium(T_id, app_id, *combination),id = f'eq-{str(combination)}',style={'display': 'none'})

        if i==len(combinations):
            component.style={'display': 'block'}
        else:
            pass 

        latex_components.append(component)
        i+=1

    formulas = html.Div(latex_components, id = 'latex-formulas')



    toggles = {}
    for toggle in ['horizontal', 'vertical', 'moment']:
        toggles[toggle] = html.Div([
                dbc.ButtonGroup([
                dbc.Button(html.I(className="fa fa-minus"), id=f"btn-{toggle}-negative", color="info", outline=True, active=False,size="sm"),
                dbc.Button(html.Div('OFF', style={'font-weight':'bold'}), id=f"btn-{toggle}-disabled", color="secondary", active=False, outline=True,size="sm"),
                dbc.Button(html.I(className="fa fa-plus"), id=f"btn-{toggle}-positive", color="info", outline=True, active=True,size="sm"),
            ])
        ],className='d-grid gap-2 col-11 mx-auto')

    support_toggle = dbc.Switch(
        id='support-mode-2',
        label=T['apps'][app_id]['show_reactions'][T_id],
        value=False,
        style= {'display': 'flex','justify-content': 'center'},
        label_style ={'margin-left': '10px'}
    )

    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = html.Div([
        html.Br(),
        html.Div(
            [
                dcc.Markdown(T['apps'][app_id]['horizontal_force'][T_id], style={'display': 'inline-block', 'margin-right': 4}),  
                '$\\boldsymbol{F_{1}}$',
            ],
            style={'textAlign': "center"}
        ),
        toggles['horizontal'],
        html.Br(),
        html.Div(
            [
                dcc.Markdown(T['apps'][app_id]['vertical_force'][T_id], style={'display': 'inline-block', 'margin-right': 4}),  
                '$\\boldsymbol{F_{2}}$',
            ],
            style={'textAlign': "center"}
        ),
        toggles['vertical'],
        html.Br(),
       html.Div(
            [
                dcc.Markdown(T['apps'][app_id]['external_moment'][T_id], style={'display': 'inline-block', 'margin-right': 4}),   
                '$\\boldsymbol{\Gamma}$',
            ],
            style={'textAlign': "center"}
        ),
        toggles['moment'],
        html.Br(),
        html.Br(),
        support_toggle
    ])

    graphs = html.Div([
        html.Br(),
        dcc.Graph(config={'displayModeBar':False},id='graph-cantilever'),
        html.Br(),
        formulas
    ])

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout