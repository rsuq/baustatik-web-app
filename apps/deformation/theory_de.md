Die Euler-Bernoulli Balkentheorie ist die gebräuchlichste Theorie zur berechnung der Verformung elastischer Balken. Sie beruht auf der Annahme, dass der Balkenquerschnitt unter Last eben und senkrecht zur Stabachse bleibt. Diese Annahmen setzten voraus, dass Querkräfte keinen Beitrag zur Verformung $w$ leisten:
$$
    EI_y w'''' = q_z 
$$
Soll die Verformung infolge Querkraft mitberücksichtig werden, ist die Timoshenko Balkentheorie zu verwenden. Zwar setzt auch diese Theorie voraus, dass der Querschnitt eben bleibt, jedoch wird eine Schiebung des Querschnitts infolge Querkraft mitberücksichtigt, wodurch der Querschnitt nicht mehr senkrecht zur Stabachse bleibt:
$$
    EI_y w'''' = q_z - \frac{EI}{GA_v} q_z''
$$
Daraus folgt auch, dass die Verformung nach Timoshenko gleich oder grösser der Verformung nach Euler-Bernoulli ist. Dieser Unterschied wird signifikant, wenn der Querschnitt gedrungen ist, also ein grosses Verhältnis Höhe/Länge aufweist.
Die Herleitung der beiden Theorien kann in Kapitel 3 nachgelesen werden.