The Euler-Bernoulli beam theory is the most common theory for calculating the deformation of elastic beams. It is based on the assumption that the beam cross-section under load remains plane and perpendicular to the beam axis. These assumptions impose that shear forces do not contribute to the deformation $w$:
$$
    EI_y w'''' = q_z 
$$
If the deformation due to shear is to be taken into account, the Timoshenko beam theory must be used. Also this theory assumes that the cross-section remains plane. However, a displacement of the cross-section due to shear force is also taken into account, which results in the cross-section no longer remaining perpendicular to the member axis:
$$
    EI_y w'''' = q_z - \frac{EI}{GA_v} q_z''
$$
It also follows that the deformation according to Timoshenko is equal to or greater than the deformation according to Euler-Bernoulli. This difference becomes significant if the cross section is stocky, i.e. has a large height/length ratio.
The derivation of the two theories can be found in chapter 3.