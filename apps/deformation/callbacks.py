from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Calbacks

# Buttons color
@app.callback(
        Output({'type':f'supports-{app_id}', 'index': ALL}, 'active'),
        Input({'type':f'supports-{app_id}', 'index': ALL}, 'n_clicks'),
        prevent_initial_call=True
    )
def set_active(n_clicks):
    # General metadata about the trigger
    btn_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0] 
    btn_id = json.loads(btn_id)
    btn_id = btn_id['index']

    # Activate only the clicked button
    active = [False, False, False, False]
    active[btn_id] = True

    return active

space = True
# Update figure
@app.callback(
    Output(f'graph-{app_id}', 'figure'),
    Output(f'graph-moment-{app_id}', 'figure'),
    Output(f'graph-rotation-{app_id}', 'figure'),
    Output(f'graph-deflection-{app_id}', 'figure'),
    Output(f'graph-ratios-{app_id}', 'figure'),
    State('T_id', 'data'),
    Input({'type':f'supports-{app_id}', 'index': ALL}, 'active'),
    Input(f'lambda-input-{app_id}', 'value'),
    Input(f'q-input-{app_id}', 'value'),
    Input(f'refresh-range-{app_id}', 'n_clicks'),
    State(f'graph-{app_id}', 'figure'),
    State(f'graph-moment-{app_id}', 'figure'),
    State(f'graph-rotation-{app_id}', 'figure'),
    State(f'graph-deflection-{app_id}', 'figure'),
    State(f'graph-ratios-{app_id}', 'figure')
)
def update_figure(T_id, active_support, this_lambda, q, refresh_range, fig_beam, fig_moment, fig_rotation, fig_deflection, fig_ratios):
    global space
    # Get callback context
    context = dash.callback_context.triggered[0]['prop_id'].split('.')[0]

    # Get figure state layouts
    fig_beam_layout = fig_beam['layout']
    fig_beam_layout.pop('annotations', None)
    fig_beam_layout.pop('shapes', None)
    fig_moment_layout = fig_moment['layout']
    fig_moment_layout.pop('annotations', None)
    fig_moment_layout.pop('shapes', None)
    fig_rotation_layout = fig_rotation['layout']
    fig_rotation_layout.pop('annotations', None)
    fig_rotation_layout.pop('shapes', None)
    fig_deflection_layout = fig_deflection['layout']
    fig_deflection_layout.pop('annotations', None)
    fig_deflection_layout.pop('shapes', None)
    fig_ratios_layout = fig_ratios['layout']
    fig_ratios_layout.pop('annotations', None)
    fig_ratios_layout.pop('shapes', None)

    # Initial parameters
    poisson = 0.3
    b=0.3 # m
    l = 10 # m
    h = this_lambda*l  
    q = q*1000 # kN.m to N.m
    E = 200e9 # Pa
    I = b*h**3/12 
    G = E/(2*(1+poisson))
    Av = 5*b*h/6

    ## Beam figure
    traces = []
    # Beam sketch trace
    trace_beam = go.Scatter(
        x=[0, 10],
        y=[0, 0],
        mode='lines+markers+text',
        text=['A', 'B'],
        fill='tonexty',
        textposition=['top right', 'top left'],
        line=dict(
            width=style['line_width']['lg'],
        ),
        marker=dict(size=10,
                    color=style['color']['primary']),
        textfont=dict(
            color=style['color']['primary'],
            size=style['font_size']['xl']
        ),
        hoverinfo='skip'
    )
    traces.append(trace_beam)

    trace_height = go.Scatter(
        x=[0, 10, 10, 0, 0],
        y=[0, 0, h ,h, 0],
        mode='lines',
        fill='tonexty',
        line=dict(
            width=1,
            color=style['color']['primary']
        ),
        hoverinfo='skip'
    )
    traces.append(trace_height)

    # Dimensions
    offset_tri = 0.1
    arrow_l = helper.dimension_scatter(
        x = [offset_tri, 5 ,10-offset_tri],
        y = [0-1.1]*3,
        text = f'𝑙={l}',
        orientation = 'h',
        color=style['color']['info'],
        font_size=style['font_size']['lg']
    )
    traces.append(arrow_l)

    arrow_h = helper.dimension_scatter(
        x = [10+0.3]*3,
        y = [offset_tri, h/2 ,h-offset_tri],
        text = f' ℎ={round(h,2)}',
        orientation = 'v',
        color=style['color']['info'],
        font_size=style['font_size']['lg']
    )
    traces.append(arrow_h)

    # Disributed load
    dist_scale = 0.07/1000
    offset_load = 0.4
    for step in range(11):
        load = helper.load_scatter(
            x = [step]*2,
            y = [h+q*dist_scale+offset_load, h+offset_load],
            text = '𝑞' if step==5 else '',
            orientation = 'v',
            color=style['color']['danger'],
            font_size=style['font_size']['lg']
        )
        traces.append(load)

        tranversal_line = go.Scatter(
            x=[0, 10],
            y=[h+q*dist_scale+offset_load-0.015, h+q*dist_scale+offset_load-0.015],
            mode='lines',
            line=dict(color=style['color']['danger']),
            hoverinfo='skip',
        )
        traces.append(tranversal_line)

    fig = go.Figure(data=traces, layout=fig_beam_layout)
    
    # Supports
    left_support = ['fixed', 'fixed', 'fixed', 'pinned'][active_support.index(True)]
    right_support = ['fixed', 'roller', 'free-end', 'roller'][active_support.index(True)]

    path='assets/geometries'
    support_scale=0.008
    translate = 10
    LeftSupport = helper.SVGPath(left_support)
    LeftSupport.reader(f'{path}/{left_support}_support.svg')
    LeftSupport.scale(x=support_scale, y=support_scale)

    RightSupport = helper.SVGPath(right_support)
    RightSupport.reader(f'{path}/{right_support}_support.svg')
    RightSupport.scale(x=support_scale, y=support_scale)
    RightSupport.translate(x=translate)
    if right_support == 'fixed':
        RightSupport.flip()

    # Display supports
    fig.add_shape(
        type='path',
        path=LeftSupport.path+RightSupport.path,
        fillcolor=None,
        line_color=style['color']['primary'],
        line={'width': style['line_width']['sm']},
        yref='y',
        layer='below'
    )


    ## Moment/rotation/deflection figures
    x = np.arange(0,l+0.1,0.1)
    solutions = solvers.beam_theory([left_support, right_support], l, h, E, I, G, Av, q, x)
    solutions['Euler-Bernoulli']['color'] = style['color']['warning']
    solutions['Euler-Bernoulli']['dash'] = None
    solutions['Timoshenko']['color'] = style['color']['primary']
    solutions['Timoshenko']['dash'] = 'dash'

    # Moment figure
    traces_moment = []
    for name, solution in solutions.items():
        y = np.array(solution['moment'])/1000 # kN.m
        trace = go.Scatter(
            x=x,
            y=y,
            #fill='tozeroy',
            line_shape='spline',
            mode='lines',
            line=dict(color=solution['color'], dash=solution['dash']),
            hovertemplate='<b>%{y:.1f} kN.m',
            name=name
        )
        traces_moment.append(trace)

    fig_moment = go.Figure(data=traces_moment, layout=fig_moment_layout)

    # Rotation figure
    traces_rotation = [] 
    for name, solution in solutions.items():
        y = solution['rotation']
        trace = go.Scatter(
            x=x,
            y=y,
            hovertemplate='<b>%{y} °',
            #fill='tozeroy',
            line_shape='spline',
            mode='lines',
            line=dict(color=solution['color'], dash=solution['dash']),
            name=name
        )
        traces_rotation.append(trace)

    fig_rotation = go.Figure(data=traces_rotation, layout=fig_rotation_layout)

    # Deflection figure
    traces_deflection = []
    for name, solution in solutions.items():
        y = solution['deflection']*1000 # mm
        trace = go.Scatter(
            x=x,
            y=y,
            hovertemplate='<b>%{y:.2} mm',
            #fill='tozeroy',
            line_shape='spline',
            mode='lines',
            line=dict(color=solution['color'], dash=solution['dash']),
            name=name
        )
        traces_deflection.append(trace)

    fig_deflection = go.Figure(data=traces_deflection, layout=fig_deflection_layout)

    # Calculate and display ratios
    w_eb_max = max(abs(solutions['Euler-Bernoulli']['deflection']))
    w_ts_max = max(abs(solutions['Timoshenko']['deflection']))
    w_m_max = w_eb_max
    w_v_max = w_ts_max - w_m_max
    ratio_v_m = round(w_v_max/w_m_max, 2)
    ratio_eb_ts = round(w_eb_max/w_ts_max, 2)

    fig_ratios_layout['annotations']=[
        dict(
            x=0.3,  # arrows' head
            y=0,  # arrows' head
            text=f'<i>𝑤<sup> 𝑉</sup><sub>max</sub> /𝑤<sup> 𝑀</sup><sub>max</sub> = {ratio_v_m}',
            showarrow=False,
            xref="paper", yref="paper",
            font={'size': style['font_size']['xl'], 'color': style['color']['primary']},
            bordercolor=style['color']['primary'],
            borderwidth=style['line_width']['sm'],
            borderpad=4,
            bgcolor=style['color']['light'],
            opacity=0.8
        ),
        dict(
            x=0.02,  # arrows' head
            y=0,  # arrows' head
            text=f'<i>𝑤<sup> EB</sup><sub>max</sub> /𝑤<sup> TS</sup><sub>max</sub> = {ratio_eb_ts}',
            showarrow=False,
            xref="paper", yref="paper",
            font={'size': style['font_size']['xl'], 'color': style['color']['primary']}, 
            bordercolor=style['color']['primary'],
            borderwidth=style['line_width']['sm'],
            borderpad=4,
            bgcolor=style['color']['light'],
            opacity=0.8
        ),
    ]
    fig_ratios = go.Figure(layout=fig_ratios_layout)


    # Refresh ranges
    scaler=1.2
    if context==f'refresh-range-{app_id}':
        fig_moment.update_yaxes(range=[(solutions['Timoshenko']['moment'].max()/1000)*scaler , (solutions['Timoshenko']['moment'].min()/1000)*scaler])
        fig_rotation.update_yaxes(range=[solutions['Timoshenko']['rotation'].max()*scaler, solutions['Timoshenko']['rotation'].min()*scaler])
        fig_deflection.update_yaxes(range=[solutions['Timoshenko']['deflection'].max()*1000*scaler, solutions['Timoshenko']['deflection'].min()*1000*scaler])
 

    return fig, fig_moment, fig_rotation, fig_deflection, fig_ratios