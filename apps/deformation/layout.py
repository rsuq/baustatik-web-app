from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Layout
l = 10
def fig_line_layout(xaxis_title, yrange, height=200, margin_left=80, margin_bottom=10):
        layout = go.Layout(
            xaxis=dict(
                domain=[0, 1],
                range=[-1.1, l+1.1],
                tickmode='array',
                showgrid=True,
                zeroline=False,
                showticklabels=False,
                fixedrange=True,
                hoverformat='.2',
                            
            ),
            yaxis=dict(
                domain=[0, 1],
                range=yrange,
                #scaleanchor='x',
                #scaleratio=1,
                showgrid=True,
                zeroline=True,
                showticklabels=True,
                fixedrange=True,
                title=xaxis_title,
                #tickformat='.3',
                automargin=False
            ),
            showlegend=True,
            legend=dict(
                orientation='h',
                yanchor="top",
                y=1.2,
                xanchor="right",
                x=1
            ),
            hovermode='x unified',
            hoverlabel=dict(bgcolor='white', bordercolor=style['color']['light']),
            #paper_bgcolor= 'rgba(100,200,115,0.9)',
            plot_bgcolor= 'rgba(236,240,241,0.2)',
            transition={'duration': 500, 'easing': 'cubic-in-out'},
            margin=dict(l=margin_left, r=10, b=margin_bottom, t=1),
            font=dict(size=style['font_size']['sm']),
            height=height
        )
        return layout

def layout(T_id):
    ## Parameters

    # Length slider
    lambda_slider = dcc.Slider(
        id=f'lambda-input-{app_id}',
        min=0.01,
        max=0.2,
        value=0.1,
        step=0.01,
        marks={str((int(value) if value.is_integer() else value)): {'label': str(int(value) if value.is_integer() else round(value,2)), 'style': {'font-size': style['font_size']['sm']}}
            for value in np.linspace(0.01, 0.2, 4)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    q_slider = dcc.Slider(
        id=f'q-input-{app_id}',
        min=5,
        max=20,
        value=15,
        step=1,
        marks={str(value): {'label': str(value), 'style': {
                    'font-size': style['font_size']['sm']}} for value in range(0, 21, 5)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    refresh_range = dbc.Button(
            [
                html.I(className='fas fa-redo-alt me-2'),
                T['apps'][app_id]['axes_scale'][T_id]
        ],
        n_clicks=0,
        value=5,
        color='info', 
        className="me-1",
        id=f'refresh-range-{app_id}'
    )

    material = html.Div( 
        dbc.Toast(
            html.Div(
                [   
                    html.P(T['apps'][app_id]['young_modulus'][T_id], style={'font-weight': 'bold','margin-top': 5}),
                    html.P('E = 200 GPa', style={'margin-top': -15}),
                    html.P(T['apps'][app_id]['poisson_ratio'][T_id], style={'font-weight': 'bold'}),
                    html.P('ν = 0.3', style={'margin-top': -15, 'margin-bottom':5}),
                ],
                style={'textAlign':'center'}
            ),
            header=T['apps'][app_id]['steel'][T_id],
            icon='secondary'
        ),
        style={'margin':15}
    )

    # Supports dropdown list
    icon_width = 50
    btn_style = {'margin-left':10, 'margin-right': 10, 'margin-bottom':5}
    beam_supports = html.Div(
        [
            dbc.Button(
                [
                    html.Img(
                        src='/assets/icons/fixed_fixed_beam.svg',
                        style={'height':icon_width}
                    ),
                ],
                id={'type':f'supports-{app_id}', 'index':0}, color='info', outline=True, active=True,size='md', style=btn_style
            ),
            dbc.Button(
                [
                    html.Img(
                        src='/assets/icons/fixed_roller_beam.svg',
                        style={'height':icon_width}
                    )
                ], 
                id={'type':f'supports-{app_id}', 'index':1}, color='info', active=False, outline=True,size='md', style=btn_style
            ),
            dbc.Button(
                [
                    html.Img(
                        src='/assets/icons/fixed_free-end_beam.svg',
                        style={'height':icon_width}
                    )
                ], 
                id={'type':f'supports-{app_id}', 'index':2}, color='info', outline=True, active=False,size='md', style=btn_style
            ),
            dbc.Button(
                [
                    html.Img(
                        src='/assets/icons/pinned_roller_beam.svg',
                        style={'height':icon_width}
                    )
                ], 
                id={'type':f'supports-{app_id}', 'index':3}, color='info', outline=True, active=False,size='md', style=btn_style
            )
        ],
        className="d-grid gap-2 mx-auto"
    )

    # Figure layout
    fig_layout = go.Layout(
        xaxis=dict(
            domain=[0, 1],
            range=[-1.1, l+1.1],
            tickmode='array',
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            fixedrange=True
                        
        ),
        yaxis=dict(
            domain=[0, 1],
            range=[-1.5   ,4],
            #scaleanchor='x',
            #scaleratio=1,
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            fixedrange=True,
            title=T['apps'][app_id]['beam'][T_id],
            automargin=False
        ),
        height=400,
        showlegend=False,
        #paper_bgcolor= 'rgba(100,200,115,0.9)',
        #plot_bgcolor= 'rgba(236,240,241,0.2)',
        transition={'duration': 500, 'easing': 'cubic-in-out'},
        margin=dict(l=80, r=10, b=15, t=0),
        font=dict(size=style['font_size']['sm']),
    )


    fig_ratios_layout = go.Layout(
        xaxis=dict(
            domain=[0, 1],
            range=[-1.1, l+1.1],
            tickmode='array',
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            fixedrange=True
                        
        ),
        yaxis=dict(
            domain=[0, 1],
            range=[-1.5   ,4],
            #scaleanchor='x',
            #scaleratio=1,
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            fixedrange=True
        ),
        height=60,
        showlegend=False,
        #paper_bgcolor= 'rgba(100,200,115,0.9)',
        #plot_bgcolor= 'rgba(236,240,241,0.2)',
        margin=dict(l=80, r=0, b=0, t=0),
        font=dict(size=style['font_size']['sm']),
    )

    fig_moment_layout = fig_line_layout(f"{T['apps'][app_id]['moment'][T_id]} <b>𝑀 [kN.m]" , yrange=[100,-200])
    fig_rotation_layout = fig_line_layout(f"{T['apps'][app_id]['rotation'][T_id]} <b>𝜑 [deg]", yrange=[35e-6,-35e-6])
    fig_deflection_layout = fig_line_layout(f"{T['apps'][app_id]['deflection'][T_id]} <b>𝑤 [mm]", yrange=[0.12,-0.01], height=260, margin_left=80, margin_bottom=0)

    # Page layout
    theory_box = helper.theory_box(app_id, T_id, module_path)

    padding = 17
    parameters = html.Div(
        [
            dcc.Markdown(T['apps'][app_id]['beam_supports'][T_id], style={'textAlign': 'center'}),
            beam_supports,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['height_ratio'][T_id]} **𝜆**", style={'textAlign': 'center'}),
            lambda_slider,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['distributed_load'][T_id]} **𝑞 \[kN/m]**",style={'textAlign': "center"}),
            q_slider,
            html.Br(),
            html.Hr(),
            dcc.Markdown(T['apps'][app_id]['mechanical_properties'][T_id], style={'textAlign': 'center'}),
            material
        ]
    )

    graphs = html.Div(
        [
            dcc.Graph(figure=go.Figure(layout=fig_layout), config={'displayModeBar':False}, id=f'graph-{app_id}'),
            dcc.Graph(figure=go.Figure(layout=fig_moment_layout), config={'displayModeBar':False}, id=f'graph-moment-{app_id}'),
            dcc.Graph(figure=go.Figure(layout=fig_rotation_layout), config={'displayModeBar':False}, id=f'graph-rotation-{app_id}'),
            dcc.Graph(figure=go.Figure(layout=fig_deflection_layout), config={'displayModeBar':False}, id=f'graph-deflection-{app_id}'),
            dcc.Graph(figure=go.Figure(layout=fig_ratios_layout), config={'displayModeBar':False}, id=f'graph-ratios-{app_id}', style={'margin-bottom': -10}),
            html.Div(refresh_range, className='d-md-flex justify-content-md-end', style={'margin-right': '25px', 'position':'relative', 'top':'-30px'})  
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout