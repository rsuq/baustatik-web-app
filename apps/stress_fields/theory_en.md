The beam theory postulates the following stress tensor for elastic beams with prismatic shape:
$$
\underline{\underline{\sigma}} \sim 
\begin{pmatrix}
    \sigma_x\vphantom{l}_x & \tau_x\vphantom{l}_y & \tau_x\vphantom{l}_z\\
    \tau_y\vphantom{l}_x & 0 & 0\\
    \tau_z\vphantom{l}_x & 0 & 0\\
\end{pmatrix}.
$$ 
$\sigma_y\vphantom{l}_y$, $\sigma_z\vphantom{l}_z$ and $\tau_y\vphantom{l}_z$ are negligible compared to the other components. For pure tension/compression as well as pure bending, all components except $\sigma_x\vphantom{l}_x$ are zero.