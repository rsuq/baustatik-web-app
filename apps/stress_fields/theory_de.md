Die Balkentheorie postuliert den folgenden Spannungstensor für prismatische elastische Balken:

$$
\underline{\underline{\sigma}} \sim 
\begin{pmatrix}
    \sigma_x\vphantom{l}_x & \tau_x\vphantom{l}_y & \tau_x\vphantom{l}_z\\
    \tau_y\vphantom{l}_x & 0 & 0\\
    \tau_z\vphantom{l}_x & 0 & 0\\
\end{pmatrix}.
$$ 

Die Komponenten $\sigma_y\vphantom{l}_y$, $\sigma_z\vphantom{l}_z$ und $\tau_y\vphantom{l}_z$ sind dabei im Vergleich zu den anderen Komponenten vernachlässigbar klein. Für reinen Zug/Druck sowie für reine Biegung sind alle Komponenten ausser $\sigma_x\vphantom{l}_x$ gleich Null.