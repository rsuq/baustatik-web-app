from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# NOTE : This module was developed with a different axis convention than the one shown: 
# X keeps the same
# Y pointing upwards
# Z onwards the screen (on the viewer direction)

## Layout
fig_3d_layout = go.Layout(
    scene = dict(
        aspectratio=dict(x=1, y=1, z=1),
        aspectmode = 'manual',
        dragmode ='orbit',
        camera = dict(
            up=dict(x=0, y=1, z=0),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=0, y=0.2, z=0.55),
            #projection=dict(type = 'orthographic')
        ),
        xaxis = dict(range=[-10,10], visible=False),
        yaxis = dict(range=[-10,10], visible=False),
        zaxis = dict(range=[-10,10], visible=False),
    ),
    margin = dict(l=0, r=0, t=0, b=0),
    height=400,
    paper_bgcolor= 'rgba(236,240,241,0.2)',
    plot_bgcolor= 'rgba(236,240,241,0.2)',
)

fig_cross_layout = go.Layout(
    height=400,
    font=dict(size=style['font_size']['sm']),
    margin = dict(t=60),
)

def layout(T_id):
    # Parameters
    btn_width=110
    cross_switch = html.Div(
        [
            dbc.ButtonGroup(
                [
                    dbc.Button(
                        [
                            html.I(className='bi bi-plus-square-dotted me-2'),
                            T['apps'][app_id]['rectangle'][T_id],
                        ],
                        id={'type':'cross_btn', 'index':0}, color='info', active=True, outline=True,size='sm', style={'width':btn_width}
                    ),
                    dbc.Button(
                        [
                            html.I(className='bi bi-plus-circle-dotted me-2'),
                            T['apps'][app_id]['circle'][T_id],
                        ],
                        id={'type':'cross_btn', 'index':1}, color='info', outline=True, active=False,size='sm', style={'width':btn_width}
                    )
                ]
            )
        ],
        className='d-grid gap-2 col-10 mx-auto'
    )

    mz_slider =dcc.Slider(
        id=f'mz-input-{app_id}',
        min=-500,
        max=500,
        value=500,
        step=100,
        marks={value: {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(-500,501,250)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    section_slider =dcc.Slider(
        id=f'section-input-{app_id}',
        min=-5,
        max=5,
        value=2,
        step=1,
        marks={value: {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(-5,6,2)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    properties_switch = html.Div(
        [
            dbc.ButtonGroup(
                [
                    dbc.Button(
                        T['apps'][app_id]['stress'][T_id],
                        id={'type':'prop_btn', 'index':0}, color='info', outline=True, active=True,size='sm'
                    ),
                    dbc.Button(
                        T['apps'][app_id]['strain'][T_id], 
                        id={'type':'prop_btn', 'index':1}, color='info', active=False, outline=True,size='sm'
                    )
                ]
            )
        ],
        className='d-grid gap-2 col-8 mx-auto'
    )

    abs_toggle = dbc.Switch(
        id=f'modulus-{app_id}',
        label=T['apps'][app_id]['modulus'][T_id],
        value=False,
        style= {'display': 'flex','justify-content': 'center', 'margin-top':8, 'margin-bottom':-5},
        label_style ={'margin-left': '10px', 'font-size':style['font_size']['sm']}
    )

    material = html.Div(
        dbc.Toast(
            html.Div(
                [   
                    html.P(T['apps'][app_id]['young_modulus'][T_id], style={'font-weight': 'bold','margin-top': 5}),
                    html.P('E = 200 GPa', style={'margin-top': -15}),
                    html.P(T['apps'][app_id]['poisson_ratio'][T_id], style={'font-weight': 'bold'}),
                    html.P('ν = 0.3', style={'margin-top': -15, 'margin-bottom':5}),
                ],
                style={'textAlign':'center'}
            ),
            header=T['apps'][app_id]['steel'][T_id],
            icon='secondary'
        ),
        style={'margin':15}
    )

    magnified_alert = dbc.Alert(
        dcc.Markdown(T['apps'][app_id]['3d_displacements'][T_id], style={'margin-bottom':-15}),
        id='alert-no-fade',
        dismissable=True,
        fade=True,
        is_open=True,
        duration='10000',
        color='warning'
    )

    loading = dcc.Loading(
        id=f'loading-{app_id}',
        children=[html.Div([html.Div(id='loading-output-2')])],
        type='cube',
        color='rgba(44,62,80,0.5)',
        style={'zoom':'50%','margin-top':-800}
    )

    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = html.Div(
        [
            dcc.Markdown(T['apps'][app_id]['cross_section'][T_id], style={'textAlign': 'center'}),
            cross_switch,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['bending_moment'][T_id]} **𝑀𝑦 \[kN.m]**", style={'textAlign': 'center'}),
            mz_slider,
            html.Br(),
            html.Hr(),
            dcc.Markdown(f"{T['apps'][app_id]['intersection_position'][T_id]} **\[m]**", style={'textAlign': 'center'}),
            section_slider,
            html.Br(),
            dcc.Markdown(T['apps'][app_id]['tensor_components'][T_id], style={'textAlign': 'center'}),
            properties_switch,
            abs_toggle,
            html.Br(),
            html.Hr(),
            dcc.Markdown(T['apps'][app_id]['mechanical_properties'][T_id], style={'textAlign': 'center'}),
            material
        ]
    )

    graphs = html.Div(
        [
            magnified_alert,
            dcc.Graph(figure=go.Figure(layout=fig_3d_layout), config={'displayModeBar':False}, id=f'graph-3d-{app_id}'),
            loading,
            dcc.Graph(figure=go.Figure(layout=fig_cross_layout), config={'displayModeBar':False}, id=f'graph-cross-{app_id}')    
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout