from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

## Calbacks

# Buttons color - cross section shape
@app.callback(
        Output({'type':'cross_btn', 'index': ALL}, 'active'),
        Input({'type':'cross_btn', 'index': ALL}, 'n_clicks'),
        prevent_initial_call=True
    )
def set_active(n_clicks):
    # General metadata about the trigger
    btn_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0] 
    btn_id = json.loads(btn_id)
    btn_id = btn_id['index']

    # Activate only the clicked button
    active = [False, False]
    active[btn_id] = True

    return active


# Buttons color - Tensor components
@app.callback(
        Output({'type':'prop_btn', 'index': ALL}, 'active'),
        Input({'type':'prop_btn', 'index': ALL}, 'n_clicks'),
        prevent_initial_call=True
    )
def set_active(n_clicks):
    # General metadata about the trigger
    btn_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0] 
    btn_id = json.loads(btn_id)
    btn_id = btn_id['index']

    # Activate only the clicked button
    active = [False, False]
    active[btn_id] = True

    return active

# Update figure
@app.callback(
    Output(f'graph-3d-{app_id}', 'figure'),
    Output(f'graph-cross-{app_id}', 'figure'),
    Output(f'loading-{app_id}', 'children'),
    State('T_id', 'data'),
    State(f'graph-3d-{app_id}', 'figure'),
    State(f'graph-cross-{app_id}', 'figure'),
    Input({'type':'cross_btn', 'index': ALL}, 'active'),
    Input(f'mz-input-{app_id}','value'),
    Input(f'section-input-{app_id}','value'),
    Input({'type':'prop_btn', 'index': ALL}, 'active'),
    Input(f'modulus-{app_id}', 'value')
)
def update_figure(T_id, fig_3d, fig_cross, cross_section, mz, position, prop, modulus):

    ## Static parameters
    #mz = -mz # axis convention change
    l = 10
    n_points = 30
    step = 1
    n_sections=l//step
    young = 200 # GPa
    poisson = 0.3

    # Figure layout
    fig_3d_layout = fig_3d['layout']
    fig_3d_layout.pop('annotations', None)
    fig_3d_layout.pop('shapes', None)
    fig_cross_layout = fig_cross['layout']
    fig_cross_layout.pop('annotations', None)
    fig_cross_layout.pop('shapes', None)

    ## 3D visualization
    # Generate beam matrix in 3 different planes of XYZ
    if cross_section[0]:
        # Cross-section
        b = 4
        h = 1
        cross_shape={'type':'rectangle', 'b':b, 'h':h}

        # Grids
        z, y, x = helper.rectangular_beam(hor=b, ver=h, lon=l, n_points=n_points, step=step)
        x2, y2, z2 = helper.rectangular_beam(hor=l, ver=h, lon=b, n_points=n_points, step=step)
        x3, z3, y3 = helper.rectangular_beam(hor=l, ver=b, lon=h, n_points=n_points, step=step)
        grids = {
            'xGrid':[x,y,z],
            'yGrid':[x2,y2,z2],
            'zGrid':[x3,y3,z3]
        }
        
    else:
        # Cross-section
        a = -l/2
        r = 1 
        cross_shape={'type':'circle', 'r':r}

        # Grids
        x, y, z = helper.cylinder_beam(r, l, a, n_points, n_sections)
        
        grids = {
            'xGrid':[array.T[::4][:-1] for array in (x,y,z)],
            'zGrid':[x,y,z]
        }

        

    # Calculate displacements
    magnifier = 2e4
    Solver = solvers.Displacement(mz=mz, young=young, poisson=poisson, magnifier=magnifier, cross_section=cross_shape)

    # Traces / frames
    def new_surface(x,y,z):
        surface = go.Surface(
            x = x,
            y = y,
            z = z,
            opacity=1,
            lighting=lighting_effects,
            colorscale=helper.single_colorscale(style['color']['secondary']),
            showscale=False,
            hoverinfo='skip'
        )
        return surface
        
    lighting_effects = dict(ambient=1, diffuse=0.9, roughness = 0.5, specular=2, fresnel=4.5)
    frames = []
    w=0
    for time in np.linspace(0,1,10): # time animation loop
        traces = []
        for plane, axis in grids.items(): # looping sections in 3 different planes of XYZ
            w+=0.1
            x = axis[0] + Solver.x_disp(axis[0],axis[1])*time
            y = axis[1] + Solver.y_disp(axis[0],axis[1],axis[2])*time
            z = axis[2] + Solver.z_disp(axis[1],axis[2])*time

            # Surfaces
            # NOTE : filtered the grids to only plotted the minimum surfaces (6 for rectangular beam)
            if plane == 'yGrid': # Only for rectangle cross-section
                x_surface = x[:,:n_points]
                y_surface = y[:,:n_points]
                z_surface = z[:,:n_points]
                traces.append(new_surface(x_surface,y_surface,z_surface))
                x_surface = x[:,n_points*2:n_points*3]
                y_surface = y[:,n_points*2:n_points*3]
                z_surface = z[:,n_points*2:n_points*3]
                traces.append(new_surface(x_surface,y_surface,z_surface))
        
            elif plane == 'zGrid':
                surface = go.Surface(
                    x = x,
                    y = y,
                    z = z,
                    opacity=1,
                    lighting=lighting_effects,
                    colorscale=helper.single_colorscale(style['color']['secondary']),
                    showscale=False,
                    hoverinfo='skip',
                    lightposition=dict(
                        x=20, 
                        y=100, 
                        z=0
                    )
                )
                traces.append(surface)

                if cross_section[1]:
                    # Cylinder top surface
                    x_2 = x[:2]
                    y_2 = y[:2]
                    z_2 = z[:2]

                    x_2[1] = np.ones(n_points)*a
                    y_2[1] = np.ones(n_points)*(y[0].min()+y[0].max())/2
                    z_2[1] = np.zeros(n_points)

                    surface_top = go.Surface(
                        x = x_2,
                        y = y_2,
                        z = z_2,
                        opacity=1,
                        lighting=lighting_effects,
                        colorscale=helper.single_colorscale(style['color']['secondary']),
                        showscale=False,
                        hoverinfo='skip',
                        lightposition=dict(
                            x=20, 
                            y=100, 
                            z=0
                        )
                    )
                    traces.append(surface_top)

                    # Cylinder bottom surface
                    x_2 = x[-2:]
                    y_2 = y[-2:]
                    z_2 = z[-2:]

                    x_2[-2] = -np.ones(n_points)*a
                    y_2[-2] = np.ones(n_points)*(y[-1].min()+y[-1].max())/2
                    z_2[-2] = np.zeros(n_points)

                    surface_bottom = go.Surface(
                        x = x_2,
                        y = y_2,
                        z = z_2,
                        opacity=1,
                        lighting=lighting_effects,
                        colorscale=helper.single_colorscale(style['color']['secondary']),
                        showscale=False,
                        hoverinfo='skip',
                        lightposition=dict(
                            x=20, 
                            y=100, 
                            z=0
                        )
                    )

                    traces.append(surface_bottom)
            
            else:
                pass

            # Scale the grid to avoid overlap between lines and surfaces
            offset = 0.01
            axis[0] = axis[0]*(l+offset)/l
            if cross_section[0]: # rectangular cross-section
                axis[1] = axis[1]*(h+offset)/h
                axis[2] = axis[2]*(b+offset)/b
            else: # circular cross-section
                axis[1] = axis[1]*(r+offset)/r
                axis[2] = axis[2]*(r+offset)/r

            x = axis[0] + Solver.x_disp(axis[0],axis[1])*time
            y = axis[1] + Solver.y_disp(axis[0],axis[1],axis[2])*time
            z = axis[2] + Solver.z_disp(axis[1],axis[2])*time
            
            for i in range(len(x)): # Single section loop
                # Grid lines
                aux = go.Scatter3d(
                    x = x[i],
                    y = y[i],
                    z = z[i],
                    mode ='lines',
                    line = dict(color=style['color']['primary'], width=2),
                    opacity=0.8, 
                    showlegend=False,
                    hoverinfo='skip'
                )
                traces.append(aux)
        
    
        # Intersection plane
        size=3
        x_plane=np.array([[position,position], [position,position]])
        y_plane=np.array([[-size,size],[-size,size]])
        z_plane=np.array([[-size,-size],[size,size]])
        x = x_plane + Solver.x_disp(x_plane, y_plane)*time
        y = y_plane + Solver.y_disp(x_plane, y_plane, z_plane)*time
        z = z_plane + Solver.z_disp(y_plane, z_plane)*time

        intersection = go.Surface(
            x = x,
            y = y,
            z = z,
            showscale=False,
            opacity=0.2,
            colorscale=helper.single_colorscale(style['color']['info']),
            hoverinfo='skip',
            contours=go.surface.Contours(
                x=go.surface.contours.X(highlight=False),
                y=go.surface.contours.Y(highlight=False),
                z=go.surface.contours.Z(highlight=False),
            )
        )
        traces.append(intersection)
        frames.append(traces) # Traces for static plot is only the final frame
    
    # Generate plot
    frames=[go.Frame(data=x) for x in frames]
    fig_3d = go.Figure(data=traces, layout=fig_3d_layout, frames=frames[::-1]+frames)
    fig_3d.update_layout(
        updatemenus=[
            dict(
                type='buttons',
                buttons=[
                    dict(
                        label=T['apps'][app_id]['animate'][T_id],
                        method='animate',
                        args=[None, {'frame': {'duration': 100, 'redraw': True},'fromcurrent': False}]
                    )
                ],
                x=0.05,
                xanchor='left',
                y=0.95,
                yanchor='top'
            )
        ]
    )

    # Axis convention
    fig_3d = helper.axis_3d(
        fig_3d, 
        origin=[-9,-3,0], 
        size=1, 
        u=[1,0,0], 
        v=[0,-1,0], 
        w=[0,0,1], 
        labels = ['𝑥','𝑧','𝑦'], 
        color=style['color']['secondary'],
        font_color=style['color']['primary'],
        font_size=style['font_size']['xl']
    )

    ## Mechanical properties visualization
    n_points=50
    # Generate cross-section 
    if cross_section[0]: # Rectangular cross-section
        hor = np.linspace(-b/2,b/2, n_points*b)
        ver = np.linspace(-h/2, h/2, n_points*h)
        z, y = np.meshgrid(hor, ver)
        y = y.flatten()
        z = z.flatten()
    
    else: # Circular cross-section
        theta = np.linspace(0, 2*np.pi, 360)
        hor = np.array([x*np.cos(theta) for x in np.linspace(0,r,n_points*r)])
        ver = np.array([x*np.sin(theta) for x in np.linspace(0,r,n_points*r)])
        z = hor.flatten()
        y = ver.flatten()


    # Color scale
    if prop[0]: # stress
        colormap_xx = Solver.stress(y)/1e6 # units: Pa to MPa
        colormap_yy = np.zeros(len(y))
        colormap_zz = np.zeros(len(y))

        cmax = 1
        units = ' MPa'
        component = T['apps'][app_id]['stress'][T_id]
        normal_char = '𝜎'
        tranversal_char = '𝜏'
        hover_format = '%{text:.2f} '

    else: # strain
        colormap_xx = Solver.stress(y)/(young*1e9)
        colormap_yy = -poisson*Solver.stress(y)/(young*1e9)
        colormap_zz = -poisson*Solver.stress(y)/(young*1e9)
        cmax = 4e-6
        units = ' '
        component = T['apps'][app_id]['strain'][T_id]
        normal_char = '𝜀'
        tranversal_char = '𝛾'
        hover_format = '%{text:.2e} '

    if modulus:
        cmin = 0
        colormap_xx = abs(colormap_xx)
        colormap_yy = abs(colormap_yy)
        colormap_zz = abs(colormap_zz)
        colors = px.colors.sample_colorscale(
            [
                style['color']['light'], 
                style['color']['info'],
                style['color']['primary'],
            ], 
            np.linspace(0,1,11), 
            colortype='rgb'
        )
    else:
        cmin = -cmax
        colors = px.colors.sample_colorscale(
            [
                style['color']['danger'],
                style['color']['warning'], 
                style['color']['light'], 
                style['color']['info'], 
                style['color']['primary']
            ], 
            np.linspace(0,1,11), 
            colortype='rgb'  
        )

    # Create figure
    fig_cross = make_subplots(
        rows=3, cols=3,
        shared_xaxes=True,
        shared_yaxes=True,
        horizontal_spacing=0.04,
        vertical_spacing=0.04,

    )

    # Mapping plot data
    tensor = {
        f'{normal_char}<sub>𝑥𝑥'     : {'position': [1,1], 'colormap': colormap_xx},
        #''                         : {'position': [1,2], 'colormap': np.zeros(len(y))}, 
        #''                         : {'position': [1,3], 'colormap': np.zeros(len(y))},
        f'{tranversal_char}<sub>𝑦𝑥' : {'position': [2,1], 'colormap': np.zeros(len(y))},
        f'{normal_char}<sub>𝑦𝑦'     : {'position': [2,2], 'colormap': colormap_yy}, 
        #''                         : {'position': [2,3], 'colormap': np.zeros(len(y))},
        f'{tranversal_char}<sub>𝑧𝑥' : {'position': [3,1], 'colormap': np.zeros(len(y))}, 
        f'{tranversal_char}<sub>𝑧𝑦' : {'position': [3,2], 'colormap': np.zeros(len(y))}, 
        f'{normal_char}<sub>𝑧𝑧'     : {'position': [3,3], 'colormap': colormap_zz}
    } 

    # Update figure
    for title, matrix in tensor.items():
        # Axis
        fig_cross.update_yaxes(
            title_text=title, 
            title_standoff = 0, 
            row=matrix['position'][0], 
            col=matrix['position'][1]
        )

        # Traces
        data = go.Scattergl(
            x=z,
            y=y,
            marker=dict(
                color=matrix['colormap'], # Stress or strain visualization
                size=2,
                coloraxis="coloraxis"
            ),
            text=matrix['colormap'],
            #hoverinfo='skip',
            name='',
            mode='markers',
            hovertemplate =hover_format +  f'{units}<extra> {component}</extra>',  
            textfont=dict(
                size=style['font_size']['xl']+20
            )
        )

        fig_cross.add_trace(data, row=matrix['position'][0], col=matrix['position'][1])

    # Update layout
    fig_cross.update_layout(
        yaxis=dict(fixedrange=True, scaleanchor='x', scaleratio=1),
        xaxis8=dict(matches= 'x'),
        xaxis9=dict(matches= 'x'),
        yaxis4=dict(matches='y'),
        yaxis7=dict(matches='y'),
        showlegend=False,
        coloraxis=dict(
            cmin=cmin, 
            cmax=cmax, 
            colorscale=colors, 
            colorbar=dict(
                x=1.01, 
                xanchor='left',
                ticksuffix=units, 
                nticks=5,
                tickfont=dict(size=style['font_size']['sm']),
                exponentformat='e'   
            ) 
        ),
        #plot_bgcolor ='rgba(100,200,100,1)',
        #paper_bgcolor ='rgba(200,200,100,1)',
        height=400,
        title=component + ' ' + T['apps'][app_id]['components'][T_id],
        font=dict(size=style['font_size']['sm']),
        margin = dict(t=40,l=50,r=120,b=0)
    )

    fig_cross.for_each_xaxis(lambda x: x.update(showgrid=False, zeroline=True, showticklabels=False,))
    fig_cross.for_each_yaxis(lambda x: x.update(showgrid=False, zeroline=True, showticklabels=False, title_font=dict(size=style['font_size']['xl'])))
                    

    return fig_3d, fig_cross, None


