from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Callbacks

@app.callback(
    Output('parameters', 'children'), 
    [Input('tabs-load', 'active_tab')],
    [State('parameters', 'children')]
)
def switch_tab(load_mode, content):
    if load_mode == 'point-load':
        content[1]['props']['style'] = {'display':'none'} # dist_load_slider
        content[2]['props']['style'] = {'display':'none'} # html_break
        content[3]['props']['style'] = {'display':'block'} # load_slider
        content[4]['props']['style'] = {'display':'block'} # load_position
        content[5]['props']['style'] = {'display':'block'} # html_break
    elif load_mode == 'distributed-load':
        content[1]['props']['style'] = {'display':'block'} # dist_load_slider
        content[2]['props']['style'] = {'display':'none'} # html_break
        content[3]['props']['style'] = {'display':'none'} # load_slider
        content[4]['props']['style'] = {'display':'none'} # load_position
        content[5]['props']['style'] = {'display':'none'} # html_break
    else:
        content = 'Something wrong happened :('
    return content


@app.callback(
    Output('graph-beam', 'figure'),
    [
        State('T_id', 'data'),
        Input('load-input', 'value'),
        Input('dist-load-input', 'value'),
        Input('load-position', 'value'),
        Input('beam-length', 'value'),
        Input('support-mode', 'value'),
        Input('tabs-load', 'active_tab')
    ]
)

def update_figure(T_id, F, q, a, l, support_mode, load_mode):
    a = a*l if load_mode=='point-load' else 0.5*l
    Calc = solvers.SimplySupportedBeam(F=F, q=q, a=a, l=l, load_mode=load_mode) 

    moment_array = [Calc.moment(x=x) for x in np.linspace(0,l,21)]
    trace_moment = go.Scatter(
        x=np.linspace(0,10, 21),
        y=moment_array,
        text=[
            f'{int(x)} kNm' 
            if (abs(x)==max(abs(np.array(moment_array))) or x==0) and sum(moment_array)!=0 
            else ' ' 
            for x in moment_array
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in moment_array],
        textfont=dict(size=style['font_size']['md']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y'
    )

    shear_array = [Calc.shear(x=x) if load_mode=='point-load' else Calc.shear(x=x) for x in np.linspace(0,l,1001)]
    trace_shear = go.Scatter(
        x=np.linspace(0,10,1001),
        y=shear_array,
        text=[
            f'{int(shear_array[i])} kN' 
            if i==0 or i==(len(shear_array)-1)
            else ' ' 
            for i in range(len(shear_array))
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in shear_array],
        textfont=dict(size=style['font_size']['md']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y2',
        line_shape='linear',
        line=dict(color=style['color']['success'])
    )

    trace_beam = go.Scattergl(
        x=[0, 10],
        y=[0, 0],
        mode='lines+markers+text',
        text=['A', 'B'],
        xaxis='x',
        yaxis='y3',
        textposition=['middle left', 'middle right'],
        textfont=dict(
            color=style['color']['primary'],
            size=style['font_size']['xl']
        ),
        line=dict(width=style['line_width']['lg']),
        marker=dict(size=12,
                    color=style['color']['primary']),       
    )

    data = [trace_moment, trace_shear, trace_beam]
    layout = go.Layout(
        xaxis=dict(
            domain=[0, 1],
            range=[-0.8, 10.8],
            tickmode='array',
            tickvals=np.linspace(0, 10, 9),
            ticktext=[0, 'ℓ/8', 'ℓ/4', '3ℓ/8',
                      'ℓ/2', '5ℓ/8', '3ℓ/4', '7ℓ/8', 'ℓ']
        ),
        yaxis=dict(
            domain=[0, 0.3],
            range=[290, -290]
        ),
        yaxis2=dict(
            domain=[0.33, 0.66],
            range=[120, -120],
        ),
        yaxis3=dict(
            domain=[0.7, 1],
            range=[-1.8, 0.9],
            scaleanchor='x',
            scaleratio=1,
            showgrid=False,
            zeroline=False
        ),
    )

    fig = go.Figure(data=data, layout=layout)

    if support_mode == False:
        path='assets/geometries'
        scale=0.008
        PinnedSupport = helper.SVGPath()
        PinnedSupport.reader(f'{path}/pinned_support.svg')
        PinnedSupport.scale(x=scale, y=scale)
        RollerSupport = helper.SVGPath()
        RollerSupport.reader(f'{path}/roller_support.svg')
        RollerSupport.scale(x=scale, y=scale)
        RollerSupport.translate(x=10)


        # Supports
        fig.add_shape(
            type='path',
            path=PinnedSupport.path+RollerSupport.path,
            fillcolor=None,
            line_color=style['color']['primary'],
            line={'width': style['line_width']['sm']},
            yref='y3')

    else:
        # Av
        Arrow = helper.ArrowDim(style['line_width']['md'], Calc.shear(x=-0.001))
        fig.add_annotation(
            x=0,  # arrows' head
            y=-0.03,  # arrows' head
            ax=0,  # arrows' tail
            ay=-Arrow.length(),  # arrows' tail
            xref='x',
            yref='y3',
            axref='x',
            ayref='y3',
            text=f'A<sub>V</sub> = {abs(round(Calc.shear(x=-0.001),1))} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=Arrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['success'],
            font={'size': style['font_size']['lg'], 'color': style['color']['success']}
        )

        # Bv
        Arrow = helper.ArrowDim(style['line_width']['md'], Calc.shear(x=l))
        fig.add_annotation(
            x=10,  # arrows' head
            y=-0.03,  # arrows' head
            ax=10,  # arrows' tail
            ay=Arrow.length(),  # arrows' tail
            xref='x',
            yref='y3',
            axref='x',
            ayref='y3',
            text=f'B<sub>V</sub> = {abs(round(Calc.shear(x=l),2))} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=Arrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['success'],
            font={'size': style['font_size']['lg'], 'color': style['color']['success']}
        )

    # Load
    if load_mode=='point-load' and F!=0:
        LoadArrow = helper.ArrowDim(style['line_width']['md'], F)
        fig.add_annotation(
            x=(a*10)/l,
            y=0,
            ax=(a*10)/l,
            ay=LoadArrow.length(),
            xref='x',
            yref='y3',
            axref='x',
            ayref='y3',
            text=f'𝐹 = {abs(F)} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=LoadArrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['danger'],
            font={'size': style['font_size']['lg'], 'color': style['color']['danger']}
        )
    elif load_mode=='distributed-load' and q!=0:
        LoadArrow = helper.ArrowDim(style['line_width']['md'], q*4.2)
        for x in np.linspace(0,10,7):
            offset = 0.18 if q>0 else -0.18 
            fig.add_annotation(
                x=x,
                y=0,
                ax=x,
                ay=LoadArrow.length()+offset if x==5 else LoadArrow.length(),
                xref='x',
                yref='y3',
                axref="x",
                ayref="y3",
                text=f'𝑞 = {abs(q)} kN/m' if x==5 else '',
                showarrow=True,
                align="left",
                arrowhead=1,
                arrowsize=LoadArrow.head(),
                arrowwidth=style['line_width']['md'],
                arrowcolor=style['color']['danger'],
                font={'size': style['font_size']['lg'], 'color': style['color']['danger']}
            )
            fig.add_shape(
                type='line',
                x0=0,
                y0=LoadArrow.length(),
                x1=10,
                y1=LoadArrow.length(),
                line_color=style['color']['danger'],
                yref="y3",
                line={'width': style['line_width']['md']}
            )

    # Dimensions
    fig = helper.dimension(
        fig,
        text=f'ℓ = {round(l,1)} m',
        start=0,
        end=10,
        position=-1.65,
        line_width=style['line_width']['md'], 
        color=style['color']['info'], 
        font_size=style['font_size']['lg']
    )

    if load_mode=='point-load':
        fig = helper.dimension(
            fig,
            text=f'𝒶 = {round(a,1)} m',
            start=0,
            end=(a*10)/l,
            position=-1.3,
            line_width=style['line_width']['md'], 
            color=style['color']['info'], 
            font_size=style['font_size']['lg'],
        )
    else:
        pass

    fig.update_layout(
        height=800,
        showlegend=False,
        title=T['apps'][app_id]['internal_forces'][T_id],
        xaxis_title='𝑥 [m]',
        yaxis_title=f"{T['apps'][app_id]['moment'][T_id]} <b>𝑀 [kNm]",
        yaxis2_title=f"{T['apps'][app_id]['shear'][T_id]} <b>𝑉 [kN]",
        yaxis3={'showticklabels': False},
        font=dict(size=style['font_size']['md']),
        plot_bgcolor='rgba(236,240,241,0.2)',
    )
    fig.update_traces(hoverinfo='skip')
    fig.update_yaxes(fixedrange=True)

    return fig

