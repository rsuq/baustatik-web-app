from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Callbacks
suffix='-superp'

@app.callback(
    Output(f'graph-beam{suffix}', 'figure'),
    [
        State('T_id', 'data'),
        Input(f'load-input{suffix}', 'value'),
        Input(f'dist-load-input{suffix}', 'value'),
        Input(f'load-position{suffix}', 'value'),
        Input(f'beam-length{suffix}', 'value'),
        Input(f'support-mode{suffix}', 'value')
    ]
)
def update_figure_superp(T_id, F, q, a, l, support_mode):
    load_mode='point-load'
    a = a*l if load_mode=='point-load' else 0.5*l
    Calc = solvers.SimplySupportedBeam(F=F, q=q, a=a, l=l, load_mode='point-load')
    CalcDist = solvers.SimplySupportedBeam(F=F, q=q, a=a, l=l, load_mode='distributed-load')

    # Individual moment - point load
    moment_array = [Calc.moment(x=x)  for x in np.linspace(0,l,21)]
    trace_moment = go.Scatter(
        x=np.linspace(0,10, 21),
        y=moment_array,
        text=[
            f'{int(x)} kNm' 
            if (abs(x)==max(abs(np.array(moment_array))) or x==0) and sum(moment_array)!=0 
            else ' ' 
            for x in moment_array
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in moment_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y2',
        line=dict(color=style['color']['danger']),
        name='𝑀(𝐹)',
        legendrank=4,
        legendgroup = 'moment'
    )

    # Individual moment - distributed load
    moment_array = [CalcDist.moment(x=x)  for x in np.linspace(0,l,21)]
    trace_moment_dist = go.Scatter(
        x=np.linspace(0,10, 21),
        y=moment_array,
        text=[
            f'{int(x)} kNm' 
            if (abs(x)==max(abs(np.array(moment_array))) or x==0) and sum(moment_array)!=0 
            else ' ' 
            for x in moment_array
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in moment_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y2',
        line=dict(color='#F0A29A'),
        name='𝑀(q)',
        legendrank=5,
        legendgroup = 'moment'
    )

    # Combined moment
    moment_array = [Calc.moment(x=x) + CalcDist.moment(x=x)  for x in np.linspace(0,l,21)]
    trace_moment_comb = go.Scatter(
        x=np.linspace(0,10, 21),
        y=moment_array,
        text=[
            f'{int(x)} kNm' 
            if (abs(x)==max(abs(np.array(moment_array))) or x==0) and sum(moment_array)!=0 
            else ' ' 
            for x in moment_array
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in moment_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y',
        line=dict(color=style['color']['primary']),
        name='𝑀(q) + 𝑀(𝐹)',
        legendrank=6,
        legendgroup = 'moment'
    )

    # Individual shear - point load
    shear_array = [Calc.shear(x=x) for x in np.linspace(0,l,21)]
    trace_shear = go.Scatter(
        x=np.linspace(0,10,21),
        y=shear_array,
        text=[
            f'{int(shear_array[i])} kN' 
            if i==0 or i==(len(shear_array)-1)
            else ' ' 
            for i in range(len(shear_array))
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in shear_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y4',
        line_shape='hv' if load_mode=='point-load' else 'linear',
        line=dict(color=style['color']['danger']),
        name='𝑉(𝐹)',
        legendrank=1,
        legendgroup = 'shear'
    )

    # Individual shear - distributed load
    shear_array = [CalcDist.shear(x=x) for x in np.linspace(0,l,21)]
    trace_shear_dist = go.Scatter(
        x=np.linspace(0,10,21),
        y=shear_array,
        text=[
            f'{int(shear_array[i])} kN' 
            if i==0 or i==(len(shear_array)-1)
            else ' ' 
            for i in range(len(shear_array))
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in shear_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y4',
        line_shape='linear',
        line=dict(color='#F0A29A'),
        name='𝑉(q)',
        legendrank=2,
        legendgroup = 'shear'
    )

    # Combined shear
    shear_array = [Calc.shear(x=x) + CalcDist.shear(x=x) for x in np.linspace(0,l,1001)]
    trace_shear_comb = go.Scatter(
        x=np.linspace(0,10,1001),
        y=shear_array,
        text=[
            f'{int(shear_array[i])} kN' 
            if i==0 or i==(len(shear_array)-1)
            else ' ' 
            for i in range(len(shear_array))
        ],
        textposition=['bottom center' if x>0 else 'top center' for x in shear_array],
        textfont=dict(size=style['font_size']['sm']),
        fill='tozeroy',
        mode='lines+text',
        xaxis='x',
        yaxis='y3',
        line_shape='linear',
        line=dict(color=style['color']['success']),
        name='𝑉(q) + 𝑉(𝐹)',
        legendrank=3,
        legendgroup = 'shear'
    )

    # Beam
    trace_beam = go.Scattergl(
        x=[0, 10],
        y=[0, 0],
        mode='lines+markers+text',
        text=['A', 'B'],
        xaxis='x',
        yaxis='y5',
        textposition=['middle left', 'middle right'],
        line=dict(width=style['line_width']['lg']),
        marker=dict(size=10,
                    color=style['color']['primary']),
        textfont=dict(
            color=style['color']['primary'],
            size=style['font_size']['lg']
        ),
        showlegend=False,
        legendrank = 0,
    )

    data = [
        trace_moment, trace_moment_dist, trace_moment_comb, 
        trace_shear_comb, trace_shear_dist, trace_shear,
        trace_beam
        ]
    layout = go.Layout(
        xaxis=dict(
            domain=[0, 1],
            range=[-0.8, 10.8],
            tickmode='array',
            tickvals=np.linspace(0, 10, 9),
            ticktext=[0, 'ℓ/8', 'ℓ/4', '3ℓ/8',
                        'ℓ/2', '5ℓ/8', '3ℓ/4', '7ℓ/8', 'ℓ']
        ),
        # Combined moment
        yaxis=dict(
            domain=[0, 0.18],
            range=[600, -600]
        ),
        # Individual moment
        yaxis2=dict(
            domain=[0.18, 0.36],
            range=[600, -600],
        ),
        # Combined shear
        yaxis3=dict(
            domain=[0.39, 0.57],
            range=[250, -250],
        ),
        # Individual shear
        yaxis4=dict(
            domain=[0.57, 0.75],
            range=[250, -250],
        ),
        # Beam diagram
        yaxis5=dict( 
            domain=[0.78, 1],
            range=[-1.5, 0.7],
            scaleanchor='x',
            scaleratio=1,
            showgrid=False,
            zeroline=False
        ),
    )

    fig = go.Figure(data=data, layout=layout)

    if support_mode == False:
        # Supports
        path='assets/geometries'
        scale=0.008
        PinnedSupport = helper.SVGPath()
        PinnedSupport.reader(f'{path}/pinned_support.svg')
        PinnedSupport.scale(x=scale, y=scale)
        RollerSupport = helper.SVGPath()
        RollerSupport.reader(f'{path}/roller_support.svg')
        RollerSupport.scale(x=scale, y=scale)
        RollerSupport.translate(x=10)

        fig.add_shape(
            type='path',
            path=PinnedSupport.path+RollerSupport.path,
            fillcolor=None,
            line_color=style['color']['primary'],
            line={'width': style['line_width']['sm']},
            yref='y5'
            )

    else:
        # Av
        Arrow = helper.ArrowDim(style['line_width']['md'], (Calc.shear(x=-0.001)+CalcDist.shear(x=-0.001)))
        fig.add_annotation(
            x=0,  # arrows' head
            y=-0.03,  # arrows' head
            ax=0,  # arrows' tail
            ay=-Arrow.length(),  # arrows' tail
            xref='x',
            yref='y5',
            axref='x',
            ayref='y5',
            text=f'A<sub>V</sub> = {abs(round((Calc.shear(x=-0.001)+CalcDist.shear(x=-0.001)),1))} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=Arrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['success'],
            font={'size': style['font_size']['md'], 'color': style['color']['success']}
        )

        # Bv
        Arrow = helper.ArrowDim(style['line_width']['md'],(Calc.shear(x=l)+CalcDist.shear(x=l)))
        fig.add_annotation(
            x=10,  # arrows' head
            y=-0.03,  # arrows' head
            ax=10,  # arrows' tail
            ay=Arrow.length(),  # arrows' tail
            xref='x',
            yref='y5',
            axref='x',
            ayref='y5',
            text=f'B<sub>V</sub> = {abs(round((Calc.shear(x=l)+CalcDist.shear(x=l)),2))} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=Arrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['success'],
            font={'size': style['font_size']['md'], 'color': style['color']['success']}
        )


    # Distributed load
    if q!=0:
        LoadArrow = helper.ArrowDim(style['line_width']['md'], q*4.2)
        for x in np.linspace(0,10,7):
            offset = 0.21 if q>0 else -0.21 
            fig.add_annotation(
                x=x,
                y=0,
                ax=x,
                ay=LoadArrow.length()+offset if x==5 else LoadArrow.length(),
                xref='x',
                yref='y5',
                axref="x",
                ayref="y5",
                text=f'𝑞 = {abs(q)} kN/m' if x==5 else '',
                showarrow=True,
                align="left",
                arrowhead=1,
                arrowsize=LoadArrow.head(),
                arrowwidth=style['line_width']['md'],
                arrowcolor='#F0A29A',
                font={'size': style['font_size']['md'], 'color': '#F0A29A'}
            )
            fig.add_shape(
                type='line',
                x0=0,
                y0=LoadArrow.length(),
                x1=10,
                y1=LoadArrow.length(),
                line_color='#F0A29A',
                yref="y5",
                line={'width': style['line_width']['md']}
            )

    # Point load
    if F!=0:
        LoadArrow = helper.ArrowDim(style['line_width']['md'], F)
        fig.add_annotation(
            x=(a*10)/l,
            y=0,
            ax=(a*10)/l,
            ay=LoadArrow.length(),
            xref='x',
            yref='y5',
            axref='x',
            ayref='y5',
            text=f'𝐹 = {abs(F)} kN',
            showarrow=True,
            align='left',
            arrowhead=1,
            arrowsize=LoadArrow.head(),
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['danger'],
            font={'size': style['font_size']['md'], 'color':style['color']['danger']}
        )
    # Dimensions
    fig = helper.dimension(
        fig,
        text=f'ℓ = {round(l,1)} m',
        start=0,
        end=10,
        position=-1.65,
        line_width=style['line_width']['md'], 
        color=style['color']['info'], 
        font_size=style['font_size']['md'],
        yref='y5',
    )

    if load_mode=='point-load':
        fig = helper.dimension(
            fig,
            text=f'𝒶 = {round(a,1)} m',
            start=0,
            end=(a*10)/l,
            position=-1.3,
            line_width=style['line_width']['md'], 
            color=style['color']['info'], 
            font_size=style['font_size']['md'],
            yref='y5',
        )
    else:
        pass

    spacer = '\t'*45
    fig.update_layout(
        height=1000,
        margin=dict(l=0, r=0, t=80, b=20),
        title=T['apps'][app_id]['internal_forces'][T_id],
        xaxis_title='𝑥 [m]',
        #yaxis_title='Combined moment [kNm]',
        yaxis2_title=f"{T['apps'][app_id]['moment'][T_id]} <b>𝑀 [kNm] {spacer} <sub>",
        #yaxis3_title='Combined share [kN]',
        yaxis4_title=f"{T['apps'][app_id]['shear'][T_id]} <b>𝑉 [kN] {spacer} <sub>",
        yaxis5={'showticklabels': False},
        font=dict(size=style['font_size']['sm']),
        #paper_bgcolor='rgba(50,0,0,1)',
        plot_bgcolor='rgba(236,240,241,0.2)',
        showlegend=True,
        legend=dict(
            #orientation="h",
            x=1,
            y=0.26,
            xanchor="left",
            yanchor="bottom",
            #bgcolor="rgba(250,250,250,0)",
            tracegroupgap = 265,
            traceorder='grouped',
            groupclick='toggleitem',
            font=dict(size=style['font_size']['md'])
        )
    )
    fig.update_traces( hoverinfo='skip')
    fig.update_yaxes(fixedrange=True)

    return fig
