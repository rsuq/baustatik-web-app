Internal forces diagrams are used for visual representation of normal force $N$, shear force $V$ and bending moment $M$ along the beam. 
The internal forces diagrams of statically determined systems can always be calculated by solving the equilibrium conditions. 
However, it is often easier and faster to determine the internal forces diagrams using the differential relations as described in the script in Chapter 4:

$$
\begin{aligned}
        \frac{\mathrm{d}N}{\mathrm{d}x} + q_x &= 0 \\
        \frac{\mathrm{d}V}{\mathrm{d}x} + q_z &= 0 \\
        \frac{\mathrm{d}M}{\mathrm{d}x}  &= V
\end{aligned}
$$

According to the 1st order theory, support reactions, displacements and internal force diagrams from different load cases can be summed up. The 1st order theory assumes very small deformations and a linear elastic system behavior. This principle is also called the superposition principle and is described in more detail in the script in Chapter 5.


