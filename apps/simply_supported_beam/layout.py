from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# The following layout works for both 'simply supported beam' and 'superposition of loads' apps
# Layout
def layout(T_id, suffix):
    load_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['point_load'][T_id]} **𝐹 \[kN]**", style={'textAlign': "center"}), 
        dcc.Slider(
            id=f'load-input{suffix}',
            min=-100,
            max=100,
            value=70 if suffix=='' else -70,
            step=10,
            marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
                for value in range(-100, 101, 50)},
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    dist_load_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['distributed_load'][T_id]} **𝑞 \[kN/m]**",style={'textAlign': "center"}),
        dcc.Slider(
            id=f'dist-load-input{suffix}',
            min=-20,
            max=20,
            value=15,
            step=1,
            marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
                for value in range(-20, 21, 10)},
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    marks = {
        '0': '0',
        #'0.125': 'ℓ/8',
        '0.25': 'ℓ/4',
        #'0.375': '3ℓ/8',
        '0.5': 'ℓ/2',
        #'0.625': '5ℓ/8',
        '0.75': '3ℓ/4',
        #'0.875': '7ℓ/8',
        '1': 'ℓ'
    }
    position_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['load_position'][T_id]} **𝒶 \[m]**",style={'textAlign': "center"}),
        dcc.Slider(
            id=f'load-position{suffix}',
            min=0,
            max=1,
            value=0.5,
            marks={keys : {'label': values, 'style':{'font-size':style['font_size']['sm']}} for keys, values in marks.items()},
            step=1/(2*10),
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    length_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['beam_length'][T_id]} **ℓ \[m]**",style={'textAlign': "center"}),
        dcc.Slider(
            id=f'beam-length{suffix}',
            min=1,
            max=10,
            value=10,
            marks={str(value): {'label': str(value), 'style': {
                'font-size': style['font_size']['sm']}} for value in range(1,11,3)},
            step=0.5,
            updatemode='drag',
            className='dbc_light',
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    support_toggle = dbc.Switch(
        id=f'support-mode{suffix}',
        label=T['apps'][app_id]['show_reactions'][T_id],
        value=False,
        style= {'display': 'flex','justify-content': 'center'},
        label_style ={'margin-left': '10px'}
    )

    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = [
        html.Div(
            [
                dbc.Tabs(
                    [
                        dbc.Tab(label=T['apps'][app_id]['point'][T_id], tab_id=f"point-load{suffix}", tab_style={'margin':'auto','cursor': 'pointer'}),
                        dbc.Tab(label=T['apps'][app_id]['distributed'][T_id], tab_id=f"distributed-load{suffix}",tab_style={'marginRight':'auto','cursor': 'pointer'}),
                    ],
                    id =f'tabs-load{suffix}', active_tab = f'point-load{suffix}'
                ) 
            ], 
            style={'display': 'block' if suffix=='' else 'none'}
        ),
        html.Div(
            [
                html.Br(),
                dist_load_slider,
                html.Br(),
                load_slider,
                html.Br(),
                position_slider,
                html.Br(),
                length_slider,
                html.Br(),
                support_toggle
            ],
            id=f'parameters{suffix}'
        ) 
    ] 

    graphs = dcc.Graph(config={'displayModeBar':False} ,id=f'graph-beam{suffix}')

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout