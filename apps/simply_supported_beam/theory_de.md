Schnittkraftdiagramme dienen zur visuellen Darstellung der Normalkraft $N$, Querkraft $V$ und des Biegemoments $M$ entlang des Balkens. 
Die Schnittkraftdiagramme statisch bestimmter Systeme können dabei immer durch das Lösen der Gleichgewichtsbedingungen bestimmt werden. 
Oft ist es jedoch einfacher und schneller, die Schnittkraftdiagramme über die Differenzialbeziehungen, wie im Skript in Kapitel 4 beschrieben, zu ermitteln:

$$
\begin{aligned}
        \frac{\mathrm{d}N}{\mathrm{d}x} + q_x &= 0 \\
        \frac{\mathrm{d}V}{\mathrm{d}x} + q_z &= 0 \\
        \frac{\mathrm{d}M}{\mathrm{d}x}  &= V
\end{aligned}
$$

Nach der Theorie 1. Ordnung können Lagerreaktionen, Verschiebungen und Schnittgrössendiagramme aus verschiedenen Lastfällen aufaddiert werden. Die Theorie 1. Ordnung geht dabei von sehr kleinen Verformungen sowie eines linear elastischen Systemverhaltens aus. Dieses Prinzip wird auch als Superpositionsprinzip bezeichnet und wird im Skript in Kapitel 5 näher beschrieben.