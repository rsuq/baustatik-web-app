from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Color scales
colors = px.colors.sample_colorscale(
    [
        style['color']['danger'],
        style['color']['warning'], 
        style['color']['light'], 
        style['color']['info'], 
        style['color']['primary']
    ], 
    np.linspace(0,1,11), 
    colortype='rgb'  
)

# Figure layout
fig_layout = go.Layout(
    xaxis=dict(
        range=[-900, 900],
        showgrid=False,
        zeroline=False,
        showticklabels=False,
        fixedrange=True,
    ),
    yaxis=dict(
        range=[-1000, 900],
        showgrid=False,
        zeroline=False,
        showticklabels=False,
        fixedrange=True,
    ),
    coloraxis=dict(
        cmid=0, 
        colorscale=colors, 
        colorbar=dict(
            x=1.01, 
            xanchor='left',
            ticksuffix='MPa', 
            nticks=5,
            tickfont=dict(size=style['font_size']['sm']),
            exponentformat='e',
            title=dict(
                text='𝜎<sub>𝑥𝑥</sub>',
                font=(dict(size=style['font_size']['xl']+10))
            )  
        ) 
    ),
    showlegend=False,
    transition={'duration': 500, 'easing': 'cubic-in-out','ordering':'traces first'},
    font=dict(size=style['font_size']['sm']),
    margin=dict(t=80, b=0),
    height=920,   
)

# Layout
def layout(T_id):
    # Parameters
    shape_db = dcc.Store(
        id = f'shape-db-{app_id}',
        data = [
            700,
            1000,
            100,
            70
        ]
    )

    # Normal stress parameters
    n_slider = dcc.Slider(
        id=f'n-input-{app_id}',
        min=-1,
        max=1,
        value=0.5,
        step=0.1,
        marks={str((int(value) if value.is_integer() else value)): {'label': str(int(value) if value.is_integer() else value), 'style': {'font-size': style['font_size']['sm']}}
            for value in np.arange(-1, 1.1, 0.5)},
        updatemode='drag',
        included=False,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    my_slider = dcc.Slider(
        id=f'my-input-{app_id}',
        min=-100,
        max=100,
        value=100,
        step=5,
        marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(-100, 110, 50)},
        updatemode='drag',
        included=False,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    mz_slider = dcc.Slider(
        id=f'mz-input-{app_id}',
        min=-100,
        max=100,
        value=100,
        step=5,
        marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(-100, 110, 50)},
        updatemode='drag',
        included=False,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    # Shear stress parameters
    vy_slider = dcc.Slider(
        id=f'vy-input-{app_id}',
        min=-1,
        max=1,
        value=0.5,
        step=0.1,
        marks={str((int(value) if value.is_integer() else value)): {'label': str(int(value) if value.is_integer() else value), 'style': {'font-size': style['font_size']['sm']}}
            for value in np.arange(-1, 1.1, 0.5)},
        updatemode='drag',
        included=False,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    vz_slider = dcc.Slider(
        id=f'vz-input-{app_id}',
        min=-1,
        max=1,
        value=0.5,
        step=0.1,
        marks={str((int(value) if value.is_integer() else value)): {'label': str(int(value) if value.is_integer() else value), 'style': {'font-size': style['font_size']['sm']}}
            for value in np.arange(-1, 1.1, 0.5)},
        updatemode='drag',
        included=False,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    shape_switch = html.Div(
        [
            #dbc.Label("Cross section:"),
            dbc.RadioItems(
                options=[
                    {"label": T['apps'][app_id]['rectangular'][T_id], "value": 'Rectangular', 'disabled': False},
                    {"label": T['apps'][app_id]['elliptical'][T_id], "value": 'Elliptical', 'disabled': True},
                    {"label": T['apps'][app_id]['i_profile'][T_id], "value": 'I-profile'},
                    {"label": T['apps'][app_id]['l_profile'][T_id], "value": 'L-profile'},
                    {"label": T['apps'][app_id]['u_profile'][T_id], "value": 'U-profile'},
                ],
                value='I-profile',
                id=f'shape-{app_id}',
            ),
        ],
        style={'display': 'flex',
    'justify-content': 'center'},
    )

    # Parameters for dimensions input box
    label_width = 35
    description_width = 90
    box_margin = 20
    dim_margin = 5

    dim_input = html.Div(
        [
            dbc.InputGroup(
                [
                    dbc.InputGroupText("𝑏", style={'width':label_width, 'color':'white','background-color':style['color']['info']}), 
                    dbc.Input(value=700, step=10, type='number', disabled=False, id = {'type': f'dim-{app_id}', 'index': 0}, valid=True), 
                    dbc.InputGroupText(T['apps'][app_id]['width'][T_id], style={'width':description_width, 'padding-left': 23})
                ],
                style={'margin-bottom':dim_margin}
            ),
            dbc.InputGroup(
                [
                    dbc.InputGroupText("ℎ", style={'width':label_width, 'color':'white','background-color':style['color']['info']}), 
                    dbc.Input(value=1000, step=10, type='number',  disabled=False, id={'type': f'dim-{app_id}', 'index': 1},valid=True), 
                    dbc.InputGroupText(T['apps'][app_id]['height'][T_id], style={'width':description_width, 'padding-left': 21})
                ],
                style={'margin-bottom':dim_margin}
            ),
            dbc.InputGroup(
                [
                    dbc.InputGroupText("𝑡", style={'width':label_width, 'color':'white','background-color':style['color']['info']}), 
                    dbc.Input(value=100, step=10, type='number', disabled=False, id={'type': f'dim-{app_id}', 'index': 2}, valid=False), 
                    dbc.InputGroupText(T['apps'][app_id]['thickness'][T_id], style={'width':description_width})
                ],
                style={'margin-bottom':dim_margin}
            ),
            dbc.InputGroup(
                [
                    dbc.InputGroupText("𝑠", style={'width':label_width, 'color':'white','background-color':style['color']['info']}), 
                    dbc.Input(value=70, step=10, type='number', disabled=False, id={'type': f'dim-{app_id}', 'index': 3}, valid=False), 
                    dbc.InputGroupText(T['apps'][app_id]['thickness'][T_id], style={'width':description_width})
                ],
                style={'margin-bottom':dim_margin}
            ),
        ],
        style={'margin-left':box_margin, 'margin-right':box_margin}
    )

    refresh_colorscale = dbc.Button(
            [
                html.I(className='fas fa-redo-alt me-2'),
                T['apps'][app_id]['color_scale'][T_id]
        ],
        n_clicks=0,
        value=5,
        color='secondary', 
        className="me-1",
        id=f'refresh-colorscale-{app_id}'
    )

    dim_alert = dbc.Alert(
        [
            html.I(className='bi bi-exclamation-circle me-2'),
            html.Div('Please insert a valid dimension',id=f'dim-msg-{app_id}', style={'margin-bottom':-15, 'display':'inline-block'})
        ],
        dismissable=True,
        fade=True,
        is_open=False,
        #duration='10000',
        color='danger',
        id=f"dim-alert-{app_id}"
    )

    # Page layout
    theory_box = helper.theory_box(app_id, T_id, module_path, img=True)

    padding = 17
    parameters = html.Div(
        [
            shape_db,
            dbc.Tabs(
                [
                    dbc.Tab(
                        [
                            html.Br(),
                            dcc.Markdown(f"{T['apps'][app_id]['normal_force'][T_id]} **$N$ \[MN]**", style={'textAlign': 'center'}),
                            n_slider,
                            html.Br(),
                            dcc.Markdown(f"{T['apps'][app_id]['bending_moment'][T_id]} **$M_{{\eta'}}$ \[MN.m]**", style={'textAlign': 'center'}),
                            my_slider,
                            html.Br(),
                            dcc.Markdown(f"{T['apps'][app_id]['bending_moment'][T_id]} **$M_{{\zeta'}}$ \[MN.m]**", style={'textAlign': 'center'}),
                            mz_slider
                        ],
                        label='Normal stress',
                        tab_id=f'tab-normal-{app_id}',
                        label_style={'padding-left':padding, 'padding-right':padding},
                        tab_style={'margin':'auto','cursor': 'pointer'}
                    ),
                    dbc.Tab(
                        [
                            html.Br(),
                            dcc.Markdown(f"{T['apps'][app_id]['shear_force'][T_id]} **$V_{{\eta'}}$ \[MN]**", style={'textAlign': 'center'}),
                            vy_slider,
                            html.Br(),
                            dcc.Markdown(f"{T['apps'][app_id]['shear_force'][T_id]} **$V_{{\zeta'}}$ \[MN]**", style={'textAlign': 'center'}),
                            vz_slider
                        ],
                        label='Shear stress',
                        tab_id=f'tab-shear-{app_id}',
                        label_style={'padding-left':padding, 'padding-right':padding},
                        tab_style={'marginRight':'auto','cursor': 'pointer'}
                    )
                ],
                active_tab=f'tab-normal-{app_id}',
                id=f'tabs-{app_id}'
            ),   
            html.Br(), 
            html.Hr(),
            dcc.Markdown(T['apps'][app_id]['cross_shape'][T_id], style={'textAlign': 'center'}),
            shape_switch,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['cross_dimensions'][T_id]} **\[mm]**", style={'textAlign': 'center'}),
            dim_input,
            html.Br(),
            dim_alert,
        ]
    )

    graphs = html.Div(
        [
            dcc.Graph(figure=go.Figure(layout=fig_layout), config={'displayModeBar':False}, id=f'graph-{app_id}', style={'margin-bottom':-20}),
            html.Div(refresh_colorscale, className='d-md-flex justify-content-md-end', style={'margin-bottom':4, 'position':'relative', 'top':'-900px'})    
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout