from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Calbacks

# Update dimensions database
@app.callback(
    Output({'type': f'dim-{app_id}', 'index': ALL}, 'value'),
    Output({'type': f'dim-{app_id}', 'index': ALL}, 'valid'),
    Output({'type': f'dim-{app_id}', 'index': ALL}, 'invalid'),
    Output({'type': f'dim-{app_id}', 'index': ALL}, 'disabled'),
    Output(f'shape-db-{app_id}', 'data'),
    Output(f'dim-alert-{app_id}', 'is_open'),
    Output(f'dim-msg-{app_id}', 'children'),
    Input(f'shape-{app_id}','value'),
    Input({'type': f'dim-{app_id}', 'index': ALL}, 'value'),
    State({'type': f'dim-{app_id}', 'index': ALL}, 'valid'),
    State({'type': f'dim-{app_id}', 'index': ALL}, 'invalid'),
    State({'type': f'dim-{app_id}', 'index': ALL}, 'disabled'),
    State(f'shape-db-{app_id}', 'data'),
    State(f'dim-msg-{app_id}', 'children'),
    prevent_initial_call=True
)
def update_shapedb(shape, value_list, valid_list, invalid_list, disabled_list, data, alert_msg):  

    # Get updated dimension
    context = dash.callback_context.triggered[0]['prop_id'].split('.')[0]
    aux=False

    # Shape changed
    if context[0]!='{': 
        if shape=='Rectangular':
            # Turning off t and s dimensions
            disabled_list[-2:] = [True, True]
            valid_list[-2:] = invalid_list[-2:] = [False, False]
            value_list[-2:] = [None, None]
            data[-2:] = [100, 100]
        elif shape=='Elliptical':
            # Turning off s dimensions
            disabled_list[-2:] = [False, True]
            valid_list[-2:] = [True, False]
            invalid_list[-2:] = [False, False]
            value_list[-2:] = [data[-2], None]
            data[-1] = 100
        else: 
            # Reset t and s dimensions
            disabled_list[-2:] = [False, False]
            valid_list[-2:] = [True, True]
            value_list[-2:] = data[-2:]

    # Dimension changes
    else: 
        # JSON serializable
        context = json.loads(context)
        context = context['index']

        # Fix dimesion
        if value_list[context] is None:
            value_list[context]=0
            aux=True

        ## Define range
        if context==0: # b
            lower_limit = max(2,data[3]*2+10) # x2 because U-profile has two vertrical bars
            upper_limit = 1000
        elif context==1: # h
            lower_limit = max(2,data[2]*2+10)
            upper_limit = 1000
        elif context==2: # t
            lower_limit = 10
            upper_limit = min(400, (data[1]-10)/2) # ÷2 because I-profile has two horizontal bars
        else: # s
            lower_limit = 10
            upper_limit = min(400, (data[0]-10)/2) # ÷2 because U-profile has two vertrical bars

        # Dim validation
        condition = lower_limit<=value_list[context]<=upper_limit
        valid_list[context] = condition
        invalid_list[context] = not condition    
        
        # Writing in database and open/close alert:
        if valid_list[context]:
            data[context] = value_list[context]
            
        else:
        # Invalid dimensions 
            alert_msg = ['Valid range:', html.B(f' {lower_limit} to {upper_limit} mm.')]

    # Open/close alert
    alert_is_open = any(invalid_list)

    if aux:
        value_list[context]=None

    return value_list, valid_list, invalid_list, disabled_list, data, alert_is_open, alert_msg


# Update figure
@app.callback(
    Output(f'graph-{app_id}', 'figure'),
    Output(f'refresh-colorscale-{app_id}', 'value'),
    Output(f'shape-{app_id}', 'value'),
    Output(f'shape-{app_id}', 'options'),
    State('T_id', 'data'),
    State(f'graph-{app_id}', 'figure'),
    Input(f'shape-{app_id}', 'value'),
    Input(f'shape-{app_id}', 'options'),
    Input(f'shape-db-{app_id}', 'data'),
    Input(f'tabs-{app_id}', 'active_tab'),
    Input(f'n-input-{app_id}', 'value'),
    Input(f'my-input-{app_id}', 'value'),
    Input(f'mz-input-{app_id}', 'value'),
    Input(f'vy-input-{app_id}', 'value'),
    Input(f'vz-input-{app_id}', 'value'),
    Input(f'refresh-colorscale-{app_id}', 'n_clicks'),
    State(f'refresh-colorscale-{app_id}', 'value')
    
)
def update_figure(T_id, fig, shape, shape_options, shape_db, tab, N, Meta, Mzeta, Veta, Vzeta, refresh, max_stress):
    time.sleep(0.13)

    # Fig layout
    fig_layout = fig['layout']
    fig_layout.pop('annotations', None)
    fig_layout.pop('shapes', None)

    # Get callback context
    context = dash.callback_context.triggered[0]['prop_id'].split('.')[0]

    # Load dimensions
    b, h, t, s = shape_db

    # Unit transformation 
    N = N*1e6 # MN to N
    Meta = Meta*1e6 # MN to N
    Mzeta = Mzeta*1e6 # MN to N
    Veta = Veta*1e6 # MN to N
    Vzeta = Vzeta*1e6 # MN to N

    # Tab selection (Normal/shear stress)
    if tab==f'tab-normal-{app_id}':
        shape_options[0]['disabled'] = shape_options[1]['disabled'] = False
        
    else:
        # Valid shape options
        shape_options[0]['disabled'] = shape_options[1]['disabled'] = True
        if shape in ('Rectangular','Elliptical'):
            shape = 'I-profile'
    
    # Graphical settings (offsets, etc)
    margin=0
    point_gap=10 
    offset_arrow=240
    offset_tri = 10

    # Shape generation
    CrossSection = solvers.ComplexCrossSection(b, h, t, s)
    if shape=='Rectangular':
        CrossSection.add_element(-b/2, b/2, -h/2, h/2, t, point_gap, 'rectangular', margin)
    
    elif shape=='Elliptical':
        CrossSection.add_ellipse()

    elif 'I-profile' in shape:
        # warning: the point_gap shift is reducing the bar
        CrossSection.add_element(-s/2, s/2, -h/2+t, h /2-t, s, point_gap, 'I-vertical', margin)
        CrossSection.add_element(-b/2, b/2, h/2-t, h/2, t, point_gap, 'I-top', margin)
        CrossSection.add_element(-b/2, b/2, -h/2, -h/2+t, t, point_gap, 'I-bottom', margin)
        
    elif 'U-profile' in shape:
        # warning: the point_gap shift is reducing the bar
        CrossSection.add_element(-b/2, -b/2+s, -h/2+t, h/2-t, s, point_gap, 'U-vertical', margin)
        CrossSection.add_element(-b/2, b/2, h/2-t, h/2, t, point_gap, 'U-top', margin)
        CrossSection.add_element(-b/2, b/2, -h/2, -h/2+t, t, point_gap, 'U-bottom', margin)

        
    elif 'L-profile' in shape:
        CrossSection.add_element(-b/2, -b/2+s, -h/2, h/2-t, s, point_gap, 'L-vertical', margin)
        CrossSection.add_element(-b/2, b/2, h/2-t, h/2, t, point_gap, 'L-top', margin)



    # Calculate center of mass
    length = 300
    CrossSection.get_center_of_mass()
    x_axis = np.array([-length, 0, 0]) + CrossSection.center_of_mass[0]
    y_axis = np.array([0, 0, -length]) + CrossSection.center_of_mass[1]

    # Calculate principal axes
    CrossSection.moment_inertia()
    x_axis_rotated = -(length-100)*np.array([np.cos(CrossSection.theta), 0, -np.sin(CrossSection.theta)]) + CrossSection.center_of_mass[0]
    y_axis_rotated = -(length-100)*np.array([np.sin(CrossSection.theta), 0, np.cos(CrossSection.theta)]) + CrossSection.center_of_mass[1]
    
    if tab==f'tab-normal-{app_id}':
        # Stress calculation 
        stress_field = CrossSection.normal_stress(CrossSection.eta, CrossSection.zeta, N, Meta, Mzeta)
        
    else:
        # Calculate first order moment of inertia
        stress_field = CrossSection.shear_stress(Veta, Vzeta)

    # Neutral axis calculation
    neutral_eta = [-1100, 1100]
    neutral_zeta = CrossSection.neutral_axis(neutral_eta[0], neutral_eta[1], N, Meta+1e-6, Mzeta)

    ## Traces
    traces = []

    # Traces - Cross-section shape with stress colorscale
    cross_points = go.Scatter(
        x=CrossSection.eta,
        y=CrossSection.zeta,
        mode = "markers",
        marker_symbol ='circle',
        marker=dict(
                color=stress_field, # Stress or strain visualization
                size=8,
                coloraxis='coloraxis'
            ),
        text=stress_field,
        #hoverinfo='skip',
        hovertemplate = '%{text:.2f} MPa <extra>𝜎<sub>𝑥𝑥</sub></extra>' if tab==f'tab-normal-{app_id}' else '%{text:.2f} MPa <extra>𝜏</extra>',
        name=''
    )
    traces.append(cross_points)    

    # Traces - neutral axes
    if tab==f'tab-normal-{app_id}': 
        neutral_axes = go.Scatter(
            x=neutral_eta,
            y=neutral_zeta,
            line=dict(
                dash='dash',
                color=style['color']['secondary']
            ),
            mode='lines',
            hoverinfo='skip',
        )
        traces.append(neutral_axes)  

    # Filling trace (to limit neutral axes)
    def filling(dim, horizontal, positive):
        full_range = [-1100,-1100,1100,1100,-1100]
        partial_range = np.array([dim,1100,1100,dim,dim])
        partial_range = partial_range if positive else -partial_range

        x = partial_range if horizontal else full_range
        y = full_range if horizontal else partial_range
        
        filling_trace = go.Scatter(
            x=x,
            y=y,
            mode='text',
            fill='toself',
            fillcolor='white',
            hoverinfo='skip'
        )
        traces.append(filling_trace)

    gap = 50
    filling(b/2 + gap, True, True)
    filling(b/2 + gap, True, False)
    filling(h/2 + gap, False, True)
    filling(h/2 + gap, False, False)

    # Traces - neutral axes (legend)
    legend = {
        f"&nbsp;&nbsp;{T['apps'][app_id]['neutral_axis'][T_id]}":['dash','lines+text',None,style['color']['secondary']],
        f"&nbsp;{T['apps'][app_id]['principal_axes'][T_id]}": [None,'lines+text+markers','circle','black'],
        # f"&nbsp;{T['apps'][app_id]['centroidal_axes'][T_id]}": ['dashdot','lines+text+markers',['circle','triangle-right'], 'black'],
        f"&nbsp;{T['apps'][app_id]['centroidal_axes'][T_id]}": ['dashdot','lines+text+markers',['circle','triangle-right'], style['color']['secondary']],
    }
    pos_x = 110
    pos_y = -800
    for axis, props in legend.items(): 
        neutral_legend = go.Scatter(
            x=[pos_x,pos_x+100],
            y=[pos_y,pos_y],
            line=dict(
                dash=props[0],
                color=props[3]
            ),
            mode=props[1],
            marker_symbol=props[2],
            text=['',axis],
            textposition='middle right',
            hoverinfo='skip',
            textfont=dict(
                color=style['color']['primary'],
                size=style['font_size']['md']
            )
        )
        traces.append(neutral_legend) 
        pos_y -= 50

    # Traces - principal axes
    principal_axes = go.Scatter(
        x=x_axis_rotated,
        y=y_axis_rotated,
        marker_symbol='circle',
        marker=dict(
            size=7,
            color='black',
            opacity=1,
            line=dict(width=0)
        ),
        hoverinfo='skip',
        #line=dict(dash='dashdot'),
        mode='markers+lines+text',
        text=["y", "S", "z"],
        textposition=['top left', 'top right', 'top right'],
        textfont=dict(
            size=style['font_size']['md'],
            color='black'
        )
    )
    traces.append(principal_axes)

    # Traces - Center of mass     
    center_of_mass = go.Scatter(
        x=x_axis,
        y=y_axis,
        marker_symbol=['triangle-left', 'circle', 'triangle-down'],
        marker=dict(
            size=[12,10,12],
            # color='black',
            color=style['color']['secondary'],
            opacity=1,
            line=dict(width=0)
        ),
        line=dict(dash='dashdot', width=1),
        mode='markers+lines+text',
        # text=["𝜂'","S" , "𝜁'"],
        text=["𝜂'"," " , "𝜁'"],
        textposition=['bottom left', 'top right', 'bottom right'],
        textfont=dict(
            size=style['font_size']['md'],
            # color='black'
            color=style['color']['secondary']
        ),
        hoverinfo='skip',
    )
    traces.append(center_of_mass)


    # Dimension arrows
    # b arrow
    arrow_b = helper.dimension_scatter(
        x = [-b/2+offset_tri, 0 ,b/2-offset_tri],
        y = [h/2+offset_arrow]*3,
        text = f'𝑏={b}',
        orientation = 'h',
        color=style['color']['primary'],
        font_size=style['font_size']['md']
    )
    traces.append(arrow_b)

    # h arrow
    arrow_h = helper.dimension_scatter(
        x = [b/2+offset_arrow]*3,
        y = [-h/2+offset_tri, 0, h/2-offset_tri],
        text = f'  ℎ={h}',
        orientation = 'v',
        color=style['color']['primary'],
        font_size=style['font_size']['md']
    )
    traces.append(arrow_h)                  

    if shape!='Rectangular':
        # t arrow
        arrow_t = helper.dimension_scatter(
            x = [b/2+offset_arrow/2]*3,
            y = [h/2-t+offset_tri, h/2-t/2, h/2-offset_tri],
            text = f' 𝑡={t}',
            orientation = 'v',
            color=style['color']['primary'],
            font_size=style['font_size']['md']
        )
        traces.append(arrow_t)

        if 'profile' in shape:
            # s arrow
            arrow_s = helper.dimension_scatter(
                x = [-s/2+offset_tri, 0, s/2-offset_tri] if shape=='I-profile' else [-b/2+offset_tri, -b/2+s/2, -b/2+s-offset_tri],
                y = [h/2+offset_arrow/2]*3,
                text = f'𝑠={s}',
                orientation = 'h',
                color=style['color']['primary'],
                font_size=style['font_size']['md'],
            )
            traces.append(arrow_s)

    # Figure
    fig = go.Figure(data=traces, layout=fig_layout)

    notation = '.1e'
    fig.add_annotation(
                x=-750,
                y=-850, 
                arrowside='start',
                text=f"  𝐼<sub>𝜂'</sub> = {format(CrossSection.Ieta, notation)} mm<sup>4</sup>         𝛢 = {format(CrossSection.area, notation)} mm<sup>2</sup>   <br>  𝐼<sub>𝜁'</sub> = {format(CrossSection.Izeta, notation)} mm<sup>4</sup>         θ = {int(CrossSection.theta*180/np.pi)}°<br>  𝐼<sub>𝜂𝜁'</sub> = {format(CrossSection.Ietazeta, notation)} mm<sup>4</sup>",
                showarrow=False,
                font={'size': style['font_size']['lg'], 'color': style['color']['primary']},
                bordercolor=style['color']['primary'],
                borderwidth=style['line_width']['md'],
                borderpad=6.9,
                bgcolor=style['color']['light'],
                opacity=0.8,
                align='left',
                xanchor='left',
        ) 

    if context==f'refresh-colorscale-{app_id}':
        max_stress = round(max(abs(stress_field)+1e-6),1)
    
    fig.update_layout(
        coloraxis=dict(
            cmin=-max_stress,
            cmax=max_stress,
            colorbar=dict(
            title=dict(
                text='𝜎<sub>𝑥𝑥</sub>' if tab==f'tab-normal-{app_id}' else '𝜏',
                font=(dict(size=style['font_size']['xl']+2))
                )
            )
        ),
        title=f"{T['apps'][app_id]['normal_stress'][T_id]} - <b>𝜎<sub>𝑥𝑥</sub></b>" if tab==f'tab-normal-{app_id}' else f"{T['apps'][app_id]['shear_stresses'][T_id]} - <b>𝜏<sub>𝑥𝜂'</sub>, <b>𝜏<sub>𝑥𝜏'</sub></b>"
    )

    return fig, max_stress, shape, shape_options


