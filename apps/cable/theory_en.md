In the general case, the geometry of a *rigid cables* is given by the following differential equation 

$$ z''(x) + \frac{q(x)}{H} = 0, \tag{1} $$

where $z(x)$ is the vertical location of the cable, $q(x)$ is a vertical load and $H$ is the horizontal component of the cable force. This equation can be solved with two boundary conditions (e.g., $z(0)=a$ and $z(\ell)=h$).

In practice, this equation can also be solved by analogy with a simply supported beam. As was shown in the script in Chapter 3, this leads to the following solution of Eq. 1.
$$ z(x) = \frac{M(x)}{H} + \frac{h}{\ell}x, \tag{2}$$
where $M(x)$ is the moment in a simply supported beam with span $\ell$ resulting from the applied vertical load $q(x)$, and $h$ is the height difference between the cable suspension points $z(0)$ and $z(\ell)$. Note that a change in the boundary conditions would only affect the second term of  Eq. 2.

