Im allgemeinen Fall wird die Geometrie eines *dehnstarren Seils* durch die folgende Differentialgleichung beschrieben

$$ z''(x) + \frac{q(x)}{H} = 0, \tag{1} $$

wobei $z(x)$ die vertikale Lage des Seils ist, $q(x)$ eine vertikale Last und $H$ die horizontale Komponente der Seilkraft. Diese Gleichung kann mittels zweier Randbedingungen gelöst werden (z.B. $z(0)=a$ und $z(\ell)=h$).

In der Praxis kann diese Gleichung auch in Analogie zu einem einfach gelagerten Balken gelöst werden. Wie im Skript in Kapitel 3 gezeigt wurde, führt dies zu folgender Lösung von Gleichung 3:

$$ z(x) = \frac{M(x)}{H} + \frac{h}{\ell}x, \tag{2}$$

Dabei ist $M(x)$ das Moment in einem einfach gelagerten Balken mit Spannweite $\ell$, das sich aus der aufgebrachten Vertikallast $q(x)$ ergibt, und $h$ ist der Höhenunterschied zwischen den Seilaufhängungspunkten $z(0)$ und $z(\ell)$. Man beachte, dass eine Änderung der Randbedingungen nur den zweiten Term von Gleichung 2 beeinflussen würde.