from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Layout

# Figure default layout
def fig_layout(y_range):
    fig_layout = go.Layout(
            xaxis=dict(
                domain=[0, 1],
                range=[-3.7, 13.7],
                tickmode='array',
                showgrid=True,
                zeroline=False,
                showticklabels=False,  
                fixedrange=True,                  
            ),
            yaxis=dict(
                domain=[0, 1],
                range=y_range,
                showgrid=True,
                zeroline=False,
                showticklabels=False,
                fixedrange=True, 
                tick0=0, 
                dtick=2
            ),
        )
    return fig_layout

def layout(T_id):
    h_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['horizontal_force'][T_id]} **𝐻 \[kN]**", style={'textAlign': "center"}),
        dcc.Slider(
            id='h-input',
            min=100,
            max=200,
            value=100,
            step=5,
            marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
                for value in range(100, 201, 25)},
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    l_slider = html.Div([
        dcc.Markdown(f"{T['apps'][app_id]['span'][T_id]} **ℓ \[m]**", style={'textAlign': "center"}),
        dcc.Slider(
            id='l-input',
            min=1,
            max=10,
            value=10,
            step=0.5,
            marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
                for value in range(1, 11, 3)},
            updatemode='drag',
            included=True,
            tooltip={"placement": "bottom", "always_visible": False}
        )
    ])

    z0_slider = html.Div([
        dcc.Markdown('**𝓏(0)**',style={'align-self': 'center','margin-right':'5px'}),
        dcc.Slider(
            id='z0-input',
            min=-3,
            max=3,
            value=0,
            step=0.5,
            marks={str(value).replace('.0',''): {'label': str(-value).replace('.0',''), 'style': {'font-size': style['font_size']['sm']}}
                for value in np.linspace(-3, 3, 5)},
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False},
            vertical=True,
            verticalHeight=200,
            className='mt-50'
        )
    ], style={'display':'flex'})

    zl_slider = html.Div([
        dcc.Markdown('**𝓏(ℓ)**',style={'align-self': 'center','margin-right':'5px'}),
        dcc.Slider(
            id='zl-input',
            min=-3,
            max=3,
            value=0,
            step=0.5,
            marks={str(value).replace('.0',''): {'label': str(-value).replace('.0',''), 'style': {'font-size': style['font_size']['sm']}}
                for value in np.linspace(-3, 3, 5)},
            updatemode='drag',
            included=False,
            tooltip={"placement": "bottom", "always_visible": False},
            vertical=True,
            verticalHeight=200,
        )
    ], style={'display':'flex'})

    z_sliders = html.Div([
        html.Div(z0_slider, style={'margin-right': '20px'}),
        html.Div(zl_slider, style={'margin-left':'20px'}),
        ], style= {'margin-top':-10, 'display': 'flex','justify-content': 'center'}
    )

    # New load generator
    LoadGenerator = helper.LoadGenerator(T_id, app_id)
    load_db, load_container = LoadGenerator.create_db()

    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = html.Div(
        [
            h_slider,
            html.Br(),
            l_slider,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['vertical_position'][T_id]} **𝓏 \[m]**", style={'textAlign': "center"}),
            z_sliders,
            html.Hr(),
            dcc.Markdown(f"{T['apps'][app_id]['acting_loads'][T_id]} **𝐹**/**𝑞**", style={'textAlign': "center"}),
            load_container,
            html.Div(id=f'empty-{app_id}'),
            load_db, 
        ]
    )

    graphs = html.Div(
        [
            dcc.Graph(figure = go.Figure(layout=fig_layout([-7.3, 5.2])), config={'displayModeBar':False}, id=f'cable-graph-{app_id}'),
            dcc.Graph(figure = go.Figure(layout=fig_layout([-4.5,2])), config={'displayModeBar':False}, id=f'beam-graph-{app_id}'),
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout
