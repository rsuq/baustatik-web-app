from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

## Calbacks
LoadGenerator = helper.LoadGenerator(0, app_id)
signals = LoadGenerator.load_edit_signals()

# Load generator
@app.callback(
    Output(f'empty-{app_id}', 'children'),
    Input('T_id', 'data')
)
def load_gen(T_id):
    global LoadGenerator, signals
    LoadGenerator = helper.LoadGenerator(T_id, app_id)
    signals = LoadGenerator.load_edit_signals()

    return None


# Edit existing load
@app.callback(
    signals['Output'],
    signals['Input'],
    signals['State'],
)
def load_edit(*args):
    return LoadGenerator.load_edit(*args)


# Add new load + delete existing load
signals = LoadGenerator.add_new_load_signals()
@app.callback(
    signals['Output'],
    signals['Input'],
    signals['State'],
)
def add_new_load(*args):
    return LoadGenerator.add_new_load(*args)

# Load input form validation
signals = LoadGenerator.valid_input_signals()
@app.callback(
    signals['Output'],
    signals['Input'],
    signals['State'],
)
def valid_input(*args):
    return LoadGenerator.valid_input(*args)


# Re-usable function to create figures
def create_figure(T_id, H, l, z0, zl, load_label, load_magnitude, load_position, fig_height, fig_layout, mt=0, mb=0, type='cable'):
    # Removing last load, because is not defined yet
    load_label = load_label[:-1] 
    load_magnitude = load_magnitude[:-1] 
    load_position = load_position[:-1] 
    
    # Set load_position to absolute, for calculations
    load_position = [float(pos) * l if pos != None else pos for pos in  load_position]

    H += 0.001
    
    # Beam sketch trace
    trace_beam = go.Scatter(
        x=[0, l],
        y=[z0, zl],
        mode='lines+markers+text',
        text=['A', 'B'],
        xaxis='x',
        yaxis='y',
        fill=None,
        textposition=['top left', 'top right'],
        line=dict(
            width=style['line_width']['md' if type=='cable' else 'lg'], 
            dash='longdash' if type=='cable' else 'solid'
        ),
        marker=dict(size=10,
                    color=style['color']['primary']),
        textfont=dict(
            color=style['color']['primary'],
            size=style['font_size']['xl']
        )
    )
    
    # Cable vertical position trace
    Calcs = []
    for i in range(len(load_magnitude)):
        Calcs.append(
            solvers.SimplySupportedBeam(
                F=load_magnitude[i], 
                q=load_magnitude[i], 
                a=load_position[i], 
                l=l, 
                load_mode='distributed-load' if load_label[i]=='𝑞' else 'point-load'
            )
        )
    solution_space = np.linspace(0,l,101)
    Calcs = np.array(Calcs)
    z_abs = [
        solvers.cable_location(
            x=x, 
            M=sum((calc.moment(x)) for calc in Calcs.T), 
            H=H, 
            z0=z0, 
            zl= zl, 
            l=l
        )[0] # absolute z 
        for x in solution_space
    ]
    z_rel = [
        solvers.cable_location(
            x=x, 
            M=sum((calc.moment(x)) for calc in Calcs.T), 
            H=H if type == 'cable' else 1, 
            z0=z0, 
            zl= zl, 
            l=l
        )[1] # relative z 
        for x in solution_space
    ]
    # Labeling only the max displacement/moment
    text_array = [' ' for x in solution_space]
    argmin = z_rel.index(min(z_rel))
    if sum(z_rel)==0:
        pass
    elif type == 'cable':
        text_array[argmin] = f'ℎ<sub>max</sub>={round(-z_rel[argmin],2)} m'  
    else: 
        text_array[argmin] = f'𝑀<sub>max</sub>={int(-z_rel[argmin])} kN.m'
    
    trace_z = go.Scatter(
        x=solution_space,
        y=z_abs,
        text=text_array,
        textposition='bottom left' if z0>zl else 'bottom right',
        fill='tonexty',
        mode='lines+text',
        xaxis='x',
        yaxis='y',
        line=dict(color=style['color']['primary'], width=style['line_width']['lg'] if type == 'cable' else None),
        fillcolor = 'rgba(236,240,241,0)' if type =='cable' else None
    )


    data = [trace_beam, trace_z]

    fig = go.Figure(data=data, layout=fig_layout)

    # Axis convention
    fig = helper.axis(
        fig, 
        line_width=style['line_width']['md'], 
        color=style['color']['primary'], 
        font_size=style['font_size']['xl'], 
        x=-3, 
        y=0,
        scale=1.5
    )

    # Supports
    path='assets/geometries'
    support_scale=0.008
    PinnedSupport = helper.SVGPath()
    PinnedSupport.reader(f'{path}/pinned_support.svg')
    PinnedSupport.scale(x=support_scale, y=support_scale)
    PinnedSupport.translate(x=0, y=z0)

    PinnedSupport_2 = helper.SVGPath()
    support_type = 'pinned_support' if type =='cable' else 'roller_support'
    PinnedSupport_2.reader(f'{path}/{support_type}.svg')
    PinnedSupport_2.scale(x=support_scale, y=support_scale)
    PinnedSupport_2.translate(x=l, y=zl)

    fig.add_shape(
        type='path',
        path=PinnedSupport.path+PinnedSupport_2.path,
        fillcolor=None,
        line_color=style['color']['primary'],
        line={'width': style['line_width']['sm']},
        yref='y'
    )

    # Dimensions
    # L dim
    z = zl if z0>zl else z0
    z = z - 4.1 
    fig = helper.dimension(
        fig,
        line_width=style['line_width']['md'], 
        color=style['color']['info'], 
        font_size=style['font_size']['xl'],
        text=f'ℓ',
        start=0,
        end=l,
        position=z,
        yref='y',
        orientation='h',
        text_margin=-0.2,
        tab_size=0.3
    )
    # z's dim
    for text,dim in {'𝓏(0)':[z0,-1,-0.4],'𝓏(ℓ)':[zl,11,0.4]}.items():
        fig = helper.dimension(
            fig,
            line_width=style['line_width']['md'], 
            color=style['color']['info'], 
            font_size=style['font_size']['xl'],
            text=text,
            start=0,
            end=dim[0],
            position=dim[1],
            yref='y',
            orientation='v',
            text_margin=dim[2],
            tab_size=0.3
        )
    
    if type == 'cable':
        # H annontation
        fig.add_annotation(
                x=12,  # arrows' head
                y=4.75,  # arrows' head
                arrowside='start',
                text=f'𝐻={int(H)} kN',
                showarrow=False,
                font={'size': style['font_size']['xl'], 'color': style['color']['primary']},
                bordercolor=style['color']['primary'],
                borderwidth=style['line_width']['md'],
                borderpad=4,
                bgcolor=style['color']['light'],
                opacity=0.8
        )
    
    # ARROWS
    # Max displacement 
    argmin = z_rel.index(min(z_rel))
    fig = helper.dimension(
        fig,
        line_width=style['line_width']['md'], 
        color=style['color']['secondary' if type=='cable' else 'light'], 
        font_size=style['font_size']['xl'],
        text='',
        start=z_abs[argmin]-min(z_rel) if type=='cable' else 0,
        end=z_abs[argmin],
        position=(argmin / (len(z_rel)-1))*l,
        yref='y',
        orientation='v',
        text_margin=-0.2,
        tab_size=0,
        scale=0.8
    )

    arrow_size_factor = 0.15
    # Acting loads Arrows
    for i in range(len(load_label)):
        z = z0 if z0>zl else zl
        z = z + 0.2 
        # Distributed loads
        if load_label[i] == '𝑞':
            for x in np.linspace(0,l,7):
                offset = 0.24 #if q>0 else -0.2 
                q = load_magnitude[i]
                fig.add_annotation(
                    x=x,
                    y=z,
                    ax=x,
                    ay=(z + q*arrow_size_factor+offset) if x==l/2 else (z +q*arrow_size_factor),
                    xref='x',
                    yref='y',
                    axref="x",
                    ayref="y",
                    text=f'{load_label[i]}' if x==l/2 else '',
                    showarrow=True,
                    align="left",
                    arrowhead=1,
                    arrowsize=1.2,
                    arrowwidth=style['line_width']['md'],
                    arrowcolor='#F0A29A',
                    font={'size': style['font_size']['xl'], 'color': '#F0A29A'}
                )
                for y in [z, (z +q*arrow_size_factor)]:
                    fig.add_shape(
                        type='line',
                        x0=0,
                        y0=y,
                        x1=l,
                        y1=y,
                        line_color='#F0A29A',
                        yref="y",
                        line={'width': style['line_width']['md']}
                    )
                
        # Point loads
        else:
            fig.add_annotation(
                    x=load_position[i],  # arrows' head
                    y=z,  # arrows' head
                    ax=load_position[i],  # arrows' tail
                    ay=z + load_magnitude[i]*arrow_size_factor/2,  # arrows' tail
                    arrowside='end',
                    xref='x',
                    yref='y',
                    axref="x",
                    ayref="y",
                    text=f'{load_label[i]}', #= {load_magnitude[i]} kN',
                    showarrow=True,
                    arrowhead=1,
                    arrowsize=1.2,
                    arrowwidth=style['line_width']['md'],
                    arrowcolor=style['color']['danger'],
                    font={'size': style['font_size']['xl'], 'color': style['color']['danger']}
            )
        

    # Update layout
    fig.update_layout(
        yaxis_title=T['apps'][app_id]['cable_geometry' if type=='cable' else 'simply_supported'][T_id],
        height=fig_height,
        showlegend=False,
        font=dict(size=style['font_size']['md']),
        margin=dict(l=0, r=0, t=mt, b=mb),
        #paper_bgcolor="LightSteelBlue",
        plot_bgcolor= 'rgba(236,240,241,0.2)',
        transition={'duration': 300, 'easing': 'cubic-in-out'},
    )
    fig.update_traces(textfont_size=style['font_size']['lg'], hoverinfo='skip')
    #fig.update_yaxes(fixedrange=True, tick0=0, dtick=2)

    return fig



# Figure update
@app.callback(
    [
        Output(f'cable-graph-{app_id}', 'figure'),
        Output(f'beam-graph-{app_id}', 'figure')
    ],
    [
        State('T_id', 'data'),
        State(f'cable-graph-{app_id}', 'figure'),
        State(f'beam-graph-{app_id}', 'figure'),
        Input({'type': f'load-edit-{app_id}', 'index': ALL}, 'n_clicks'),
        Input('h-input', 'value'),
        Input('l-input', 'value'),
        Input('z0-input', 'value'),
        Input('zl-input', 'value'),
    ],
    [
        State({"type": f"load-label-{app_id}", "index": ALL}, 'children'),
        State({"type": f"load-magnitude-{app_id}", "index": ALL}, 'value'),
        State({"type": f"load-position-{app_id}", "index": ALL}, 'value'),
    ]
)
def update_figure(T_id, fig_cable, fig_beam, edit_clicks, H, l, z0, zl, load_label, load_magnitude, load_position):

    # Prevent triggering unwated callbacks
    trigger_name = dash.callback_context.triggered[0]['prop_id'].split('.')[0]
    trigger_value = dash.callback_context.triggered[0]['value']
    trigger_value = 0 if trigger_value==None else trigger_value

    trigger_name='0' if trigger_name == '' else trigger_name

    if ((trigger_value % 2) != 0) & ((trigger_name[0] == '{')): # collapsed mode
        raise PreventUpdate

    # Figure layout
    fig_cable_layout = fig_cable['layout']
    fig_cable_layout.pop('annotations', None)
    fig_cable_layout.pop('shapes', None)
    fig_beam_layout = fig_beam['layout']
    fig_beam_layout.pop('annotations', None)
    fig_beam_layout.pop('shapes', None)
    
    fig_cable = create_figure(T_id, H, l, z0, zl, load_label, load_magnitude, load_position, fig_height=650+20, fig_layout=fig_cable_layout, mb=8, type='cable')
    fig_beam = create_figure(T_id, 100, l, 0, 0, load_label, load_magnitude, load_position, fig_height=330+20, fig_layout=fig_beam_layout, mt=8, type='beam')
    

    return fig_cable, fig_beam
