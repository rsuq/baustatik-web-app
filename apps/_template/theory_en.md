## Layout template

### Implementation steps:

**Metadata**

In the [translate.json]('assets/translate.json') file:
1. Add a new module inside the 'apps' tree
2. Fill in the module metadata
3. Add new strings to be translated

**Deploy**

In [main.py](main.py) file:
1. Add to `# Modules` section
```python
from apps._template import callbacks, layout as app0
``` 

2.  Add to `apps_layout` dictionary
```python
'/app0' : app0.layout
```

**Develop**

In [theory_de.md](apps/_template/theory_de.md) and [theory_en.md](apps/_template/theory_en.md) files
1. Write theory box

In [layout.py](apps/_template/layout.py) file:
1. Add parameters
2. Add graphs  

In the [callbacks.py](apps/_template/callbacks.py) file:
1.  Add callbacks
