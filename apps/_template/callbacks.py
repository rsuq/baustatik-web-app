from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

## Calbacks

# Example
@app.callback(
    Output(f"graph-{app_id}", "children"),
    State(f'T_id', 'data'),
    Input(f"input-{app_id}", "n_clicks")
)
def example(T_id, input):
    output = f"{T['apps'][app_id]['counter'][T_id]}: {input}"
    return output