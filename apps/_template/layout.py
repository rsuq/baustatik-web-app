from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Layout
def layout(T_id):
    # Components
    input = dbc.Button(T['apps'][app_id]['click_me'][T_id], n_clicks=0, id=f'input-{app_id}')

    graph = html.Div('Output', id=f'graph-{app_id}')


    # Page layout
    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = html.Div(
        [
            input
        ]
    )

    graphs = html.Div(
        [
            graph
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout