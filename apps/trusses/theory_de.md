Bei der Modellierung von Fachwerken werden diese häufig als *ideales Fachwerk* behandelt. Dadurch ergeben sich die folgenden Voraussetzungen:

* jeder Träger beginnt und endet in einem Knoten ohne Exzentrizität;
* Knotenpunkte sind Gelenke, die keine Momente übertragen können;
* die Kräfte konzentrieren sich in den Knotenpunkten.

Daraus folgt, dass in einem idealen Fachwerk jeder Träger nur Normalkräfte und keine Querkräfte ($V=0$) oder Momente ($M=0$) erfährt.