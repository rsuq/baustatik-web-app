from apps.cable.layout import fig_layout
from utils.packages_callbacks import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

# Define colorscale
color_min = style['color']['danger']
color_zero = style['color']['light']
color_max = style['color']['info']


## Calbacks

# Buttons color
@app.callback(
        [
            Output({'type':'truss_btn', 'index': ALL}, 'active'),
            Output({'type':'truss_icon', 'index':ALL}, 'src')
        ],
        Input({'type':'truss_btn', 'index': ALL}, 'n_clicks'),
        State({'type':'truss_icon', 'index':ALL}, 'src'),
        prevent_initial_call=True
    )
def set_active(n_clicks, icons):
    
    # General metadata about the trigger
    btn_id = dash.callback_context.triggered[0]['prop_id'].split('.')[0] 
    btn_id = json.loads(btn_id)
    btn_id = btn_id['index']

    # Activate only the clicked button
    active = [False, False, False]
    active[btn_id] = True

    # Edit color of the truss icons
    suffix='_alt'
    for i in range(0,len(icons)):
        icons[i]=icons[i].replace(suffix,'')
        if active[i]:
            icons[i] = icons[i].split('.') 
            icons[i][0] += suffix
            icons[i]='.'.join(icons[i])
        else: 
            pass

    return active, icons



# Call load form
@app.callback(
    Output(f'load-form-{app_id}','children'),
    Output(f'new-load-{app_id}', 'is_open'),
    Output(f'load-db-{app_id}', 'data'),
    Output(f'load-delete-{app_id}','disabled'),
    Output(f'load-h-{app_id}', 'value'),
    Output(f'load-v-{app_id}', 'value'),
    State('T_id', 'data'),
    Input(f'graph-{app_id}', 'selectedData'),
    Input({'type':'truss_btn', 'index': ALL}, 'active'),
    Input('bays-input','value'),
    Input(f'load-cancel-{app_id}','n_clicks'),
    Input(f'load-delete-{app_id}','n_clicks'),
    Input(f'load-validate-{app_id}','n_clicks'),
    State(f'load-db-{app_id}', 'data'),
    State(f'load-h-{app_id}', 'value'),
    State(f'load-v-{app_id}', 'value'),
    State(f'graph-{app_id}', 'figure')
)
def load_form(T_id, select, active, bays, cancel, delete, validate, load_db, mag_h, mag_v, fig): 
    context = dash.callback_context.triggered[0]['prop_id'].split('.')
    nodes_text=[T['apps'][app_id]['select_nodes'][T_id]]

    # Calculate number of nodes
    geom = ['warren', 'howe', 'pratt']
    geom = geom[active.index(True)]

    # Collapse load form
    if select is None or select['points']==[] or context[1] in ('value','active') or context[0]==f'load-cancel-{app_id}':
        is_open = False
        nodes = nodes_text
    # Expand load form
    else:
        nodes = [str(x['text']) for x in select['points']] # Nodes indexes
        is_open = True

    # Validation button  
    if context[0]==f'load-validate-{app_id}':
        for node in nodes:
            load_db[node] = [0 if x is None else x for x in [mag_h, mag_v]]
        is_open = False
        nodes = nodes_text
    # Delete button
    elif context[0]==f'load-delete-{app_id}':
        load_db = {key: value for key, value in load_db.items() if key not in nodes}
        is_open = False
        nodes = nodes_text

    # Filter valid loads, according with existing nodes in the figure
    len_nodes = 2*bays + active[0]  # Get nodes from the figure to filter valid loads
    load_db = {key: value for key, value in load_db.items() if int(key) <= len_nodes}

    ## Check if selected nodes are in load_db
    existing_nodes = [node for node in nodes if node in load_db.keys()]
    # Enable/disable delete button
    del_disabled = not any(existing_nodes)
    # Update load magnitudes if node exists
    if not del_disabled:
        mag_h, mag_v = load_db[existing_nodes[0]]
    else:
        mag_h, mag_v = [None, None]

    if is_open:
        load_db=no_update   
    
    nodes = [dbc.Badge(f'{node}', color='white', text_color='primary', pill=True,  className="border me-1", style={'margin-right':2,'margin-bottom':2}) for node in nodes]
    badge = html.B(
        [ 
            html.Div(T['apps'][app_id]['edit_loads'][T_id], style={'display':'inline','margin-right':7}) 
        ] 
        + nodes
    )

    return badge, is_open, load_db, del_disabled, mag_h, mag_v


# Update load container
@app.callback(
    Output(f'load-container-{app_id}','children'),
    State('T_id', 'data'),
    Input(f'load-db-{app_id}', 'data')
)
def load_container(T_id, load_db):
    rows = []  

    if not load_db:
        load_db['-'] = ['-','-']

    for node, load in load_db.items():
        #if node!='-':
        #    load[0] = f'{load[0]} kN'
        #    load[1] = f'{load[1]} kN'
        row = html.Tr(
            [
                html.Td(node), 
                html.Td(load[0]),
                html.Td(load[1])
            ]
        )
        rows.append(row)

    table_header = [
        html.Thead(
            html.Tr(
                [
                    html.Th(dcc.Markdown(T['apps'][app_id]['node'][T_id],dangerously_allow_html=True,style={'margin-bottom':-15})), 
                    html.Th(dcc.Markdown('&nbsp; 𝐹<sub>H</sub>   &nbsp;\[kN]',dangerously_allow_html=True,style={'margin-bottom':-15})), 
                    html.Th(dcc.Markdown('&nbsp; 𝐹<sub>V</sub>  &nbsp;\[kN]' ,dangerously_allow_html=True,style={'margin-bottom':-15}))
                ]
            )
        )
    ]
    table_body = [html.Tbody(rows)]
    table = dbc.Table(
        table_header + table_body,
        bordered=False,
        hover=True,
        responsive=True,
        size='sm',
        style={'text-align': 'center','max-height': '10px','overflow':'auto'}
    )      

    return table


# Disable load validation
@app.callback(
    Output(f'load-validate-{app_id}','disabled'),
    Input(f'load-h-{app_id}', 'value'),
    Input(f'load-v-{app_id}', 'value')
)
def valid_form(mag_h, mag_v):
    if any([mag_h, mag_v]):
        disabled = False
    else:
        disabled = True
    
    return disabled


# Update figure
@app.callback(
        Output(f'graph-{app_id}', 'figure'),
        Output(f'force-db-{app_id}', 'data'),
        State('T_id', 'data'),
        State(f'graph-{app_id}', 'figure'),
        Input({'type':'truss_btn', 'index': ALL}, 'active'),
        Input('bays-input','value'),
        Input(f'load-db-{app_id}','data'),
        Input(f'h-input-{app_id}','value')
    )
def update_figure(T_id, fig, active, bays, load_db,h):
    time.sleep(0.07)

    l = 10
    #h = 3

    # Figure layout
    fig_layout = fig['layout']
    fig_layout.pop('annotations', None)
    fig_layout.pop('shapes', None)

    # Calculate Truss geometry & forces
    geom = ['warren', 'howe', 'pratt']
    geom = geom[active.index(True)]
    Truss = solvers.Truss(l, h , bays, geom)

    step = l/(bays*2) if geom=='warren' else l/bays

    # Calculate internal forces
    internal_forces = np.round(Truss.internal_forces(load_db),0).astype(int) 
    max_force = max(abs(internal_forces))
    if all(internal_forces==0):
        max_force += 2
    scaled_forces = (internal_forces + max_force)/(2*max_force)
    colors = px.colors.sample_colorscale([color_min, color_zero, color_max], scaled_forces, colortype='rgb')

    # Define traces
    traces = []
    j = 0
    for elements in [Truss.diagonals, Truss.btm_chords,Truss.top_chords]:
        for i in range(0,len(elements),2):
            
            # Create a rectangular shape from a line
            margin=5e-2
            x=elements[i:i+2,0]
            y=elements[i:i+2,1]
            a=x[1]-x[0]
            b=y[1]-y[0]
            alpha = math.atan(b/a)
            x_margin = margin*math.sin(alpha)
            y_margin = margin*math.cos(alpha)

            # Area plot
            if internal_forces[j]>0:
                name = T['apps'][app_id]['tension'][T_id] 
            elif internal_forces[j]<0:
                name = T['apps'][app_id]['compression'][T_id]
            else:
                name = '-'
            
            traces.append(
                go.Scatter(
                    x=np.append(x-x_margin,np.flip(x)+x_margin),
                    y=np.append(y+y_margin,np.flip(y)-y_margin),
                    text=f'{internal_forces[j]} kN',
                    name=name,
                    mode='lines',
                    line=dict(
                        width=0,
                        color=colors[j]
                    ),
                    fill='toself',
                    fillcolor=colors[j],
                    hoveron = 'fills',
                    hoverlabel=dict(align='left') 
                )
            )
            j+=1

        # Dummy plot (to keep the traces ID) 
        for i in range(20-len(elements)//2):
            traces.append(
                go.Scatter(
                    x=[10,10,10.01,10.01],
                    y=[0,0.01,0.01,0],
                    text='dummy',
                    mode='lines',
                    fill='toself',
                    line=dict(
                        width=style['line_width']['sm'],
                        color=style['color']['light']
                    ),
                )
            )

    # Nodes plot
    trace_nodes=go.Scatter(
        x=Truss.nodes[:,0],
        y=Truss.nodes[:,1],
        text=Truss.labels,
        mode='markers+text',
        textfont=dict(
                size=style['font_size']['sm']-2,
                color=style['color']['primary']
        ),
        marker=dict(
            size=19,
            color=style['color']['light'],
            line=dict(
                width=style['line_width']['sm'],
                color=style['color']['primary'],
            )
        ),
        hoveron = 'fills',   
    )


    data=traces+[trace_nodes]
    
    fig = go.Figure(data=data, layout=fig_layout)

    # Axis convention
    fig = helper.axis(
        fig, 
        line_width=style['line_width']['md'], 
        color=style['color']['primary'], 
        font_size=style['font_size']['lg'], 
        x=-0.75, 
        y=3.6,
        scale=1
    )

    # Dimensions
    # L dim
    fig = helper.dimension(
        fig,
        line_width=style['line_width']['md'], 
        color=style['color']['primary'], 
        font_size=style['font_size']['xl'],
        text=f'ℓ=10m',
        start=0,
        end=l,
        position=-1.3,
        yref='y',
        orientation='h',
        text_margin=-0.2,
    )

    # H dim
    fig = helper.dimension(
        fig,
        line_width=style['line_width']['md'], 
        color=style['color']['primary'], 
        font_size=style['font_size']['xl'],
        text=f'ℎ',
        start=0,
        end=h,
        position=l+0.7,
        yref='y',
        orientation='v',
        text_margin=0.2 
    )

    # Supports
    path='assets/geometries'
    support_scale=0.008
    PinnedSupport = helper.SVGPath()
    PinnedSupport.reader(f'{path}/pinned_support.svg')
    PinnedSupport.scale(x=support_scale, y=support_scale)

    RollerSupport = helper.SVGPath()
    RollerSupport.reader(f'{path}/roller_support.svg')
    RollerSupport.scale(x=support_scale, y=support_scale)
    RollerSupport.translate(x=10)

    fig.add_shape(
        type='path',
        path=PinnedSupport.path+RollerSupport.path,
        fillcolor=None,
        line_color=style['color']['primary'],
        line={'width': style['line_width']['sm']},
        yref='y',
        layer='below'
    )

    # Loads
    arrow_size_factor = 0.2
    radius = 0.13
    
    for node, mag in load_db.items():
        node = int(node)

        # Arrow tail position    
        arrow_x = np.log(1+ abs(mag[0])*arrow_size_factor) if mag[0]>=0 else - np.log(1+ abs(mag[0])*arrow_size_factor)
        arrow_y = np.log(1+ abs(mag[1])*arrow_size_factor) if mag[1]>=0 else - np.log(1+ abs(mag[1])*arrow_size_factor)

        # Offset the arrow head to match the node radius
        alpha = math.atan2(arrow_y,arrow_x)
        gap_x = radius*math.cos(alpha)
        gap_y = radius*math.sin(alpha)

        fig.add_annotation(
            x=Truss.nodes[node-1,0] - gap_x,  # arrows' head
            y=Truss.nodes[node-1,1] + gap_y,  # arrows' head
            ax=Truss.nodes[node-1,0] - arrow_x,  # arrows' tail
            ay=Truss.nodes[node-1,1] +  arrow_y,  # arrows' tail
            arrowside='end',
            xref='x',
            yref='y',
            axref='x',
            ayref='y',
            text=f'𝐹<sub>{node}', #= {load_magnitude[i]} kN',
            showarrow=True,
            arrowhead=1,
            arrowsize=1.2,
            arrowwidth=style['line_width']['md'],
            arrowcolor=style['color']['primary'],
            font={'size': style['font_size']['xl'], 'color': style['color']['primary']},
        )
    
    fig.update_layout(title=T['apps'][app_id]['normal_forces'][T_id])

    # Re-write force_db
    force_db={}
    force_db['traces_text']=[trace['text'].split(' ')[0] for trace in traces]
    force_db['max_force']=max_force

    return fig, force_db



# Update color bar with hover
@app.callback(
    Output(f'colorbar-{app_id}', 'figure'),
    State('T_id', 'data'),
    State(f'colorbar-{app_id}', 'figure'),
    Input(f'graph-{app_id}', 'hoverData'),
    Input(f'force-db-{app_id}', 'data'),
    #Input({'type':'truss_btn', 'index': ALL}, 'active'),
)
def update_colorbar(T_id, colorbar, hover, force_db):
    time.sleep(0.35)

    # Figure layout
    colorbar_layout = colorbar['layout']
    colorbar_layout.pop('annotations', None)
    colorbar_layout.pop('shapes', None)

    max_force = force_db['max_force']
    
    try:
        hover = hover['points'][0]['curveNumber'] 
        text = force_db['traces_text'][hover]        
    except:
        text = 0
           

    # Calculate internal forces (for colorbar)
    internal_forces = np.linspace(-max_force,max_force,1000) 
    scaled_forces = (internal_forces + max_force)/(2*max_force)
    colors = px.colors.sample_colorscale([color_min, color_zero, color_max], scaled_forces, colortype='rgb')  

    bar=go.Bar(
        x=internal_forces,
        y=np.zeros(len(internal_forces))+1,
        marker_color=colors,
        marker=dict(line=dict(width=0))
        #orientation='h'
    )

    line=go.Scatter(
        x=[text,text],
        y=[-0.15,1.15],
        line=dict(
            width=style['line_width']['md'],
            color=style['color']['primary']
        ),
        marker=dict(
            size=10,
            color=style['color']['primary']
        ),
        marker_symbol=['triangle-up','triangle-down'],
        hoverinfo='skip'
    )
    
    ticks = np.linspace(-max_force,max_force,5)
    data = [bar, line]
    colorbar = go.Figure(data=data, layout=colorbar_layout)
    colorbar.update_layout(
        xaxis = dict(
            range=[-max_force*1.01, max_force*1.01],
            tickmode = 'array',
            tickvals = ticks,
            ticktext = ticks
        )
    )

    colorbar.add_annotation(
        xref='x domain',
        yref='y domain',
        x=0.01, 
        y=0.51,
        text=T['apps'][app_id]['compression'][T_id],
        showarrow=False,
        font=dict(
            color='rgba(255,255,255,0.8)',
            size= style['font_size']['sm']
        ),
    )

    colorbar.add_annotation(
        xref='x domain',
        yref='y domain',
        x=0.99, 
        y=0.51,
        text=T['apps'][app_id]['tension'][T_id],
        showarrow=False,
        font=dict(
            color='rgba(255,255,255,0.8)',
            size= style['font_size']['sm']
        ),
    )

    colorbar.add_annotation(
        xref='x domain',
        yref='y domain',
        x=1.03, 
        y=-0.45,
        text='kN',
        showarrow=False,
        font=dict(
            color=style['color']['primary'],
            size= style['font_size']['sm']
        ),
    )

    return colorbar


