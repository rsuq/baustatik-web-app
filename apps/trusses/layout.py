from utils.packages_layout import *

# Module data
module_path = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
app_id = module_path.split('/')[-1]

##  Layout
# Figure layout
l = 10
fig_layout = go.Layout(
    xaxis=dict(
        domain=[0, 1],
        range=[-1, l+1],
        tickmode='array',
        showgrid=False,
        zeroline=False,
        showticklabels=False,
                    
    ),
    yaxis=dict(
        domain=[0, 1],
        range=[-2   ,3.6],
        scaleanchor='x',
        scaleratio=1,
        showgrid=False,
        zeroline=False,
        showticklabels=False,
        fixedrange=True
    ),
    height=550,
    clickmode='event+select',
    showlegend=False,
    #plot_bgcolor= 'rgba(236,240,241,0.2)',
    transition={'duration': 500, 'easing': 'cubic-in-out'},
    dragmode='select',
    margin=dict(l=0, r=0, b=0),
    #title='Normal forces diagram',
    font=dict(size=style['font_size']['sm']),
)

colorbar_layout = go.Layout(
    bargap=0,
    #plot_bgcolor= 'rgba(236,240,241,0.2)',
    #paper_bgcolor='rgba(236,240,241,0.2)',
    margin=dict(l=25, r=25, t=15, b=30),
    height=90,
    xaxis=dict(
        range=[-2*1.01, 2*1.01],
        showgrid=False,
        zeroline=False,
        fixedrange=True,
        dtick=1
    ),
    yaxis=dict(
        range=[-0.2,1.2],
        showgrid=False,
        zeroline=False,
        showticklabels=False,
        fixedrange=True,
    ),
    font={'size': style['font_size']['sm']},
    showlegend=False,
    transition={'duration': 500, 'easing': 'cubic-in-out'}
)

def layout(T_id):
    # Parameters 
    icon_width = 30
    btn_width = 80
    truss_type = html.Div(
        [
            dbc.ButtonGroup(
                [
                    dbc.Button(
                        [
                            html.Img(
                                src='/assets/icons/truss_warren_alt.svg',
                                style={'height':icon_width},
                                id={'type':'truss_icon', 'index':0}
                            ), 
                        T['apps'][app_id]['warren'][T_id]
                        ],
                        id={'type':'truss_btn', 'index':0}, color='info', outline=True, active=True,size='md', style={'width':btn_width}
                    ),
                    dbc.Button(
                        [
                            html.Img(
                                src='/assets/icons/truss_howe.svg',
                                style={'height':icon_width},
                                id={'type':'truss_icon', 'index':1}
                            ), 
                            T['apps'][app_id]['howe'][T_id]
                        ], 
                        id={'type':'truss_btn', 'index':1}, color='info', active=False, outline=True,size='md', style={'width':btn_width}
                    ),
                    dbc.Button(
                        [
                            html.Img(
                                src='/assets/icons/truss_pratt.svg',
                                style={'height':icon_width},
                                id={'type':'truss_icon', 'index':2}
                            ), 
                            T['apps'][app_id]['pratt'][T_id]
                        ], 
                        id={'type':'truss_btn', 'index':2}, color='info', outline=True, active=False,size='md', style={'width':btn_width}
                    ),
                ]
            )
        ],
        className='d-grid gap-2 col-11 mx-auto'
    )

    bays_slider = dcc.Slider(
        id='bays-input',
        min=1,
        max=10,
        value=2,
        step=1,
        marks={str(value): {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(2, 11, 2)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )

    h_slider =dcc.Slider(
        id=f'h-input-{app_id}',
        min=1,
        max=3,
        value=3,
        step=0.5,
        marks={value: {'label': str(value), 'style': {'font-size': style['font_size']['sm']}}
            for value in range(1,4)},
        updatemode='drag',
        included=True,
        tooltip={'placement': 'bottom', 'always_visible': True},
    )


    # Data storage
    load_db = dcc.Store(
        id = f'load-db-{app_id}',
        data = {}
    )

    force_db = dcc.Store(
        id = f'force-db-{app_id}',
        data = {'max_force':2}
    )

    load_form = dbc.Card(
        [
            dbc.CardHeader(
                id=f'load-form-{app_id}',
                style={'background-color':'#F7F7F7'}
            ),
            dbc.Collapse(
                [
                    dbc.CardBody(
                        [
                            dbc.Label(T['apps'][app_id]['horizontal_magnitude'][T_id], html_for=f'load-h-{app_id}'),
                            dbc.Input(
                                id=f'load-h-{app_id}',
                                type='number', 
                                min=-10, 
                                max=10, 
                                step=1, 
                                placeholder='-10 to 10 kN',
                                className='mb-3',
                            ),
                            dbc.Label(T['apps'][app_id]['vertical_magnitude'][T_id], html_for=f'load-v-{app_id}'),
                            dbc.Input(
                                id=f'load-v-{app_id}',
                                type='number', 
                                min=-10, 
                                max=10, 
                                step=1, 
                                placeholder='-10 to 10 kN',
                                className='mb-3'
                            )
                        ],
                        style={'padding':15}
                    ),
                    dbc.ButtonGroup(
                        [      
                            dbc.Button(
                                html.I(className='fa fa-times'),
                                id=f'load-cancel-{app_id}',
                                color='secondary', 
                                outline=True, 
                                active=False,
                                type='cancel',
                            ),
                            dbc.Tooltip(
                                'Close',
                                placement='bottom',
                                target=f'load-cancel-{app_id}'
                            ),
                            dbc.Button(
                                html.I(className='fa fa-trash'),
                                id=f'load-delete-{app_id}',
                                color='danger', 
                                outline=True, 
                                active=False,
                                type='submit',
                            ),
                            dbc.Tooltip(
                                'Delete',
                                placement='bottom',
                                target=f'load-delete-{app_id}'
                            ),
                            dbc.Button(
                                html.I(className='fa fa-check'),
                                id=f'load-validate-{app_id}',
                                color='success', 
                                outline=True, 
                                active=False,
                                type='submit',
                                className='btn-block',
                            ),
                            dbc.Tooltip(
                                'Confirm',
                                placement='bottom',
                                target=f'load-validate-{app_id}',
                            ),
                        ],
                        className='d-grid gap-1 d-md-flex',
                        style={'background-color':'white'}
                    )
                ],
                id=f'new-load-{app_id}',
                is_open=False
            ),
        ],
        color='rgba(236,240,241,0.1)'
        #outline=False
    )

    load_container = html.Div(
        [
            'Load list should be here'
        ],
        style={'maxHeight': 240, "overflowY": "auto"},
        id=f'load-container-{app_id}'
    )

    theory_box = helper.theory_box(app_id, T_id, module_path)

    parameters = html.Div(
        [
            dcc.Markdown(T['apps'][app_id]['truss_geometry'][T_id], style={'textAlign': 'center'}),
            truss_type,
            html.Br(),
            dcc.Markdown(T['apps'][app_id]['number_bays'][T_id],style={'textAlign': 'center'}),
            bays_slider,
            html.Br(),
            dcc.Markdown(f"{T['apps'][app_id]['truss_height'][T_id]} **ℎ \[m]**",style={'textAlign': 'center'}),
            h_slider,
            html.Br(),
            html.Hr(),
            dcc.Markdown(T['apps'][app_id]['acting_loads'][T_id],style={'textAlign': 'center'}),
            load_form,
            html.Br(),
            load_container,
            load_db,
            force_db
        ]
    )

    graphs = html.Div(
        [
            dcc.Graph(figure = go.Figure(layout=fig_layout), config={'displayModeBar':False}, id=f'graph-{app_id}'),
            dcc.Graph(figure = go.Figure(layout=colorbar_layout), config={'displayModeBar':False}, id=f'colorbar-{app_id}'),
            html.Div(id='blank')
        ]
    )

    layout = helper.layout_template(app_id, T_id, theory_box, parameters, graphs)

    return layout