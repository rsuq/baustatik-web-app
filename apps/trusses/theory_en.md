When modelling trusses, we often treat them as *ideal trusses*. This imposes the following constraints:

* Each beam starts and ends in a node without eccentricity;
* Nodes are hinges which cannot transmit moments;
* Forces are concentrated in the nodes.

As a consequence, in an ideal truss each beam experiences only normal forces and no shear forces ($V=0$) or moments ($M=0$).