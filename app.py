import dash
import dash_bootstrap_components as dbc
from dash_bootstrap_templates import load_figure_template

# Themes
theme = 'FLATLY'
bootstrap_theme = getattr(dbc.themes, theme)
load_figure_template(theme.lower())

MATHJAX_CDN = '''
https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/
MathJax.js?config=TeX-MML-AM_CHTML'''
external_scripts = [
    {
        'type': 'text/javascript',
        'id': 'MathJax-script',
        'src': MATHJAX_CDN,
    },
]

# Flask application
app = dash.Dash(
    __name__, 
    title='Baustatik I Web App',
    suppress_callback_exceptions=True,
    external_stylesheets=[bootstrap_theme, dbc.icons.BOOTSTRAP,  dbc.icons.FONT_AWESOME],
    external_scripts=external_scripts,
    meta_tags=[{'name':'viewport','content': 'width=device-width, initial-scale=1.0, maximum-scale=1.2, minimum-scale=0.5,'}]
)

###### important for latex ######
app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>{%title%}</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            <script type="text/x-mathjax-config">
            MathJax.Hub.Config({
                tex2jax: {
                inlineMath: [ ['$','$'],],
                processEscapes: true
                }
            });
            </script>
            {%renderer%}
        </footer>
    </body>
</html>
'''


